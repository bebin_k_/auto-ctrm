import pandas as pd
import calendar
from pyrebase import pyrebase
from io import BytesIO

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

storage = firebase.storage()

def cjs(upload_filename, mapping_filename, date_1):

    df = pd.read_csv(upload_filename, encoding='ISO-8859-1', header=None)
    map_df = pd.read_excel(mapping_filename, sheet_name='cjs')
    sub_acc = pd.read_excel(mapping_filename, sheet_name = 'sub-accounts')
    sub_acc = sub_acc["Sub-Acc"].to_numpy()
    
    rows = df.shape[0]
    new_df = pd.DataFrame(columns = ['Ctrbook Code*', 'Profit Center*', 'Ctr Date*', 'Contract Term*', 'Trader*', 'Broker*', 'Broker Reference', 'Position Type*', 'Broker Account No*', 'No. Of Lots*', 'Shipment Month*', 'Price*', 'Sub Strategy', 'Contract type', 'Trade Type*', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)']) 
    map = pd.DataFrame(columns =  ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','46','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63','64','65','66','67','68','69','70','71','72','73','74','75','76','77','78','79','80','81','82','83','84','85','86','87','88','89','90','91','92','93','94','95','96','97','98','99','100','101','102','103','104'])
    #backdated = pd.DataFrame(columns =  ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','46','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63','64','65','66','67','68','69','70','71','72','73','74','75','76','77','78','79','80','81','82','83','84','85','86','87','88','89','90','91','92','93','94','95','96','97','98','99','100','101','102','103','104'])
    backdated = pd.DataFrame(columns = ['Ctrbook Code*', 'Profit Center*', 'Ctr Date*', 'Contract Term*', 'Trader*', 'Broker*', 'Broker Reference', 'Position Type*', 'Broker Account No*', 'No. Of Lots*', 'Shipment Month*', 'Price*', 'Sub Strategy', 'Contract type', 'Trade Type*', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)']) 

    price = 0
    lots = 0
    pos_type = ''
    index = 0
    cleaned_lots = ''

    for i in range(0, rows):
        cleaned_lots = ''

        code = df[35].values[i]
        #print(code)
        comm = df[37].values[i]
        comm = comm.split(' ')[2]
        #print(comm)
        temp = map_df.loc[(map_df['CJS ACC No'] == code) & (map_df['CJS File Commodity Name'] == comm)]
        #print(temp)
        date = df.iloc[i, 44]
        #print(date)
        dt = date.split('/')[0]
        month = date.split('/')[1]
        year = date.split('/')[2]
        month_naming = calendar.month_name[int(month)]
        month_naming = month_naming.upper()[0:3]
        ctr_date = dt+'-'+month_naming+'-'+year
        #print(ctr_date)

        code = code.split(' ')[3]

        if code in sub_acc:
            #print("Acc Not to be taken")
            continue
        
        elif ctr_date != date_1:
            #print('Backdated')
            if comm == 'WEAT':
                index = index + 1
                price = df.iloc[i, 51]
                #print(price)
                lots = df.iloc[i, 48]
                for j in range(0, len(lots)):
                    if lots[j] != '(' and lots[j] != ')':
                        cleaned_lots = cleaned_lots + lots[j]
                cleaned_lots = float(cleaned_lots)
                if df.iloc[i, 48][0] == '(':
                    pos_type = 'SELL'
                else:
                    pos_type = "BUY"

                #shipment_month = month_naming+' '+year
                shipment_month = df[37].values[i]
                print(shipment_month)
                dt = shipment_month.split(' ')[1]
                index_month = 3
                month = dt[:index_month]
                index_year = 3
                year = dt[index_year:]
                year = "20" + year
                shipment_month_final = month + ' ' + year

                #shipment_month = df.iloc[i, 37]
                #print(shipment_month)
                #shipment_month_month = shipment_month.split('-')[1]
                #shipment_month_year = int(shipment_month.split('-')[2]) + 2000
                #shipment_month_final = shipment_month_month + " " + str(shipment_month_year)
                #row = ['WHEAT-ZA', 'WHEAT-ZA', ctr_date, 'SAFEX WHEAT', 'AlexD', 'CJS SECURITIES (PTY) LTD', 'XRO132', pos_type, 'XRO132', cleaned_lots, shipment_month_final, price, 'HEDGE - HEDGE DEFAULT', '', 'FUTURES', 'DEFAULT', '', '', '', '']
                #new_df.loc[index] = row
                not_mapped = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], pos_type, temp['Broker Account No.'].values[0], cleaned_lots, shipment_month_final, price, temp['Sub Strategy'].values[0], '', 'FUTURES', '', '', '', '', '']
                backdated.loc[i] = not_mapped
            

        elif not temp.empty:
            #print("hello")
            #print(temp)
            if comm == 'WEAT':
                index = index + 1
                price = df.iloc[i, 51]
                #print(price)
                lots = df.iloc[i, 48]
                for j in range(0, len(lots)):
                    if lots[j] != '(' and lots[j] != ')':
                        cleaned_lots = cleaned_lots + lots[j]
                cleaned_lots = float(cleaned_lots)
                if df.iloc[i, 48][0] == '(':
                    pos_type = 'SELL'
                else:
                    pos_type = "BUY"

                #shipment_month = month_naming+' '+year
                shipment_month = df[37].values[i]
                print(shipment_month)
                dt = shipment_month.split(' ')[1]
                index_month = 3
                month = dt[:index_month]
                index_year = 3
                year = dt[index_year:]
                year = "20" + year
                shipment_month_final = month + ' ' + year

                #shipment_month = df.iloc[i, 37]
                #print(shipment_month)
                #shipment_month_month = shipment_month.split('-')[1]
                #shipment_month_year = int(shipment_month.split('-')[2]) + 2000
                #shipment_month_final = shipment_month_month + " " + str(shipment_month_year)
                #row = ['WHEAT-ZA', 'WHEAT-ZA', ctr_date, 'SAFEX WHEAT', 'AlexD', 'CJS SECURITIES (PTY) LTD', 'XRO132', pos_type, 'XRO132', cleaned_lots, shipment_month_final, price, 'HEDGE - HEDGE DEFAULT', '', 'FUTURES', 'DEFAULT', '', '', '', '']
                #new_df.loc[index] = row
                row = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], pos_type, temp['Broker Account No.'].values[0], cleaned_lots, shipment_month_final, price, temp['Sub Strategy'].values[0], '', 'FUTURES', '', '', '', '', '']
                new_df.loc[i] = row

        else:
            #print("Mapping not Found")
            not_mapped = [df[1].values[i], df[2].values[i], df[3].values[i], df[4].values[i], df[5].values[i], df[6].values[i], df[7].values[i], df[8].values[i],  df[9].values[i], df[10].values[i], df[11].values[i], df[12].values[i], df[13].values[i], df[14].values[i], df[15].values[i], df[16].values[i], df[17].values[i], df[18].values[i], df[19].values[i], df[20].values[i], df[21].values[i], df[22].values[i], df[23].values[i], df[24].values[i], df[25].values[i], df[26].values[i], df[27].values[i], df[28].values[i], df[29].values[i], df[30].values[i], df[31].values[i], df[32].values[i], df[33].values[i], df[34].values[i], df[35].values[i], df[36].values[i], df[37].values[i], df[38].values[i], df[39].values[i], df[40].values[i], df[41].values[i], df[42].values[i], df[43].values[i], df[44].values[i], df[45].values[i], df[46].values[i], df[47].values[i], df[48].values[i], df[49].values[i], df[50].values[i], df[51].values[i], df[52].values[i], df[53].values[i], df[54].values[i], df[55].values[i], df[56].values[i], df[57].values[i], df[58].values[i], df[59].values[i], df[60].values[i], df[61].values[i], df[62].values[i], df[63].values[i], df[64].values[i], df[65].values[i], df[66].values[i], df[67].values[i], df[68].values[i], df[69].values[i], df[70].values[i], df[71].values[i], df[72].values[i], df[73].values[i], df[74].values[i], df[75].values[i], df[76].values[i], df[77].values[i], df[78].values[i], df[79].values[i], df[80].values[i], df[81].values[i], df[82].values[i], df[83].values[i], df[84].values[i], df[85].values[i], df[86].values[i], df[87].values[i], df[88].values[i], df[89].values[i], df[90].values[i], df[91].values[i], df[92].values[i], df[93].values[i], df[94].values[i], df[95].values[i], df[96].values[i], df[97].values[i], df[98].values[i], df[99].values[i], df[100].values[i], df[101].values[i], df[102].values[i], df[103].values[i], df[104].values[i]]
            map.loc[i] = not_mapped

    # init writer
    bio = BytesIO()
    writer = pd.ExcelWriter(bio, engine='xlsxwriter')
    # sheets
    new_df.to_excel(writer, "CJS", index=False)
    map.to_excel(writer, "Mapping Not found", index=False)
    backdated.to_excel(writer, "Backdated", index=False)

    # save the workbook
    writer.save()
    bio.seek(0)      

    # get the excel file (answers my question)          
    workbook    = bio.read()  
    excelFile   = workbook


    storage.child('cjs_output/CJS Output.xlsx').put(excelFile)
    storage.child('cjs_output/CJS Output.xlsx').get_url(None)

    
