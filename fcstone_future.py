import pandas as pd
import calendar
from pyrebase import pyrebase
from io import BytesIO
import numpy as np

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

storage = firebase.storage()

def fcstone_future(upload_filename, mapping_filename):
    print("hello world futures")

    #Brokers for mapping
    data = [['Société Generale', 'SocGen'], ['INTL FCStone', 'FCStone'],['MAREX SPECTRON','ms']]
    b_df = pd.DataFrame(data, columns=['Broker','code'])

    accounts= pd.read_excel(mapping_filename, sheet_name = "accounts")
    accounts=pd.merge(accounts,b_df, on ='Broker', how ='inner')   #inner join to match all the broker code names
    t1 = accounts["code"].isin(['FCStone'])
    #t2 = accounts["BU"].isin(['PL GRAINS'])
    accounts = accounts[t1]

    #Instrument coded file
    ins= pd.read_excel(mapping_filename, sheet_name = "ins_code")

    # multiplier Dataset
    FC_mf = pd.read_excel(mapping_filename, sheet_name = "fcstone_futures") #directory
                                  #sheets
    #FC_mf= pd.read_excel(xls, 'FC Stone Futures')
    FC_mf.rename(columns = {'Commodity':'Index'}, inplace = True)

    df=pd.read_csv(upload_filename)

    #filtering based on broker account in FCstone, and to get the future prices 
    x = df["Account Id"].isin(accounts['Broker Ac'])
    x2 = ~df["Trade Type"].isin(["Call","Put"])
    df2=df[x & x2]

    final = df2[["Instrument Name","Delivery Period","Market Price"]]
    final.rename(columns = {'Instrument Name':'Index'}, inplace = True)
    # remove duplicates
    y=final.drop_duplicates()

    test = pd.DataFrame()
    test = y

    #inner join to match all the index of FCStone to the corresponding MULTIPLIERS
    y = pd.merge(y, FC_mf, on ='Index', how ='inner')

    #add a column with static value 1 
    y.insert(0, "dd", "1")

    # concat columns by separator to create DATE column to match Fusion format
    y['Date']=y['dd'].str.cat(y[['Delivery Period']], sep='-')

    #inner join to match all the index code to the corresponding instrument names
    joined = pd.merge(y, ins, on ='Index', how ='inner')

    xyz= "FCStone" + " " + joined['Ins_full'] + " " + ">>"
    joined['Instrument'] = xyz

    # Closing price with multiplier
    joined['CP_with_multiplier']= joined['Future Price Multiplier']*joined['Market Price']
    

    #joined['Date'] = joined['Date'].apply(lambda x: x.replace('-', '/'))
    joined['Date'] = joined['Date'].replace({'-':'/'},regex=True)
    joined['Date'] = joined['Date'].apply(lambda x: '/'.join([x.split('/')[0], x.split('/')[1][0] + x.split('/')[1][1:].lower(), x.split('/')[2]]))


    # download the file as csv
    #joined.to_csv("C:\\Users\\saumya.joshi\\Downloads\\Market Prices\\joined2.csv")


    # Soc gen output as read file here
    socgen_url = storage.child('fusion_socgen_uploader/Socgen_future_output.xlsx').get_url(None)
    op_socgen= pd.read_excel(socgen_url)
    print("----------------------------------")
    op_socgen.head()
    op_copy = op_socgen.copy()
    #op_ind = op_copy['Months'].values.tolist()
    op_copy.set_index('Months', inplace=True)
    

    joined_records = joined.to_dict('record')
    for row in joined_records:
        try:
            op_copy.at[row['Date'], row['Instrument']]
            op_copy.at[row['Date'], row['Instrument']] = row['CP_with_multiplier']
        except KeyError:
            continue

    
    op_copy.reset_index(inplace=True)

    # download the file as csv
    #op_copy.to_csv("C:\\Users\\saumya.joshi\\Downloads\\Market Prices\\csv exports daily\\Output_FCStone.csv", index=False)
    
    # init writer
    bio = BytesIO()
    writer = pd.ExcelWriter(bio, engine='xlsxwriter')
    # sheets
    op_copy.to_excel(writer, "FcStone - Futures", index=False)
    test.to_excel(writer, "FcStone Unique Values", index=False)

    # save the workbook
    writer.save()
    bio.seek(0)      

    # get the excel file (answers my question)          
    workbook    = bio.read()  
    excelFile   = workbook

    storage.child('fusion_fcstone_uploader/Fcstone_future_output.xlsx').put(excelFile)
    storage.child('fusion_fcstone_uploader/Fcstone_future_output.xlsx').get_url(None)
