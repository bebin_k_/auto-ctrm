import pandas as pd
import calendar
from pyrebase import pyrebase
from io import BytesIO
import numpy as np
import re

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

storage = firebase.storage()

def marex_option(upload_filename, mapping_filename):
    print("hello world option")

    #mapping BRoker names
    data = [['Société Generale', 'SocGen'], ['INTL FCStone', 'FCStone'],['MAREX SPECTRON','Marex']]
    b_df = pd.DataFrame(data, columns=['Broker','code'])

    # code for years
    datayr = [['2020', '20'], ['2021', '21'],['2022','22'],['2023','23']]
    yr_df = pd.DataFrame(datayr, columns=['Yr','year'])

    #code for call and put
    data_cp = [['C', 'CALL'], ['P', 'PUT']]
    cp_df = pd.DataFrame(data_cp, columns=['Option Type','op_code'])

    accounts= pd.read_excel(mapping_filename, sheet_name = "accounts")
    accounts=pd.merge(accounts,b_df, on ='Broker', how ='inner')   #inner join to match all the broker code names
    t1 = accounts["code"].isin(['Marex'])
    accounts = accounts[t1]

    #Instrument coded file
    ins= pd.read_excel(mapping_filename, sheet_name = "ins_code") #directory
    #ins = pd.read_excel(file1, 'Ins_code')
    #ins_op= pd.read_excel(file1, 'for_options')

    # multiplier Dataset
    Marex_mo = pd.read_excel(mapping_filename, sheet_name = "marex_options")  #directory
    #Marex_mo = pd.read_excel(xls, 'Marex Options')
    Marex_mo.rename(columns = {'CTRM Commodity':'Index'}, inplace = True)

    #Tickers
    months=pd.read_excel(mapping_filename, sheet_name = "months")
    months['Number']=months['Number'].astype('str')
    
    # Ticker Fendhal mapping for Exchange column
    MX_ticker = pd.read_excel(mapping_filename, sheet_name = "marex_ticker") #directory
    #FC_ticker = pd.read_excel(xls_ticker, 'Marex')
    MX_ticker.rename(columns = {'Fusion Price Quote':'Index'}, inplace = True)
    #----------------




    df2= pd.read_csv(upload_filename) #directory
    df2.head()




    len(df2)


    df2['Position Delta'] = df2['Position Delta'].astype(str).str.replace(",", "")
    df2['Volume'] = df2['Volume'].astype(str).str.replace(",", "")



    df2['Delta']=df2['Position Delta'].astype(float)/df2['Volume'].astype(float)
    df2['Delta']= df2['Delta'].round(4)
#    df2.head()



    df2['Ledger Code'].unique()



    df2['Ledger Code']=df2['Ledger Code'].astype(str)
    df2['Ledger Code']=df2['Ledger Code'].str.strip()


    # In[583]:


    df2['Ledger Code'].unique()



    #filtering based on broker account for MArex, and to get the future prices 

    x = df2["Ledger Code"].isin(['8968','8979'])
    x2 = df2["Option Type"].isin(["C","P"])
    df4=df2[x & x2]
    df4



    final = df4[["Instrument Long Name","Delivery/Prompt date","Strike","Option Type","Market Rate",'Delta']]
    final.rename(columns = {'Instrument Long Name':'Index'}, inplace = True)
    # remove duplicates
    y = final.drop_duplicates(subset=['Index', 'Delivery/Prompt date','Strike','Option Type'], keep='first')

    test = pd.DataFrame()
    test = y




    #Splitting Delivery/Prompt date Column data into Date - Month - Year

    y[['Yr','Number','Start']] = y["Delivery/Prompt date"].str.split('/', n=2 , expand=True)

    y.head()




    #
    y = pd.merge(y,cp_df, on ='Option Type', how ='inner')
    y


    #inner join to match all the index of FCStone to the corresponding MULTIPLIERS

    y = pd.merge(y,Marex_mo , on ='Index', how ='inner')
    y



    # removing the extra 0 infront of months - for mapping 
    y["Number"] = y["Number"].apply(lambda x: x.lstrip('0'))
    y


    y=pd.merge(y, months, on ='Number', how ='inner')
    y.head()



    y=pd.merge(y, yr_df, on ='Yr', how ='inner')
    y


    #inner join to match all the index code to the corresponding instrument names

    y = pd.merge(y, ins, on ='Index', how ='inner')
    y


    #inner join to match all the index code to the corresponding instrument names

    y = pd.merge(y, MX_ticker, on ='Index', how ='inner')
    y.head()



    gel= y['Fusion Ticker'] + "-" + y['Code'] + y['year'] + " " + "(" + y['Full'] + ")"
    y['Exchange Contract'] = gel
    y
    #CBOT-C-V22 (October)


    # In[628]:


    xyz= "Marex" + " " + y['Ins_full']
    y['Instrument'] = xyz
    y.head()



#    y['Strike']=y['Strike'].astype(float)


    # In[631]:


    # Strike multiplication

    y['Strike']= y['Strike']*y['Strike Multiplier']
    y.head()


    # In[632]:


    y.iloc[-0]


    # In[633]:


    # Strike multiplication

    y['Final Market Rrate']= y['Market Rate']*y['Premium Multiplier']
    y.head()


    len(y)


    # In[636]:


    # Remove two extra columns

    y.drop(['Delivery/Prompt date','Option Type','Index','Number','Month', 'Yr','months_sm','Code','Full','Ins_full','Fusion Ticker'], axis=1,inplace=True)
    y.head()


    # In[637]:



    #Adding extra empty columns

    y['Order'] = ''
    y['Quote Date'] = ''
    y['Price'] = ''
    y['Start Date'] = ''
    y['End Date'] = ''
    y['Expiry Date'] = ''
    y['Volatility'] = ''
    y['Interest Rate'] = ''

    y['Rho'] = ''
    y['Impl Volatility'] = ''
    y['Correlation'] = ''
    y['Instrument Type Code'] = ''

    y['Gamma'] = ''
    y['Vega'] = ''
    y['Theta'] = ''






    # reaaranging columns

    dft=y[['Instrument','Order','Exchange Contract','Quote Date','Strike','op_code','Price','Start Date','End Date','Expiry Date','Volatility','Interest Rate','Final Market Rrate','Rho','Impl Volatility','Correlation','Instrument Type Code','Delta','Gamma','Vega','Theta']]

#    dft=y[['Instrument','Exchange Contract','Strike','op_code','Final Market Rrate','Delta']]
    dft


    # In[638]:


    len(dft)   # including duplicates


    # In[639]:


    dft=dft.drop_duplicates()
    dft

    dft['Strike']=dft['Strike'].round(1)


    test2 = pd.DataFrame()
    test2 = dft


    op3 = storage.child('fusion_fcstone_uploader/Fcstone_option_output.xlsx').get_url(None)
    op3= pd.read_excel(op3)


    # In[661]:



    for i in range(len(dft)):
        for j in range(len(op3)):
            x1=dft.iloc[i,0]
            x2=dft.iloc[i,2]
            x3=dft.iloc[i,4]
            x4=dft.iloc[i,5]
            y1=op3.iloc[j,0]
            y2=op3.iloc[j,2]
            y3=op3.iloc[j,4]
            y4=op3.iloc[j,5]

            #print("read file",x1,x2,x3,x4)
            #print("write file",y1,y2,y3,y4)
            if x1==y1 and x2==y2 and int(x3)==int(y3) and x4==y4 :
                op3.iloc[j,12]=dft.iloc[i,12]
                op3.iloc[j,17]=dft.iloc[i,17]
                print("match")
    
        




    print(op3)
    # init writer
    bio = BytesIO()
    writer = pd.ExcelWriter(bio, engine='xlsxwriter')
    # sheets
    op3.to_excel(writer, "Marex - Options", index=False)
    test.to_excel(writer, "Marex Unique Values", index=False)
    test2.to_excel(writer, "Marex Mapped Table", index=False)

    # save the workbook
    writer.save()
    bio.seek(0)      

    # get the excel file (answers my question)          
    workbook    = bio.read()  
    excelFile   = workbook


    storage.child('fusion_marex_uploader/Marex_option_output.xlsx').put(excelFile)
    storage.child('fusion_marex_uploader/Marex_option_output.xlsx').get_url(None)

    