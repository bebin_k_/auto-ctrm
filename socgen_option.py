import pandas as pd
import calendar
from pyrebase import pyrebase
from io import BytesIO
import numpy as np

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

storage = firebase.storage()

def socgen_option(upload_filename, mapping_filename):
    print("hello world options")

    # mapping for Broker names
    data = [['Société Generale', 'SocGen'], ['INTL FCStone', 'FCStone'],['MAREX SPECTRON','ms']]
    b_df = pd.DataFrame(data, columns=['Broker','code'])

    #Instrument coded file
    #used the same excel for futures and options ?? - Hari
    ins= pd.read_excel(mapping_filename, sheet_name = "ins_code") #directory
    #ins = pd.read_excel(file1, 'Ins_code')
    #ins_op= pd.read_excel(file1, 'for_options')

    # multiplier Dataset
    SG_mo = pd.read_excel(mapping_filename, sheet_name = "socgen_options") #directory
    #SG_mo = pd.read_excel(xls, 'SocGen Options')
    SG_mo.rename(columns = {'Commodity':'Index'}, inplace = True)

    #Tickers
    #tickers=pd.read_csv(mapping_filename, sheet_name = "socgen_ticker") #month ticker

    #Tickers
    months=pd.read_excel(mapping_filename, sheet_name = "months") #months

    accounts= pd.read_excel(mapping_filename, sheet_name = "accounts")
    accounts=pd.merge(accounts,b_df, on ='Broker', how ='inner')   #inner join to match all the broker code names
    t1 = accounts["code"].isin(['SocGen'])      #filter
    accounts = accounts[t1]

    df=pd.read_csv(upload_filename)

    #filtering based on broker account in PL grains-SocGen, and to get the option prices 
    x = df["Account Number"].isin(accounts['Broker Ac'])
    x2 = df["Put/Call"].isin(["C","P"])
    df2=df[x & x2]

    final = df2[["Futures Description","Closing Price","Delta"]]
    # remove duplicates
    y=final.drop_duplicates()

    test = pd.DataFrame()
    test = y

    #removing double space in between column - Futures Description
    y['Futures Description'] = y['Futures Description'].replace(r'\s+', ' ', regex=True)

    #splitting the column
    #y[['Option Type','Month','Yr','Index','Strike Price']] = y["Futures Description"].str.split(' ', n=4 , expand=True)
    y[['Option Type','Month','Yr','split']] = y["Futures Description"].str.split(' ', n=3 , expand=True)
    y['Index'] = y["split"].map(lambda x: str(x)[:-4])
    #split the last letter in the end of that column
    y['Strike'] = y["split"].str[-4:]
    y['Index'] = y['Index'].str.strip()

    y=pd.merge(y, months, on ='Month', how ='inner')

    #inner join to match all the index code to the corresponding instrument names
    y = pd.merge(y, ins, on ='Index', how ='inner')

    # Ticker Fendhal mapping for Exchange column
    SG_ticker = pd.read_excel(mapping_filename, sheet_name = "socgen_ticker") #Ticker mapping
    #SG_ticker = pd.read_excel(xls_ticker, 'SocGen')
    SG_ticker.rename(columns = {'Fusion Price Quote':'Ins_full'}, inplace = True)

    y = pd.merge(y, SG_ticker, on ='Ins_full', how ='inner')

    gel= y['Fusion Ticker'] + "-" + y['Code'] + y['Yr'] + " " + "(" + y['Full'] + ")"
    y['Exchange Contract'] = gel

    xyz= "SocGen" + " " + y['Ins_full']
    y['Instrument'] = xyz

    # for using in lambda function
    y.rename(columns = {'Closing Price':'Closing_Price' },inplace=True)

    y['space']=y['Closing_Price'].apply(lambda Closing_Price: Closing_Price.lstrip())

    #Splitting 
    y[['no','fraction']] = y["space"].str.split(' ', expand=True)

    # Defining all the conditions inside a function
    def condition(x):
        if x=="1/2":
            return 5
        elif x=="1/4":
            return 25
        elif x=="3/4":
            return 75
        elif x=="1/8":
            return 125
        elif x=="3/8":
            return 375
        elif x=="7/8":
            return 875
        elif x=="5/8":
            return 625
        else:
            return 0

    # Applying the conditions
    y['f2'] = y['fraction'].apply(condition)

    y['f2']=y['f2'].apply(str)

    # concat columns to create a complete Closing Price 
    y['joined price']= y['no']+y['f2']

    # for making the closing price multipliable with Strike price
    floatArray = np.asfarray(y['joined price'], dtype = float)

    y['change_CP']= floatArray

    #inner join to match all commodity to the corresponding multiplier
    y = pd.merge(y,SG_mo , on ='Index', how ='inner')

    # Closing price with Strike prices
    y['CP_with_multiplier']= y['Premium Multiplier']*y['change_CP']



    #Adding extra empty columns

    y['Order'] = ''
    y['Quote Date'] = ''
    y['Price'] = ''
    y['Start Date'] = ''
    y['End Date'] = ''
    y['Expiry Date'] = ''
    y['Volatility'] = ''
    y['Interest Rate'] = ''

    y['Rho'] = ''
    y['Impl Volatility'] = ''
    y['Correlation'] = ''
    y['Instrument Type Code'] = ''

    y['Gamma'] = ''
    y['Vega'] = ''
    y['Theta'] = ''



    # reaaranging columns
    dft=y[['Instrument','Order','Exchange Contract','Quote Date','Strike','Option Type','Price','Start Date','End Date','Expiry Date','Volatility','Interest Rate','CP_with_multiplier','Rho','Impl Volatility','Correlation','Instrument Type Code','Delta','Gamma','Vega','Theta']]

#    dft=y[['Instrument','Exchange Contract','Strike','Option Type','CP_with_multiplier','Delta']]

    # download the file as csv
    #dft.to_csv("C:\\Users\\saumya.joshi\\Downloads\\Market Prices\\csv exports daily\\Options_SocGen_table.csv", index=False)


#    dft['Strike']=dft['Strike'].round(1)

    test2 = pd.DataFrame()
    test2 = dft


    op= pd.read_excel(mapping_filename, sheet_name = "option_output") #Option Output


    for i in range(len(dft)):
        for j in range(len(op)):
            x1=dft.iloc[i,0]
            x2=dft.iloc[i,2]
            x3=dft.iloc[i,4]
            x4=dft.iloc[i,5]
            y1=op.iloc[j,0]
            y2=op.iloc[j,2]
            y3=op.iloc[j,4]
            y4=op.iloc[j,5]

            #print("read file",x1,x2,x3,x4)
            #print("write file",y1,y2,y3,y4)
            if x1==y1 and x2==y2 and int(x3)==int(y3) and x4==y4:
                op.iloc[j,12]=dft.iloc[i,12]
                op.iloc[j,17]=dft.iloc[i,17]
                print("match")


#Output CSV

    # init writer
    bio = BytesIO()
    writer = pd.ExcelWriter(bio, engine='xlsxwriter')
    # sheets
    op.to_excel(writer, "SocGen - Options", index=False)
    test.to_excel(writer, "SocGen Unique Values", index=False)
    test2.to_excel(writer, "SocGen Mapped Table", index=False)

    # save the workbook
    writer.save()
    bio.seek(0)      

    # get the excel file (answers my question)          
    workbook    = bio.read()  
    excelFile   = workbook


    storage.child('fusion_socgen_uploader/Socgen_option_output.xlsx').put(excelFile)
    storage.child('fusion_socgen_uploader/Socgen_option_output.xlsx').get_url(None)