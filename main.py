#from flask_ngrok import run_with_ngrok
import pandas as pd
import calendar
from flask import Flask, url_for, render_template, request, send_file, redirect, flash
from werkzeug.utils import secure_filename
from flask_caching import Cache
#from config import Config
import pyrebase
import gcsfs
from io import BytesIO

from socgen_future import socgen_future
from socgen_option import socgen_option

from marex_future import marex_future
from marex_option import marex_option

from fcstone_future import fcstone_future
from fcstone_option import fcstone_option

from vanguard import vanguard
from cocoa import cocoa
from cjs import cjs
from absa import absa
from rmb import rmb
from rmd import rmd
from rmd import rmd_fx

from fcstone import fcstone
from macquaire import macquaire
from marex import marex
from newedge import newedge

import requests
import numpy as np
from datetime import date
from recon_macquaire import recon_macquaire

app = Flask(__name__)

#run_with_ngrok(app)


#app.config.from_object(Config)

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

cache = Cache(config={'CACHE_TYPE': 'null'})

cache.init_app(app)

storage = firebase.storage()

upload_filename = ''

@app.route('/newedge_dashboard', methods=["GET", "POST"])
def newedge_dashboard():
    return render_template('newedge_dashboard.html')

@app.route('/', methods=["GET", "POST"])
def home():

    if request.method == 'POST':
        req = request.form
        url = request.url
        password = req['password']
        print(password == 'qwerty')
        if password == 'qwerty':
            return redirect('/dashboard')
        elif password == 'Srijit@123':
            print("qwerttttttreeeeeeeeeeeee")
            #return redirect('/newedge_dashboard')
            return render_template('newedge_dashboard.html')
        else:
            return redirect('/')


    return render_template('login.html')

@app.route('/dashboard', methods=["GET", "POST"])
def dashbaord():
    return render_template('upload_larry.html')

@app.route('/recon', methods=["GET", "POST"])
def recon():
    return render_template('upload_recon_ctrm.html')

@app.route('/upload_recon_ctrm', methods=["GET", "POST"])
def upload_recon_ctrm():
    storage_url_recon_ctrm = ''
    #mapping_url_rmd = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            #print("---------===========----------")
            f = request.files['file_upload']
            #date = request.form['date']
            storage.child("recon_ctrm/recon_input.xlsx").put(f)
            #storage_url_recon_ctrm = storage.child("recon_ctrm/recon_input.xlsx").get_url(None)

            #mapping_url_rmd = storage.child("mapping_files/Brokers_mapping.xlsx").get_url(None)
            #recon_ctrm_repo(storage_url_recon_ctrm)

            return render_template('upload_recon_broker.html')


        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

@app.route('/recon_dashboard', methods=["GET", "POST"])
def recon_dashboard():
    if request.method == 'POST':
        date = request.form['date']
        recon_ctrm_url = storage.child("recon_ctrm/recon_input.xlsx").get_url(None)
        macquaire_url = storage.child("recon_ctrm/recon_input.xlsx").get_url(None)
        fcstone_url = storage.child("recon_ctrm/recon_input.xlsx").get_url(None)
        newedge_url = storage.child("recon_ctrm/recon_input.xlsx").get_url(None)
        marex_url = storage.child("recon_ctrm/recon_input.xlsx").get_url(None)

    return render_template('recon_dashboard.html')

@app.route('/upload_recon_broker', methods=["GET", "POST"])
def upload_recon_broker():
    return render_template('upload_recon_broker.html')

@app.route('/recon_fcstone', methods=["GET", "POST"])
def recon_fcstone():
    return render_template('recon_dashboard.html')

@app.route('/recon_macquaire_upload', methods=["GET", "POST"])
def recon_macquaire_upload():
    print("Function triggered ")
    recon_macquaire_url = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']

            storage.child("recon_macquaire/recon_input.csv").put(f)
            recon_macquaire_url = storage.child("recon_macquaire/recon_input.csv").get_url(None)
            #print(recon_macquaire_url)

            recon_ctrm_url = storage.child("recon_ctrm/recon_input.xlsx").get_url(None)
            #print(recon_ctrm_url)

            recon_macquaire(recon_macquaire_url, recon_ctrm_url)
            return render_template('recon_dashboard.html')


        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

@app.route('/recon_marex', methods=["GET", "POST"])
def recon_marex():
    return render_template('recon_dashboard.html')

@app.route('/recon_newedge', methods=["GET", "POST"])
def recon_newedge():
    return render_template('recon_dashboard.html')


@app.route('/fcstone', methods=["GET", "POST"])
def upload_fcstone():
    return render_template('upload_fcstone.html')

@app.route('/macquaire', methods=["GET", "POST"])
def upload_macquaire():
    return render_template('upload_macquaire.html')

@app.route('/marex', methods=["GET", "POST"])
def upload_marex():
    return render_template('upload_marex.html')

@app.route('/newedge', methods=["GET", "POST"])
def upload_newedge():
    return render_template('upload_newedge.html')

@app.route('/vanguard', methods=["GET", "POST"])
def upload_vanguard():
    return render_template('upload_vanguard.html')

@app.route('/vanguard_uploader', methods=["GET", "POST"])
def upload_vanguard_file():
    storage_url_vanguard = ''
    mapping_url_vanguard = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            date = request.form['date']
            storage.child("vanguard_input/input.csv").put(f)
            storage_url_vanguard = storage.child("vanguard_input/input.csv").get_url(None)
            mapping_url_vanguard = storage.child("mapping_files/Brokers_mapping.xlsx").get_url(None)
            vanguard(storage_url_vanguard, mapping_url_vanguard, date)
            return redirect('/downloadfile/vanguard')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

@app.route('/cjs', methods=["GET", "POST"])
def upload_cjs():
    return render_template('upload_cjs.html')

@app.route('/cjs_uploader', methods=["GET", "POST"])
def upload_cjs_file():
    storage_url_cjs = ''
    mapping_url_cjs = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            date = request.form['date']
            storage.child("cjs_input/input.csv").put(f)
            storage_url_cjs = storage.child("cjs_input/input.csv").get_url(None)
            mapping_url_cjs = storage.child("mapping_files/Brokers_mapping.xlsx").get_url(None)
            cjs(storage_url_cjs, mapping_url_cjs, date)
            return redirect('/downloadfile/cjs')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

@app.route('/absa', methods=["GET", "POST"])
def upload_absa():
    return render_template('upload_absa.html')

@app.route('/absa_uploader', methods=["GET", "POST"])
def upload_absa_file():
    storage_url_absa = ''
    mapping_url_absa = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            date = request.form['date']
            storage.child("absa_input/input.csv").put(f)
            storage_url_absa = storage.child("absa_input/input.csv").get_url(None)
            mapping_url_absa = storage.child("mapping_files/Brokers_mapping.xlsx").get_url(None)
            absa(storage_url_absa, mapping_url_absa, date)
            return redirect('/downloadfile/absa')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

@app.route('/rmb', methods=["GET", "POST"])
def upload_rmb():
    return render_template('upload_rmb.html')


@app.route('/rmb_uploader', methods=["GET", "POST"])
def upload_rmb_file():
    storage_url_rmb = ''
    mapping_url_rmb = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files.getlist('file_upload')
            date = request.form['date']
            mapping_url_rmb = storage.child("mapping_files/Brokers_mapping.xlsx").get_url(None)
            my_df  = pd.DataFrame()

            bio = BytesIO()
            writer = pd.ExcelWriter(bio, engine='xlsxwriter')
            # sheets
            my_df.to_excel(writer, "RMB", index=False)
            my_df.to_excel(writer, "Mapping Not found", index=False)
            my_df.to_excel(writer, "Backdated", index=False)

            # save the workbook
            writer.save()
            bio.seek(0)

            # get the excel file (answers my question)
            workbook    = bio.read()
            excelFile   = workbook


            storage.child('rmb_output/RMB Output.xlsx').put(excelFile)
            storage.child('rmb_output/RMB Output.xlsx').get_url(None)
            append_rmb_data  = pd.DataFrame(columns = ['Ctrbook Code', 'Profit Center', 'Ctr Date', 'Contract Term', 'Trader', 'Broker', 'Broker Reference', 'Position Type', 'Broker Account No.', 'No. Of Lots', 'Shipment Month', 'Price', 'Sub Strategy', 'Contract type', 'Trade Type', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)'])
            append_rmb_mapping  = pd.DataFrame(columns =  ['Account', 'Contract'])
            append_rmb_backdated  = pd.DataFrame(columns = ['Ctrbook Code', 'Profit Center', 'Ctr Date', 'Contract Term', 'Trader', 'Broker', 'Broker Reference', 'Position Type', 'Broker Account No.', 'No. Of Lots', 'Shipment Month', 'Price', 'Sub Strategy', 'Contract type', 'Trade Type', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)'])
            for i in range(0, len(f)):
                #print(i)
                storage.child("rmb_input/input.csv").put(f[i])
                storage_url_rmb = storage.child("rmb_input/input.csv").get_url(None)
                rmb_data, map_data, back_data = rmb(storage_url_rmb, mapping_url_rmb, i, date)

                append_rmb_data = append_rmb_data.append(rmb_data)
                append_rmb_mapping = append_rmb_mapping.append(map_data)
                append_rmb_backdated = append_rmb_backdated.append(back_data)
            #print(append_rmb_mapping)
            bio = BytesIO()
            writer = pd.ExcelWriter(bio, engine='xlsxwriter')
            # sheets
            append_rmb_data.to_excel(writer, "RMB", index=False)
            append_rmb_mapping.to_excel(writer, "Mapping Not found", index=False)
            append_rmb_backdated.to_excel(writer, "Backdated", index=False)
            # save the workbook
            writer.save()
            bio.seek(0)
            # get the excel file (answers my question)
            workbook    = bio.read()
            excelFile   = workbook
            storage.child('rmb_output/RMB Output.xlsx').put(excelFile)
            return redirect('/downloadfile/rmb')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

@app.route('/rmd', methods=["GET", "POST"])
def upload_rmd():
    return render_template('upload_rmd.html')

@app.route('/rmd_uploader', methods=["GET", "POST"])
def upload_rmd_file():
    storage_url_rmd = ''
    mapping_url_rmd = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            date = request.form['date']
            storage.child("rmd_input/input.csv").put(f)
            storage_url_rmd = storage.child("rmd_input/input.csv").get_url(None)
            print("-------------------------")
            print(storage_url_rmd)
            mapping_url_rmd = storage.child("mapping_files/Brokers_mapping.xlsx").get_url(None)
            print("-------------------------")
            print(mapping_url_rmd)
            rmd(storage_url_rmd, mapping_url_rmd, date)
            return redirect('/downloadfile/rmd')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

@app.route('/rmd_fx_uploader', methods=["GET", "POST"])
def upload_rmd_fx_file():
    storage_url_rmd = ''
    mapping_url_rmd = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            date = request.form['date']
            storage.child("rmd_input/input.csv").put(f)
            storage_url_rmd = storage.child("rmd_input/input.csv").get_url(None)
            mapping_url_rmd = storage.child("mapping_files/Brokers_mapping.xlsx").get_url(None)
            rmd_fx(storage_url_rmd, mapping_url_rmd, date)
            return redirect('/downloadfile/rmd')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

@app.route('/fcstone_uploader', methods=["GET", "POST"])
def upload_fcstone_file():
    storage_url_fcstone = ''
    mapping_url_fcstone = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            date = request.form['fcstone_date']
            storage.child("fcstone_input/input.csv").put(f)
            storage_url_fcstone = storage.child("fcstone_input/input.csv").get_url(None)
            mapping_url_fcstone = storage.child("mapping_files/Brokers_mapping.xlsx").get_url(None)
            fcstone(storage_url_fcstone, mapping_url_fcstone, date)
            return redirect('/downloadfile/fcstone')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

@app.route('/fcstone_uploader_fusion', methods=["GET", "POST"])
def upload_fcstone_file_fusion():
    storage_url_fcstone = ''
    mapping_url_fcstone = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            date = request.form['fcstone_date']
            storage.child("fcstone_input/input.csv").put(f)
            storage_url_fcstone = storage.child("fcstone_input/input.csv").get_url(None)
            mapping_url_fcstone = storage.child("mapping_file_fusion/Brokers_mapping_fusion.xlsx").get_url(None)
            fcstone(storage_url_fcstone, mapping_url_fcstone, date)
            return redirect('/downloadfile/fcstone')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

@app.route('/macquaire_uploader', methods=["GET", "POST"])
def upload_macquaire_file():
    storage_url_macquaire = ''
    mapping_url_macquaire = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            date = request.form['macquaire_date']
            storage.child("macquaire_input/input.csv").put(f)
            storage_url_macquaire = storage.child("macquaire_input/input.csv").get_url(None)
            mapping_url_macquaire = storage.child("mapping_files/Brokers_mapping.xlsx").get_url(None)
            macquaire(storage_url_macquaire, mapping_url_macquaire, date)
            return redirect('/downloadfile/macquaire')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

@app.route('/marex_uploader', methods=["GET", "POST"])
def upload_marex_file():
    storage_url_marex = ''
    mapping_url_marex = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            date = request.form['marex_date']
            storage.child("marex_input/input.csv").put(f)
            storage_url_marex = storage.child("marex_input/input.csv").get_url(None)
            mapping_url_marex = storage.child("mapping_files/Brokers_mapping.xlsx").get_url(None)
            marex(storage_url_marex, mapping_url_marex, date)
            return redirect('/downloadfile/marex')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

@app.route('/newedge_uploader', methods=["GET", "POST"])
def upload_newedge_file():
    storage_url_newedge = ''
    mapping_url_newedge = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            date = request.form['newedge_date']
            storage.child("newedge_input/input.csv").put(f)
            storage_url_newedge = storage.child("newedge_input/input.csv").get_url(None)
            mapping_url_newedge = storage.child("mapping_files/Brokers_mapping.xlsx").get_url(None)
            newedge(storage_url_newedge, mapping_url_newedge, date)
            return redirect('/downloadfile/newedge')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

@app.route('/newedge_uploader_only', methods=["GET", "POST"])
def upload_newedge_file_only():
    storage_url_newedge = ''
    mapping_url_newedge = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            date = request.form['newedge_date']
            storage.child("newedge_input/input.csv").put(f)
            storage_url_newedge = storage.child("newedge_input/input.csv").get_url(None)
            mapping_url_newedge = storage.child("mapping_files/Brokers_mapping.xlsx").get_url(None)
            newedge(storage_url_newedge, mapping_url_newedge, date)
            return redirect('/downloadfile/newedge/only')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

@app.route('/newedge_uploader_fusion', methods=["GET", "POST"])
def upload_newedge_file_fusion():
    storage_url_newedge = ''
    mapping_url_newedge = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            date = request.form['newedge_date']
            storage.child("newedge_input/input.csv").put(f)
            storage_url_newedge = storage.child("newedge_input/input.csv").get_url(None)
            mapping_url_newedge = storage.child("mapping_file_fusion/Brokers_mapping_fusion.xlsx").get_url(None)
            newedge(storage_url_newedge, mapping_url_newedge, date)
            return redirect('/downloadfile/newedge')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

@app.route('/cocoa', methods=["GET", "POST"])
def upload_cocoa():
    return render_template('upload_cocoa.html')

@app.route('/cocoa_uploader', methods=["GET", "POST"])
def upload_cocoa_file():
    storage_url_futures = ''
    storage_url_physicals = ''
    storage_url_consolidated = ''

    if request.files['file_upload_futures']:
        if request.method == 'POST':
            f = request.files['file_upload_futures']
            storage.child("cocoa_input/input_futures.xlsx").put(f)
            storage_url_futures = storage.child("cocoa_input/input_futures.xlsx").get_url(None)

    if request.files['file_upload_physicals']:
        if request.method == 'POST':
            f = request.files['file_upload_physicals']
            storage.child("cocoa_input/input_physicals.xlsx").put(f)
            storage_url_physicals = storage.child("cocoa_input/input_physicals.xlsx").get_url(None)

    if request.files['file_upload_consolidated']:
        if request.method == 'POST':
            f = request.files['file_upload_consolidated']
            storage.child("cocoa_input/input_consolidated.xlsx").put(f)
            storage_url_consolidated = storage.child("cocoa_input/input_consolidated.xlsx").get_url(None)

    if storage_url_physicals:
        print(storage_url_physicals + "  " + storage_url_futures)
        cocoa(storage_url_consolidated, storage_url_futures, storage_url_physicals)
        return redirect('/downloadfile/cocoa')

    else:
        return render_template('error.html')



@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            #fs = gcsfs.GCSFileSystem(token='google_default', project='CTRM')
            # f.save(secure_filename(f.filename))
            upload_filename = f.filename
            storage.child("larry_input/input.xlsx").put(f)
            # upload_filename = upload_filename.replace(' ', '_')
            # with fs.open('gs://ctrm-300805.appspot.com/larry_input/input.xlsx') as f:
            #     df = pd.read_csv(f)
            #     print(df)
            storage_url = storage.child("larry_input/input.xlsx").get_url(None)
            #storage.child("larry_input/input.xlsx").download("input.xlsx")
            df = pd.read_excel(storage_url, skiprows=1)
            df['Instrument Type'].ffill(inplace=True)
            df = df.iloc[ : -1, : ]
            df['Commodity'].ffill(inplace=True)
            df['Account-Origin'].ffill(inplace=True)
            df = df.drop('Unnamed: 5', axis=1)
            physical = df[df['Instrument Type'] == 'PHYSICAL']
            total_cash = pd.DataFrame(physical.select_dtypes(include = ['float64', 'int64']).sum()).transpose()
            total_cash['Instrument Type'] = 'Total Cash'
            gives_takes = pd.DataFrame(physical[(physical['Category'] == 'UnpricedSALES') | (physical['Category'] == 'UnpricedPURCHASE')]
                                    .select_dtypes(include = ['float64', 'int64']).sum()).transpose()

            gives_takes = gives_takes * -1
            gives_takes['Instrument Type'] = 'Gives/Takes'
            nfc = total_cash + gives_takes
            nfc['Instrument Type'] = 'NET FLAT CASH'
            physical_new = physical.append(total_cash).append(gives_takes).append(nfc)
            futures_options = df[df['Instrument Type'] != 'PHYSICAL']
            nft = pd.DataFrame(futures_options.select_dtypes('float64', 'int64').sum()).transpose()
            nft['Instrument Type'] = 'Net Flat Total'
            nft = nft + nfc
            nft['Instrument Type'] = 'Net Flat Total'
            final = physical_new.append(futures_options).append(nft)
            final = final.set_index(['Commodity','Instrument Type','Account-Origin'])

            dates = pd.read_excel(storage_url).columns[6:]
            dates_list=list(dates)
            str1 = []
            for x in dates_list:
                if(x != 'Totals'):
                    month = x.split('-')[1]
                    year=x.split('-')[0]
                    month_year = calendar.month_name[int(month)][:3]+'-'+year
                    str1.append(month_year)

            str1.append('Totals')
            #final.reset_index(inplace=True)
            index = 0
            month_index = 0
            for x in final.columns.values:
                if(x.find('Quantity_Calc') != -1):
                    final.columns.values[index] = str1[month_index]
                    month_index = month_index + 1
                index = index + 1

            #writer = pd.ExcelWriter(out_path , engine='xlsxwriter')
            #df.to_excel(writer, sheet_name='Sheet1')
            #final.to_excel('larry_output.xlsx')
            f_new = BytesIO()
            final.to_excel(f_new)
            f_new.seek(0)
            storage.child("larry_output/output_larry.xlsx").put(f_new)
            filename_download = storage.child("larry_output/output_larry.xlsx").get_url(None)
            print(filename_download)
            return redirect('/downloadfile/larry')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')




@app.route("/downloadfile/larry", methods = ['GET'])
def download_file():
    file_path = storage.child("larry_output/output_larry.xlsx").get_url(None)
    return render_template('download_larry.html',value=file_path)

@app.route("/downloadfile/cocoa", methods = ['GET'])
def download_file_cocoa():
    file_path = storage.child("cocoa_output/output_cocoa.xlsx").get_url(None)
    return render_template('download_cocoa.html',value=file_path)

@app.route("/downloadfile/vanguard", methods = ['GET'])
def download_file_vanguard():
    file_path = storage.child("vanguard_output/Vanguard Output.xlsx").get_url(None)
    return render_template('download_vanguard.html',value=file_path)

@app.route("/downloadfile/cjs", methods = ['GET'])
def download_file_cjs():
    file_path = storage.child("cjs_output/CJS Output.xlsx").get_url(None)
    return render_template('download_cjs.html',value=file_path)

@app.route("/downloadfile/absa", methods = ['GET'])
def download_file_absa():
    file_path = storage.child("absa_output/ABSA Output.xlsx").get_url(None)
    return render_template('download_absa.html',value=file_path)

@app.route("/downloadfile/rmb", methods = ['GET'])
def download_file_rmb():
    file_path = storage.child("rmb_output/RMB Output.xlsx").get_url(None)
    return render_template('download_rmb.html',value=file_path)

@app.route("/downloadfile/rmd", methods = ['GET'])
def download_file_rmd():
    file_path = storage.child("rmd_output/RMD Output.xlsx").get_url(None)
    return render_template('download_rmd.html',value=file_path)

@app.route("/downloadfile/fcstone", methods = ['GET'])
def download_file_fcstone():
    file_path = storage.child("fcstone_output/FCSTONE Output.xlsx").get_url(None)
    return render_template('download_fcstone.html',value=file_path)

@app.route("/downloadfile/macquaire", methods = ['GET'])
def download_file_macquaire():
    file_path = storage.child("macquaire_output/Macquaire Output.xlsx").get_url(None)
    return render_template('download_macquaire.html',value=file_path)

@app.route("/downloadfile/marex", methods = ['GET'])
def download_file_marex():
    file_path = storage.child("marex_output/Marex Output.xlsx").get_url(None)
    return render_template('download_marex.html',value=file_path)

@app.route("/downloadfile/newedge", methods = ['GET'])
def download_file_newedge():
    file_path = storage.child("newedge_output/Newedge Output.xlsx").get_url(None)
    return render_template('download_newedge.html',value=file_path)

@app.route("/downloadfile/newedge/only", methods = ['GET'])
def download_file_newedge_only():
    file_path = storage.child("newedge_output/Newedge Output.xlsx").get_url(None)
    return render_template('newedge_down.html',value=file_path)

@app.route('/market_data')
def market_data():
  return render_template('market_data.html')

@app.route('/market_data_download', methods= ["GET", "POST"])
def market_data_download():


  exl = request.files["file"]
  d = request.form['t_day']
  d = d.upper()



  storage.child("market_data/Book.xlsx").put(exl)
  storage_url = storage.child("market_data/Book.xlsx").get_url(None)
  curr = pd.read_excel(storage_url, sheet_name = "CURR")
  fut = pd.read_excel(storage_url, sheet_name = "FUT")
  fx = pd.read_excel(storage_url, sheet_name = "FX")
  ir = pd.read_excel(storage_url, sheet_name = "IR")


  #FX
  fx['Price Date *'] = d #assign_date()
  del fx['FX Pairs']
  del fx['Rates']
  fx.columns.str.match("Unnamed")
  fx = fx.loc[:,~fx.columns.str.match("Unnamed")]
  #fx = fx.loc[:, ~fx.columns.str.contains('^Unnamed')]
  fx = fx.dropna()
  index_names = fx[ fx['Tenor Product Ticker Code *'] == 'SN' ].index
  fx.drop(index_names, inplace = True)
  fx['Open *'] = fx['Settle *']
  fx['High *'] = fx['Settle *']
  fx['Low *'] = fx['Settle *']
  fx['Close *'] = fx['Settle *']
  fx = fx[['Price Index Ticker Code *', 'Price Date *','Tenor Product Ticker Code *','Open *','High *', 'Low *', 'Close *', 'Settle *']]


  #Futures
  fut = fut.dropna()
  fut['PRICEDATE'] = d #assign_date()
  fut['POPEN'] = fut['PSETTLE']
  fut['PHIGH'] = fut['PSETTLE']
  fut['PLOW'] = fut['PSETTLE']
  fut['PCLOSE'] = fut['PSETTLE']
  fut = fut[['INDEXSHORTNAME', 'PRICEDATE','CALENDARTICKERSYMBOL','POPEN','PHIGH', 'PLOW', 'PSETTLE', 'PCLOSE']]


  #Intrest Rates
  ir['Tenor Product Ticker Code'].replace('2 YEAR','2 YEARS', inplace=True)
  ir_data = {'Index Ticker Code*':['USD IR','USD IR','USD IR','USD IR','USD IR','USD IR','USD IR','USD IR','USD IR','USD IR','USD IR','USD IR','USD IR','USD IR','USD IR','USD IR','USD IR','USD IR','EUR IR','EUR IR','EUR IR','EUR IR','EUR IR','EUR IR','EUR IR','EUR IR','EUR IR','EUR IR','EUR IR','EUR IR','EUR IR','EUR IR','EUR IR','EUR IR','EUR IR','EUR IR','ZAR IR','ZAR IR','ZAR IR','ZAR IR','ZAR IR','ZAR IR','ZAR IR','ZAR IR','ZAR IR','ZAR IR','ZAR IR','ZAR IR','ZAR IR','ZAR IR','ZAR IR','ZAR IR','ZAR IR','ZAR IR','GBP IR','GBP IR','GBP IR','GBP IR','GBP IR','GBP IR','GBP IR','GBP IR','GBP IR','GBP IR','GBP IR','GBP IR','GBP IR','GBP IR','GBP IR','GBP IR','GBP IR','GBP IR','AUD IR','AUD IR','AUD IR','AUD IR','AUD IR','AUD IR','AUD IR','AUD IR','AUD IR','AUD IR','AUD IR','AUD IR', 'BRL IR','BRL IR','BRL IR','BRL IR','BRL IR','BRL IR','BRL IR','BRL IR','BRL IR', 'CAD IR','CAD IR','CAD IR','CAD IR','CAD IR','CAD IR','CAD IR','CAD IR','BWP IR','BWP IR','BWP IR','BWP IR','BWP IR','BWP IR'],
 'Tenor Product Ticker Code*':['1 WEEK','2 WEEK','1 MONTH','2 MONTHS','3 MONTHS','4 MONTHS','5 MONTHS','6 MONTHS','7 MONTHS','8 MONTHS','9 MONTHS','10 MONTHS','11 MONTHS','1 YEAR','15 MONTHS','18 MONTHS','21 MONTHS','2 YEARS','1 WEEK','2 WEEK','1 MONTH','2 MONTHS','3 MONTHS','4 MONTHS','5 MONTHS','6 MONTHS','7 MONTHS','8 MONTHS','9 MONTHS','10 MONTHS','11 MONTHS','1 YEAR','15 MONTHS','18 MONTHS','21 MONTHS','2 YEARS','1 WEEK','2 WEEK','1 MONTH','2 MONTHS','3 MONTHS','4 MONTHS','5 MONTHS','6 MONTHS','7 MONTHS','8 MONTHS','9 MONTHS','10 MONTHS','11 MONTHS','1 YEAR','15 MONTHS','18 MONTHS','21 MONTHS','2 YEARS','1 WEEK','2 WEEK','1 MONTH','2 MONTHS','3 MONTHS','4 MONTHS','5 MONTHS','6 MONTHS','7 MONTHS','8 MONTHS','9 MONTHS','10 MONTHS','11 MONTHS','1 YEAR','15 MONTHS','18 MONTHS','21 MONTHS','2 YEARS','1 WEEK','2 WEEK','1 MONTH','2 MONTHS','3 MONTHS','4 MONTHS','5 MONTHS','6 MONTHS','7 MONTHS','8 MONTHS','9 MONTHS','2 YEARS', '1 WEEK', '2 WEEK','1 MONTH','2 MONTHS','3 MONTHS','6 MONTHS','9 MONTHS','1 YEAR','2 YEARS','1 WEEK','1 MONTH','2 MONTHS','3 MONTHS','6 MONTHS','9 MONTHS','1 YEAR','2 YEARS','1 WEEK','1 MONTH','3 MONTHS','6 MONTHS','9 MONTHS','1 YEAR']}
  ir_data = pd.DataFrame(ir_data)
  ir_data['Settle*'] = np.arange(len(ir_data))
  l = []
  index =[]
  for p_name, p_ticker, p_price in zip(ir_data['Index Ticker Code*'], ir_data['Tenor Product Ticker Code*'], ir_data['Settle*']):

    for name, ticker, price in zip(ir['Index Short Name'], ir['Tenor Product Ticker Code'], ir['Popen']):

      if p_name == name and p_ticker == ticker:
        l.append(price)
        ir_data['Settle*'] = ir_data['Settle*'].replace(p_price, price)
        index.append(ir_data.index[ir_data['Settle*'] == price].tolist())
        break

      else:
        l.append(None)

  ir_data['Price Date*'] = d #assign_date()

  flat_list = []
  for sublist in index:
    for item in sublist:
      flat_list.append(item)

  test_list = list(set(flat_list))
  length = len(ir_data["Settle*"])

  not_list =[]
  for i in range(length):
    if i not in test_list:
      not_list.append(i)

  ir_data.loc[not_list,'Settle*'] = np.nan

  ir_data['Open*'] = ir_data['Settle*']
  ir_data['High*'] = ir_data['Settle*']
  ir_data['Low*'] = ir_data['Settle*']
  ir_data['Close*'] = ir_data['Settle*']

  ir_data = ir_data[['Index Ticker Code*', 'Price Date*','Tenor Product Ticker Code*','Open*','High*', 'Low*', 'Close*', 'Settle*']]

  USD_IR = ir_data.loc[ir_data["Index Ticker Code*"] == 'USD IR']
  EUR_IR = ir_data.loc[ir_data["Index Ticker Code*"] == 'EUR IR']
  ZAR_IR = ir_data.loc[ir_data["Index Ticker Code*"] == 'ZAR IR']
  GBP_IR = ir_data.loc[ir_data["Index Ticker Code*"] == 'GBP IR']
  AUD_IR = ir_data.loc[ir_data["Index Ticker Code*"] == 'AUD IR']
  BRL_IR = ir_data.loc[ir_data["Index Ticker Code*"] == 'BRL IR']
  CAD_IR = ir_data.loc[ir_data["Index Ticker Code*"] == 'CAD IR']
  BWP_IR = ir_data.loc[ir_data["Index Ticker Code*"] == 'BWP IR']


  USD_IR = USD_IR.interpolate(method ='pad', limit_direction ='forward')
  USD_IR = USD_IR.interpolate(method ='linear', limit_direction ='backward')

  EUR_IR = EUR_IR.interpolate(method ='pad', limit_direction ='forward')
  EUR_IR = EUR_IR.interpolate(method ='linear', limit_direction ='backward')

  ZAR_IR = ZAR_IR.interpolate(method ='pad', limit_direction ='forward')
  ZAR_IR = ZAR_IR.interpolate(method ='linear', limit_direction ='backward')

  GBP_IR = GBP_IR.interpolate(method ='pad', limit_direction ='forward')
  GBP_IR = GBP_IR.interpolate(method ='linear', limit_direction ='backward')

  AUD_IR = AUD_IR.interpolate(method ='pad', limit_direction ='forward')
  AUD_IR = AUD_IR.interpolate(method ='linear', limit_direction ='backward')

  BRL_IR = BRL_IR.interpolate(method ='pad', limit_direction ='forward')
  BRL_IR = BRL_IR.interpolate(method ='linear', limit_direction ='backward')

  CAD_IR = CAD_IR.interpolate(method ='pad', limit_direction ='forward')
  CAD_IR = CAD_IR.interpolate(method ='linear', limit_direction ='backward')

  BWP_IR = BWP_IR.interpolate(method ='pad', limit_direction ='forward')
  BWP_IR = BWP_IR.interpolate(method ='linear', limit_direction ='backward')

  frames = [USD_IR, EUR_IR, ZAR_IR, GBP_IR, AUD_IR, BRL_IR, CAD_IR, BWP_IR]

  ir_result = pd.concat(frames)


  # init writer
  bio         = BytesIO()
  writer      = pd.ExcelWriter(bio, engine='xlsxwriter')

  # sheets
  fx.to_excel(writer, "FX Rates")
  fut.to_excel(writer, "Futures Rates")
  ir_result.to_excel(writer, "IR Rates")

  # save the workbook
  writer.save()
  bio.seek(0)

  # get the excel file
  workbook    = bio.read()
  excelFile   = workbook


  storage.child('market_data/bb_sheet.xlsx').put(excelFile)
  fileUrl = storage.child('market_data/bb_sheet.xlsx').get_url(None)

  return render_template('market_data_download.html',value=fileUrl)

@app.route('/mapping_file_upload_download_fusion', methods=["GET", "POST"])
def mapping_file_fusion():
    fileUrl = storage.child("mapping_file_fusion/Brokers_mapping_fusion.xlsx").get_url(None)
    return render_template('mapping_file_upload_download_fusion.html', value=fileUrl)

@app.route('/mapping_upload_fusion', methods=["GET", "POST"])
def mapping_upload_fusion():

    f = request.files['file_upload']
    storage.child("mapping_file_fusion/Brokers_mapping_fusion.xlsx").put(f)
    return render_template('mapping_uploaded.html')

@app.route('/mapping_file_upload_download', methods=["GET", "POST"])
def mapping_file():
    fileUrl = storage.child("mapping_files/Brokers_mapping.xlsx").get_url(None)
    return render_template('mapping_file_upload_download.html', value=fileUrl)

@app.route('/mapping_upload', methods=["GET", "POST"])
def mapping_upload():

    f = request.files['file_upload']
    storage.child("mapping_files/Brokers_mapping.xlsx").put(f)
    return render_template('mapping_uploaded.html')

@app.route('/safex', methods=["GET", "POST"])
def safex():
    return render_template('safex.html')

@app.route('/international', methods=["GET", "POST"])
def international():
    return render_template('international.html')


@app.route('/international_fusion', methods=["GET", "POST"])
def international_fusion():
    return render_template('international_fusion.html')


#Fusion

#Mapping File Download Page
@app.route('/mapping_file_upload_download_fusion_master', methods=["GET", "POST"])
def mapping_file_upload_download_fusion_master():
    fileUrl = storage.child("fusion_mapping_files/Fusion_Mapping_Sheet.xlsx").get_url(None)
    return render_template('mapping_file_upload_download_fusion_master.html', value=fileUrl)

#Mapping file upload page
@app.route('/mapping_upload_fusion_master', methods=["GET", "POST"])
def mapping_upload_fusion_master():
    f = request.files['file_upload']
    storage.child("fusion_mapping_files/Fusion_Mapping_Sheet.xlsx").put(f)
    return render_template('mapping_uploaded.html')

#SocGen Page
@app.route('/socgen_fusion', methods=["GET", "POST"])
def socgen_fusion():
    return render_template('socgen_fusion.html')

#SocGen Future upload trigger page
@app.route('/fusion_socgen_future_uploader', methods=["GET", "POST"])
def fusion_socgen_future_uploader():
    storage_url_future_scogen = ''
    mapping_url_future_scogen = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            storage.child("fusion_socgen_uploader/future.csv").put(f)
            storage_url_future_scogen = storage.child("fusion_socgen_uploader/future.csv").get_url(None)
            mapping_url_future_scogen = storage.child("fusion_mapping_files/Fusion_Mapping_Sheet.xlsx").get_url(None)
            socgen_future(storage_url_future_scogen, mapping_url_future_scogen)
            return redirect('/downloadfile/socgenfuture')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

#SocGen Options upload trigger page
@app.route('/fusion_socgen_option_uploader', methods=["GET", "POST"])
def fusion_socgen_option_uploader():
    storage_url_option_scogen = ''
    mapping_url_option_scogen = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            storage.child("fusion_socgen_uploader/option.csv").put(f)
            storage_url_option_scogen = storage.child("fusion_socgen_uploader/option.csv").get_url(None)
            mapping_url_option_scogen = storage.child("fusion_mapping_files/Fusion_Mapping_Sheet.xlsx").get_url(None)
            socgen_option(storage_url_option_scogen, mapping_url_option_scogen)
            return redirect('/downloadfile/socgenoption')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

#Marex Page
@app.route('/marex_fusion', methods=["GET", "POST"])
def marex_fusion():
    return render_template('marex_fusion.html')

#Marex Futures upload trigger page
@app.route('/fusion_marex_future_uploader', methods=["GET", "POST"])
def fusion_marex_future_uploader():
    storage_url_future_marex = ''
    mapping_url_future_marex = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            storage.child("fusion_marex_uploader/future.csv").put(f)
            storage_url_future_marex = storage.child("fusion_marex_uploader/future.csv").get_url(None)
            mapping_url_future_marex = storage.child("fusion_mapping_files/Fusion_Mapping_Sheet.xlsx").get_url(None)
            marex_future(storage_url_future_marex, mapping_url_future_marex)
            return redirect('/downloadfile/marexfuture')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

#Marex Options upload trigger page
@app.route('/fusion_marex_option_uploader', methods=["GET", "POST"])
def fusion_marex_option_uploader():
    storage_url_option_marex = ''
    mapping_url_option_marex = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            storage.child("fusion_marex_uploader/option.csv").put(f)
            storage_url_option_marex = storage.child("fusion_marex_uploader/option.csv").get_url(None)
            mapping_url_option_marex = storage.child("fusion_mapping_files/Fusion_Mapping_Sheet.xlsx").get_url(None)
            marex_option(storage_url_option_marex, mapping_url_option_marex)
            return redirect('/downloadfile/marexoption')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

#Fcstone Page
@app.route('/fcstone_fusion', methods=["GET", "POST"])
def fcstone_fusion():
    return render_template('fcstone_fusion.html')

#FCStone Futures upload trigger page
@app.route('/fusion_fcstone_future_uploader', methods=["GET", "POST"])
def fusion_fcstone_future_uploader():
    storage_url_future_fcstone = ''
    mapping_url_future_fcstone = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            storage.child("fusion_fcstone_uploader/future.csv").put(f)
            storage_url_future_fcstone = storage.child("fusion_fcstone_uploader/future.csv").get_url(None)
            mapping_url_future_fcstone = storage.child("fusion_mapping_files/Fusion_Mapping_Sheet.xlsx").get_url(None)
            fcstone_future(storage_url_future_fcstone, mapping_url_future_fcstone)
            return redirect('/downloadfile/fcstonefuture')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

#FCStone Options upload trigger page
@app.route('/fusion_fcstone_option_uploader', methods=["GET", "POST"])
def fusion_fcstone_option_uploader():
    storage_url_option_fcstone = ''
    mapping_url_option_fcstone = ''

    if request.files['file_upload']:
        if request.method == 'POST':
            f = request.files['file_upload']
            storage.child("fusion_fcstone_uploader/option.csv").put(f)
            storage_url_option_fcstone = storage.child("fusion_fcstone_uploader/option.csv").get_url(None)
            mapping_url_option_fcstone = storage.child("fusion_mapping_files/Fusion_Mapping_Sheet.xlsx").get_url(None)
            fcstone_option(storage_url_option_fcstone, mapping_url_option_fcstone)
            return redirect('/downloadfile/fcstoneoption')

        else:
            return render_template('error.html')

    else:
        return render_template('error.html')

#Download Files
#Socgen Future
@app.route("/downloadfile/socgenfuture", methods = ['GET'])
def download_file_socgen_future():
    file_path = storage.child("fusion_socgen_uploader/Socgen_future_output.xlsx").get_url(None)
    return render_template('download_socgen_future.html',value=file_path)

#Socgen Option
@app.route("/downloadfile/socgenoption", methods = ['GET'])
def download_file_socgen_option():
    file_path = storage.child("fusion_socgen_uploader/Socgen_option_output.xlsx").get_url(None)
    return render_template('download_socgen_option.html',value=file_path)

#Marex Future
@app.route("/downloadfile/marexfuture", methods = ['GET'])
def download_file_marex_future():
    file_path = storage.child("fusion_marex_uploader/Marex_future_output.xlsx").get_url(None)
    return render_template('download_marex_future.html',value=file_path)

#Marex Option
@app.route("/downloadfile/marexoption", methods = ['GET'])
def download_file_marex_option():
    file_path = storage.child("fusion_marex_uploader/Marex_option_output.xlsx").get_url(None)
    return render_template('download_marex_option.html',value=file_path)

#Fcstone Future
@app.route("/downloadfile/fcstonefuture", methods = ['GET'])
def download_file_fcstone_future():
    file_path = storage.child("fusion_fcstone_uploader/Fcstone_future_output.xlsx").get_url(None)
    return render_template('download_fcstone_future.html',value=file_path)

#Fcstone Option
@app.route("/downloadfile/fcstoneoption", methods = ['GET'])
def download_file_fcstone_option():
    file_path = storage.child("fusion_fcstone_uploader/Fcstone_option_output.xlsx").get_url(None)
    return render_template('download_fcstone_option.html',value=file_path)


if __name__ == "__main__":
    app.secret_key='12345'
    app.run(host='0.0.0.0', port=8080, debug=True)

