import pandas as pd
import calendar
from pyrebase import pyrebase
from io import BytesIO

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

storage = firebase.storage()

def newedge(upload_filename, mapping_filename, date):
    df = pd.read_csv(upload_filename)
    map_futures = pd.read_excel(mapping_filename, sheet_name = 'new-edge-futures')
    map_options = pd.read_excel(mapping_filename, sheet_name = 'new-edge-options')
    sub_acc = pd.read_excel(mapping_filename, sheet_name = 'sub-accounts')
    sub_acc = sub_acc["Sub-Acc"].to_numpy()

    opt = pd.DataFrame(columns = ['Ctrbook Code', 'Option Term', 'Option Exercised', 'Broker', 'Broker Account No.', 'Trader', 'Profit Center', 'Strategy', 'DEFAULT', 'External Reference', 'Trader Date', 'Long/ Short', 'Call/ Put', 'Terminal Month', 'Holiday Calender', 'Strike Price', 'Premium', 'No. Of Lots', 'Broker Commission', 'Commission Currency', 'Commission Amount (Per Lot)','Exchange ID', 'Fee', 'Fee currency'])
    fut = pd.DataFrame(columns = ['Ctrbook Code*', 'Profit Center*', 'Ctr Date*', 'Contract Term*', 'Trader*', 'Broker*', 'Broker Reference', 'Position Type*', 'Broker Account No*', 'No. Of Lots*', 'Shipment Month*', 'Price*', 'Sub Strategy', 'Contract type', 'Trade Type*', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)', 'Exchange ID', 'Fee', 'Fee currency'])
    fx = pd.DataFrame(columns = ['Contract Book',	'Contract Date',	'Long/Short',	'FX Futures Term',	'Delivery Month',	'Profit Center',	'Trader',	'Strategy',	'Tag',	'Trading Broker',	'Trading Broker Account',	'Commodity',	'Clearing Broker',	'Clearing Broker Account',	'No of Contracts',	'FX Rate',	'Internal Reference',	'External Reference',	'IO Number', 'Exchange ID', 'Fee', 'Fee currency'])
    map = pd.DataFrame(columns =  ['FUT/FX or OPT','Precid', 'Pacct', 'Ptdate', 'Pbs'	,'Pqty'	,'Pexch'	,'Pctym'	,'Pfc'	,'GMIShortDesc'	,'Pstrik'	,'Psubty'	,'Ptpric'	,'Pprtpr'	,'Pcursy'	,'Pcomm'	,'Patcom'	,'Pother'	,'Patoth'	,'Pfee1'	,'Patfe1'	,'Pfee2'	,'Patfe2'	,'Pfee3'	,'Patfe3'	,'Pexbkr'	,'Pgivio'	,'Pgivf#'	,'Pspred'	,'Ptrace'	,'Pchit',	'Pmultf'])
    #backdated = pd.DataFrame(columns =  ['Precid', 'Pacct', 'Ptdate', 'Pbs'	,'Pqty'	,'Pexch'	,'Pctym'	,'Pfc'	,'GMIShortDesc'	,'Pstrik'	,'Psubty'	,'Ptpric'	,'Pprtpr'	,'Pcursy'	,'Pcomm'	,'Patcom'	,'Pother'	,'Patoth'	,'Pfee1'	,'Patfe1'	,'Pfee2'	,'Patfe2'	,'Pfee3'	,'Patfe3'	,'Pexbkr'	,'Pgivio'	,'Pgivf#'	,'Pspred'	,'Ptrace'	,'Pchit',	'Pmultf'])
    backdated_opt = pd.DataFrame(columns = ['Ctrbook Code', 'Option Term', 'Option Exercised', 'Broker', 'Broker Account No.', 'Trader', 'Profit Center', 'Strategy', 'DEFAULT', 'External Reference', 'Trader Date', 'Long/ Short', 'Call/ Put', 'Terminal Month', 'Holiday Calender', 'Strike Price', 'Premium', 'No. Of Lots', 'Broker Commission', 'Commission Currency', 'Commission Amount (Per Lot)','Exchange ID', 'Fee', 'Fee currency'])
    backdated_fut = pd.DataFrame(columns = ['Ctrbook Code*', 'Profit Center*', 'Ctr Date*', 'Contract Term*', 'Trader*', 'Broker*', 'Broker Reference', 'Position Type*', 'Broker Account No*', 'No. Of Lots*', 'Shipment Month*', 'Price*', 'Sub Strategy', 'Contract type', 'Trade Type*', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)','Exchange ID', 'Fee', 'Fee currency'])
    backdated_fx = pd.DataFrame(columns = ['Contract Book',	'Contract Date', 'Long/Short',	'FX Futures Term',	'Delivery Month',	'Profit Center',	'Trader',	'Strategy',	'Tag',	'Trading Broker',	'Trading Broker Account',	'Commodity',	'Clearing Broker',	'Clearing Broker Account',	'No of Contracts',	'FX Rate','Exchange Fee', 'Fee', 'Fee currency'])

    pos = ''
    n=4
    m=2
    for i in range(0, len(df)):
        ctr_date = df['Ptdate'].values[i]
        ctr_date = str(ctr_date)
        split = [(ctr_date[i:i+n]) for i in range(0, len(ctr_date), n)]
        split_year = split[0]
        split_month_day = split[1]
        split_month_day = [(split_month_day[i:i+m]) for i in range(0, len(split_month_day), m)]
        split_month = split_month_day[0]
        #print(split_month)
        split_month = float(split_month)
        #print(split_month)
        split_month = int(split_month)
        #print(split_month)
        #print("----------------------")
        split_month = calendar.month_abbr[split_month]
        #split_month = calendar.month_abbr[int(split_month)]
        split_month = split_month.upper()
        split_day = split_month_day[1]
        ctr_date = split_day + '-' + split_month + '-' + split_year
        #print(ctr_date)

        if df['Pacct'].values[i] in sub_acc:
            #print("Acc Not to be taken")
            continue

        elif ctr_date != date:
            #print("Backdated Trade")
            if df['Pstrik'].values[i] != 0:
                #print('option')
                comm = df['GMIShortDesc'][i]
                #print("***",comm)
                comm = comm.split(' ')
                del comm[-1]
                del comm[0]
                del comm[0]
                comm = " ".join(comm)
                comm = comm.rstrip()
                df['GMIShortDesc'][i] = comm
                #print("###",df['GMIShortDesc'][i])
                #print("###",len(df['GMIShortDesc'][i]))
                #print("-----")
                temp = map_options.loc[(map_options['New edge File Commodity Name'] == df['GMIShortDesc'][i]) & (map_options['New Edge ACC No'] == df['Pacct'][i])]
                #print(temp)
                if not temp.empty:
                    ctr_date = df['Ptdate'].values[i]
                    ctr_date = str(ctr_date)
                    split = [(ctr_date[i:i+n]) for i in range(0, len(ctr_date), n)]
                    split_year = split[0]
                    split_month_day = split[1]
                    split_month_day = [(split_month_day[i:i+m]) for i in range(0, len(split_month_day), m)]
                    split_month = split_month_day[0]
                    #print(split_month)
                    split_month = float(split_month)
                    #print(split_month)
                    split_month = int(split_month)
                    #print(split_month)
                    #print("----------------------")
                    split_month = calendar.month_abbr[split_month]
                    #split_month = calendar.month_abbr[int(split_month)]
                    split_day = split_month_day[1]
                    ctr_date = split_day + '-' + split_month + '-' + split_year

                    terminal_month = df['Pctym'].values[i]
                    terminal_month = str(terminal_month)
                    terminal_month = [(terminal_month[i:i+n]) for i in range(0, len(terminal_month), n)]
                    split_year = terminal_month[0]
                    split_month = terminal_month[1]
                    #print(split_month)
                    split_month = float(split_month)
                    #print(split_month)
                    split_month = int(split_month)
                    #print(split_month)
                    #print("----------------------")
                    split_month = calendar.month_abbr[split_month]
                    #split_month = calendar.month_abbr[int(split_month)]
                    terminal_month = split_month + ' ' + split_year

                    buy_sell = df['Pbs'].values[i]

                    if buy_sell == 1:
                        #buy_sell = 'Buy'
                        pos = 'Long'
                    else:
                        #buy_sell = 'Sell'
                        pos = 'Short'

                    put_or_call = df['Psubty'].values[i]

                    if put_or_call == 'P':
                        put_or_call = 'PUT'

                    else:
                        put_or_call = 'CALL'

                    premium = df['Ptpric'].values[i] * temp['Premium Multiplier'].values[0]
                    strike = df['Pstrik'].values[i] * temp['Strike Multiplier'].values[0]
                    lots = df['Pqty'].values[i]

                    exchange_fee = df['Ptrace'].values[i]
                    df['Pcomm'] = df['Pcomm'].astype(str)
                    df['Pfee2'] = df['Pfee2'].astype(str)
                    df['Pother'] = df['Pother'].astype(str)
                    df['Pfee1'] = df['Pfee1'].astype(str)
                    df['Pfee3'] = df['Pfee3'].astype(str)
                    df['Pcomm'] = df['Pcomm'].str.replace(',', '').astype(float)
                    df['Pfee2'] = df['Pfee2'].str.replace(',', '').astype(float)
                    df['Pother'] = df['Pother'].str.replace(',', '').astype(float)
                    df['Pfee1'] = df['Pfee1'].str.replace(',', '').astype(float)
                    df['Pfee3'] = df['Pfee3'].str.replace(',', '').astype(float)
                    fee_1 = df['Pcomm'].values[i]
                    fee_2 = df['Pfee2'].values[i]
                    fee_3 = df['Pother'].values[i]
                    fee_4 = df['Pfee1'].values[i]
                    fee_5 = df['Pfee3'].values[i]
                    fee = abs(fee_1) + abs(fee_2) + abs(fee_3) + abs(fee_4) + abs(fee_5)
                    fee_currency = df['Pcursy'].values[i]



                    back = [temp['Ctrbook Code'].values[0], temp['OptionTerm'].values[0], 'Option Exercised', 'NEWEDGE', temp['Broker Account No.'].values[0], temp['Trader'].values[0], temp['Profit Center'].values[0], temp['Sub Strategy'].values[0], 'DEFAULT', ' ', ctr_date, pos, put_or_call, terminal_month, temp['Holiday Calender'].values[0], strike, premium, lots, 'Brokerage Commission', 'USD', '0.01', exchange_fee, fee, fee_currency]
                    backdated_opt.loc[i] = back
                else:
                    not_mapped = ['OPTIONS', df['Precid'].values[i], df['Pacct'].values[i], df['Ptdate'].values[i], df['Pbs'].values[i], df['Pqty'].values[i], df['Pexch'].values[i], df['Pctym'].values[i], df['Pfc'].values[i], df['GMIShortDesc'].values[i], df['Pstrik'].values[i], df['Psubty'].values[i], df['Ptpric'].values[i], df['Pprtpr'].values[i], df['Pcursy'].values[i], df['Pcomm'].values[i], df['Patcom'].values[i], df['Pother'].values[i], df['Patoth'].values[i], df['Pfee1'].values[i], df['Patfe1'].values[i], df['Pfee2'].values[i], df['Patfe2'].values[i], df['Pfee3'].values[i], df['Patfe3'].values[i], df['Pexbkr'].values[i], df['Pgivio'].values[i], df['Pgivf#'].values[i], df['Pspred'].values[i], df['Ptrace'].values[i], df['Pchit'].values[i], df['Pmultf'].values[i]]
                    map.loc[i] = not_mapped

            elif df['Pstrik'].values[i] == 0:
                #print('futures')
                temp = map_futures.loc[(map_futures['New edge File Commodity Name'] == df['GMIShortDesc'][i]) & (map_futures['New Edge ACC No'] == df['Pacct'][i])]
                #print(map_futures['New edge File Commodity Name'])
                #print(df['GMIShortDesc'][i])
                #print(map_futures['New Edge ACC No'])
                #print(df['Pacct'][i])
                #print('++++++++++++++++++++++++++++++++++++++++++++')
                if not temp.empty:
                    if temp['Futures or FX'].values[0] == 'Futures':
                        ctr_date = df['Ptdate'].values[i]
                        ctr_date = str(ctr_date)
                        split = [(ctr_date[i:i+n]) for i in range(0, len(ctr_date), n)]
                        split_year = split[0]
                        split_month_day = split[1]
                        split_month_day = [(split_month_day[i:i+m]) for i in range(0, len(split_month_day), m)]
                        split_month = split_month_day[0]
                        #print(split_month)
                        split_month = float(split_month)
                        #print(split_month)
                        split_month = int(split_month)
                        #print(split_month)
                        #print("----------------------")
                        split_month = calendar.month_abbr[split_month]
                        #split_month = calendar.month_abbr[int(split_month)]
                        split_day = split_month_day[1]
                        ctr_date = split_day + '-' + split_month + '-' + split_year
                        #print(ctr_date)

                        terminal_month = df['Pctym'].values[i]
                        terminal_month = str(terminal_month)
                        terminal_month = [(terminal_month[i:i+n]) for i in range(0, len(terminal_month), n)]
                        split_year = terminal_month[0]
                        split_month = terminal_month[1]
                        #print(split_month)
                        split_month = float(split_month)
                        #print(split_month)
                        split_month = int(split_month)
                        #print(split_month)
                        #print("----------------------")
                        split_month = calendar.month_abbr[split_month]
                        terminal_month = split_month + ' ' + split_year
                        tag = ""
                        if df['Pacct'].values[i] == '0ET18':
                            a_string = str(split_year)
                            a_length = len(a_string)
                            year_split = str(a_string[a_length - 2: a_length])
                            #print(year_split)

                            month_split = split_month.upper()
                            #print(month_split)

                            last_date = month_split + " " + year_split

                            tic_sym = df['Pfc'].values[i]
                            curr = df['Pcursy'].values[i]
                            if tic_sym == "CY":
                                tag = curr + " " + "-" + " " + last_date
                            elif tic_sym == "LA":
                                tag = curr + " " + "-" + " " + last_date
                            #print(tag)
                            #print("================")

                        buy_sell = df['Pbs'].values[i]
                        if buy_sell == 1:
                            buy_sell = 'Buy'
                        else:
                            buy_sell = 'Sell'

                        lots = df['Pqty'].values[i]
                        #print(type(df['Ptpric'].values[i]))
                        #print(type(temp['Future Price Multiplier'].values[0]))
                        price = float(df['Ptpric'].values[i]) * temp['Future Price Multiplier'].values[0]
                        exchange_fee = df['Ptrace'].values[i]
                        df['Pcomm'] = df['Pcomm'].astype(str)
                        df['Pfee2'] = df['Pfee2'].astype(str)
                        df['Pother'] = df['Pother'].astype(str)
                        df['Pfee1'] = df['Pfee1'].astype(str)
                        df['Pfee3'] = df['Pfee3'].astype(str)
                        df['Pcomm'] = df['Pcomm'].str.replace(',', '').astype(float)
                        df['Pfee2'] = df['Pfee2'].str.replace(',', '').astype(float)
                        df['Pother'] = df['Pother'].str.replace(',', '').astype(float)
                        df['Pfee1'] = df['Pfee1'].str.replace(',', '').astype(float)
                        df['Pfee3'] = df['Pfee3'].str.replace(',', '').astype(float)
                        fee_1 = df['Pcomm'].values[i]
                        fee_2 = df['Pfee2'].values[i]
                        fee_3 = df['Pother'].values[i]
                        fee_4 = df['Pfee1'].values[i]
                        fee_5 = df['Pfee3'].values[i]
                        fee = abs(fee_1) + abs(fee_2) + abs(fee_3) + abs(fee_4) + abs(fee_5)
                        fee_currency = df['Pcursy'].values[i]


                        back = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], buy_sell, temp['Broker Account No.'].values[0], lots, terminal_month, price, temp['Sub Strategy'].values[0], '', 'FUTURES', tag, '', '', '', '', exchange_fee, fee, fee_currency]
                        backdated_fut.loc[i] = back

                    else:
                        #print('=============FX====================')

                        ctr_date = df['Ptdate'].values[i]
                        ctr_date = str(ctr_date)
                        split = [(ctr_date[i:i+n]) for i in range(0, len(ctr_date), n)]
                        split_year = split[0]
                        split_month_day = split[1]
                        split_month_day = [(split_month_day[i:i+m]) for i in range(0, len(split_month_day), m)]
                        split_month = split_month_day[0]
                        #print(split_month)
                        split_month = float(split_month)
                        #print(split_month)
                        split_month = int(split_month)
                        #print(split_month)
                        #print("----------------------")
                        split_month = calendar.month_abbr[split_month]
                        #split_month = calendar.month_abbr[int(split_month)]
                        split_day = split_month_day[1]
                        ctr_date = split_day + '-' + split_month + '-' + split_year

                        terminal_month = df['Pctym'].values[i]
                        terminal_month = str(terminal_month)
                        terminal_month = [(terminal_month[i:i+n]) for i in range(0, len(terminal_month), n)]
                        split_year = terminal_month[0]
                        split_month = terminal_month[1]
                        #print(split_month)
                        split_month = float(split_month)
                        #print(split_month)
                        split_month = int(split_month)
                        #print(split_month)
                        #print("----------------------")
                        split_month = calendar.month_abbr[split_month]
                        #split_month = calendar.month_abbr[int(split_month)]
                        terminal_month = split_month + ' ' + split_year

                        lots = df['Pqty'].values[i]
                        price = df['Ptpric'].values[i] * temp['Future Price Multiplier'].values[0]

                        buy_sell = df['Pbs'].values[i]

                        if buy_sell == 1:
                            buy_sell = 'BUY'
                            pos = 'Long'
                        else:
                            buy_sell = 'SELL'
                            pos = 'Short'
                        #print(temp['Ctrbook Code'].values[0],',', ctr_date, ',', pos, ',', temp['Contract Term'].values[0], ',', terminal_month, ',', temp['Profit Center'].values[0], ',', temp['Trader'].values[0], ',', temp['Sub Strategy'].values[0], ',', '(NA)', ',', temp['Broker'].values[0], ',', temp['Broker Reference'].values[0], ',', temp['Commodity'].values[0], ',', temp['Broker'].values[0], ',', temp['Broker Account No.'].values[0], ',', lots, ',', price)
                        #print(backdated_fx)
                        exchange_fee = df['Ptrace'].values[i]
                        df['Pcomm'] = df['Pcomm'].astype(str)
                        df['Pfee2'] = df['Pfee2'].astype(str)
                        df['Pother'] = df['Pother'].astype(str)
                        df['Pfee1'] = df['Pfee1'].astype(str)
                        df['Pfee3'] = df['Pfee3'].astype(str)
                        df['Pcomm'] = df['Pcomm'].str.replace(',', '').astype(float)
                        df['Pfee2'] = df['Pfee2'].str.replace(',', '').astype(float)
                        df['Pother'] = df['Pother'].str.replace(',', '').astype(float)
                        df['Pfee1'] = df['Pfee1'].str.replace(',', '').astype(float)
                        df['Pfee3'] = df['Pfee3'].str.replace(',', '').astype(float)
                        fee_1 = df['Pcomm'].values[i]
                        fee_2 = df['Pfee2'].values[i]
                        fee_3 = df['Pother'].values[i]
                        fee_4 = df['Pfee1'].values[i]
                        fee_5 = df['Pfee3'].values[i]
                        fee = abs(fee_1) + abs(fee_2) + abs(fee_3) + abs(fee_4) + abs(fee_5)
                        fee_currency = df['Pcursy'].values[i]

                        row = [temp['Ctrbook Code'].values[0], ctr_date, pos, temp['Contract Term'].values[0], terminal_month, temp['Profit Center'].values[0], temp['Trader'].values[0], temp['Sub Strategy'].values[0], '(NA)', temp['Broker'].values[0], temp['Broker Reference'].values[0], temp['Commodity'].values[0], temp['Broker'].values[0], temp['Broker Account No.'].values[0], lots, price, exchange_fee, fee, fee_currency]
                        backdated_fx.loc[i] = row

                else:
                    not_mapped = ['FUTURE or FX', df['Precid'].values[i], df['Pacct'].values[i], df['Ptdate'].values[i], df['Pbs'].values[i], df['Pqty'].values[i], df['Pexch'].values[i], df['Pctym'].values[i], df['Pfc'].values[i], df['GMIShortDesc'].values[i], df['Pstrik'].values[i], df['Psubty'].values[i], df['Ptpric'].values[i], df['Pprtpr'].values[i], df['Pcursy'].values[i], df['Pcomm'].values[i], df['Patcom'].values[i], df['Pother'].values[i], df['Patoth'].values[i], df['Pfee1'].values[i], df['Patfe1'].values[i], df['Pfee2'].values[i], df['Patfe2'].values[i], df['Pfee3'].values[i], df['Patfe3'].values[i], df['Pexbkr'].values[i], df['Pgivio'].values[i], df['Pgivf#'].values[i], df['Pspred'].values[i], df['Ptrace'].values[i], df['Pchit'].values[i], df['Pmultf'].values[i]]
                    map.loc[i] = not_mapped



        elif df['Pstrik'].values[i] != 0:
            #print('option')
            comm = df['GMIShortDesc'][i]
            #print("***",comm)
            comm = comm.split(' ')
            del comm[-1]
            del comm[0]
            del comm[0]
            comm = " ".join(comm)
            comm = comm.rstrip()
            df['GMIShortDesc'][i] = comm
            #print("###",df['GMIShortDesc'][i])
            #print("###",len(df['GMIShortDesc'][i]))
            #print("-----")
            temp = map_options.loc[(map_options['New edge File Commodity Name'] == df['GMIShortDesc'][i]) & (map_options['New Edge ACC No'] == df['Pacct'][i])]
            #print(temp)
            if not temp.empty:
                ctr_date = df['Ptdate'].values[i]
                ctr_date = str(ctr_date)
                split = [(ctr_date[i:i+n]) for i in range(0, len(ctr_date), n)]
                split_year = split[0]
                split_month_day = split[1]
                split_month_day = [(split_month_day[i:i+m]) for i in range(0, len(split_month_day), m)]
                split_month = split_month_day[0]
                #print(split_month)
                split_month = float(split_month)
                #print(split_month)
                split_month = int(split_month)
                #print(split_month)
                #print("----------------------")
                split_month = calendar.month_abbr[split_month]
                #split_month = calendar.month_abbr[int(split_month)]
                split_day = split_month_day[1]
                ctr_date = split_day + '-' + split_month + '-' + split_year

                terminal_month = df['Pctym'].values[i]
                terminal_month = str(terminal_month)
                terminal_month = [(terminal_month[i:i+n]) for i in range(0, len(terminal_month), n)]
                split_year = terminal_month[0]
                split_month = terminal_month[1]
                #print(split_month)
                split_month = float(split_month)
                #print(split_month)
                split_month = int(split_month)
                #print(split_month)
                #print("----------------------")
                split_month = calendar.month_abbr[split_month]
                #split_month = calendar.month_abbr[int(split_month)]
                terminal_month = split_month + ' ' + split_year

                buy_sell = df['Pbs'].values[i]

                if buy_sell == 1:
                    #buy_sell = 'Buy'
                    pos = 'Long'
                else:
                    #buy_sell = 'Sell'
                    pos = 'Short'

                put_or_call = df['Psubty'].values[i]

                if put_or_call == 'P':
                    put_or_call = 'PUT'

                else:
                    put_or_call = 'CALL'

                premium = df['Ptpric'].values[i] * temp['Premium Multiplier'].values[0]
                strike = df['Pstrik'].values[i] * temp['Strike Multiplier'].values[0]
                lots = df['Pqty'].values[i]

                exchange_fee = df['Ptrace'].values[i]
                df['Pcomm'] = df['Pcomm'].astype(str)
                df['Pfee2'] = df['Pfee2'].astype(str)
                df['Pother'] = df['Pother'].astype(str)
                df['Pfee1'] = df['Pfee1'].astype(str)
                df['Pfee3'] = df['Pfee3'].astype(str)
                df['Pcomm'] = df['Pcomm'].str.replace(',', '').astype(float)
                df['Pfee2'] = df['Pfee2'].str.replace(',', '').astype(float)
                df['Pother'] = df['Pother'].str.replace(',', '').astype(float)
                df['Pfee1'] = df['Pfee1'].str.replace(',', '').astype(float)
                df['Pfee3'] = df['Pfee3'].str.replace(',', '').astype(float)
                fee_1 = df['Pcomm'].values[i]
                fee_2 = df['Pfee2'].values[i]
                fee_3 = df['Pother'].values[i]
                fee_4 = df['Pfee1'].values[i]
                fee_5 = df['Pfee3'].values[i]
                fee = abs(fee_1) + abs(fee_2) + abs(fee_3) + abs(fee_4) + abs(fee_5)
                fee_currency = df['Pcursy'].values[i]


                row = [temp['Ctrbook Code'].values[0], temp['OptionTerm'].values[0], 'Option Exercised', 'NEWEDGE', temp['Broker Account No.'].values[0], temp['Trader'].values[0], temp['Profit Center'].values[0], temp['Sub Strategy'].values[0], 'DEFAULT', ' ', ctr_date, pos, put_or_call, terminal_month, temp['Holiday Calender'].values[0], strike, premium, lots, 'Brokerage Commission', 'USD', '0.01', exchange_fee, fee, fee_currency]
                opt.loc[i] = row

            else:
                #print('options mapping not found')
                not_mapped = ['OPTIONS', df['Precid'].values[i], df['Pacct'].values[i], df['Ptdate'].values[i], df['Pbs'].values[i], df['Pqty'].values[i], df['Pexch'].values[i], df['Pctym'].values[i], df['Pfc'].values[i], df['GMIShortDesc'].values[i], df['Pstrik'].values[i], df['Psubty'].values[i], df['Ptpric'].values[i], df['Pprtpr'].values[i], df['Pcursy'].values[i], df['Pcomm'].values[i], df['Patcom'].values[i], df['Pother'].values[i], df['Patoth'].values[i], df['Pfee1'].values[i], df['Patfe1'].values[i], df['Pfee2'].values[i], df['Patfe2'].values[i], df['Pfee3'].values[i], df['Patfe3'].values[i], df['Pexbkr'].values[i], df['Pgivio'].values[i], df['Pgivf#'].values[i], df['Pspred'].values[i], df['Ptrace'].values[i], df['Pchit'].values[i], df['Pmultf'].values[i]]
                map.loc[i] = not_mapped

        elif df['Pstrik'].values[i] == 0:
            #print('futures')
            temp = map_futures.loc[(map_futures['New edge File Commodity Name'] == df['GMIShortDesc'][i]) & (map_futures['New Edge ACC No'] == df['Pacct'][i])]
            #print(temp)
            if not temp.empty:
                if temp['Futures or FX'].values[0] == 'Futures':
                    ctr_date = df['Ptdate'].values[i]
                    ctr_date = str(ctr_date)
                    split = [(ctr_date[i:i+n]) for i in range(0, len(ctr_date), n)]
                    split_year = split[0]
                    split_month_day = split[1]
                    split_month_day = [(split_month_day[i:i+m]) for i in range(0, len(split_month_day), m)]
                    split_month = split_month_day[0]
                    #print(split_month)
                    split_month = float(split_month)
                    #print(split_month)
                    split_month = int(split_month)
                    #print(split_month)
                    #print("----------------------")
                    split_month = calendar.month_abbr[split_month]
                    #split_month = calendar.month_abbr[int(split_month)]
                    split_day = split_month_day[1]
                    ctr_date = split_day + '-' + split_month + '-' + split_year
                    #print(ctr_date)

                    terminal_month = df['Pctym'].values[i]
                    terminal_month = str(terminal_month)
                    terminal_month = [(terminal_month[i:i+n]) for i in range(0, len(terminal_month), n)]
                    split_year = terminal_month[0]
                    split_month = terminal_month[1]
                    #print(split_month)
                    split_month = float(split_month)
                    #print(split_month)
                    split_month = int(split_month)
                    #print(split_month)
                    #print("----------------------")
                    split_month = calendar.month_abbr[split_month]
                    #split_month = calendar.month_abbr[int(split_month)]
                    terminal_month = split_month + ' ' + split_year
                    tag = ""
                    if df['Pacct'].values[i] == '0ET18':
                        a_string = str(split_year)
                        a_length = len(a_string)
                        year_split = str(a_string[a_length - 2: a_length])
                        #print(year_split)
                        month_split = split_month.upper()
                        #print(month_split)
                        last_date = month_split + " " + year_split

                        tic_sym = df['Pfc'].values[i]
                        curr = df['Pcursy'].values[i]
                        if tic_sym == "CY":
                            tag = curr + " " + "-" + " " + last_date
                        elif tic_sym == "LA":
                            tag = curr + " " + "-" + " " + last_date
                        #print(tag)
                        #print("================")

                    buy_sell = df['Pbs'].values[i]
                    if buy_sell == 1:
                        buy_sell = 'Buy'
                    else:
                        buy_sell = 'Sell'

                    lots = df['Pqty'].values[i]
                    #print(type(df['Ptpric'].values[i]))
                    #print(type(temp['Future Price Multiplier'].values[0]))
                    price = float(df['Ptpric'].values[i]) * temp['Future Price Multiplier'].values[0]

                    exchange_fee = df['Ptrace'].values[i]
                    df['Pcomm'] = df['Pcomm'].astype(str)
                    df['Pfee2'] = df['Pfee2'].astype(str)
                    df['Pother'] = df['Pother'].astype(str)
                    df['Pfee1'] = df['Pfee1'].astype(str)
                    df['Pfee3'] = df['Pfee3'].astype(str)
                    df['Pcomm'] = df['Pcomm'].str.replace(',', '').astype(float)
                    df['Pfee2'] = df['Pfee2'].str.replace(',', '').astype(float)
                    df['Pother'] = df['Pother'].str.replace(',', '').astype(float)
                    df['Pfee1'] = df['Pfee1'].str.replace(',', '').astype(float)
                    df['Pfee3'] = df['Pfee3'].str.replace(',', '').astype(float)
                    fee_1 = df['Pcomm'].values[i]
                    fee_2 = df['Pfee2'].values[i]
                    fee_3 = df['Pother'].values[i]
                    fee_4 = df['Pfee1'].values[i]
                    fee_5 = df['Pfee3'].values[i]
                    fee = abs(fee_1) + abs(fee_2) + abs(fee_3) + abs(fee_4) + abs(fee_5)
                    fee_currency = df['Pcursy'].values[i]


                    row = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], buy_sell, temp['Broker Account No.'].values[0], lots, terminal_month, price, temp['Sub Strategy'].values[0], '', 'FUTURES', tag, '', '', '', '', exchange_fee, fee, fee_currency]
                    fut.loc[i] = row

                else:
                    #print('=============FX====================')

                    ctr_date = df['Ptdate'].values[i]
                    ctr_date = str(ctr_date)
                    split = [(ctr_date[i:i+n]) for i in range(0, len(ctr_date), n)]
                    split_year = split[0]
                    split_month_day = split[1]
                    split_month_day = [(split_month_day[i:i+m]) for i in range(0, len(split_month_day), m)]
                    split_month = split_month_day[0]
                    #print(split_month)
                    split_month = float(split_month)
                    #print(split_month)
                    split_month = int(split_month)
                    #print(split_month)
                    #print("----------------------")
                    split_month = calendar.month_abbr[split_month]
                    #split_month = calendar.month_abbr[int(split_month)]
                    split_day = split_month_day[1]
                    ctr_date = split_day + '-' + split_month + '-' + split_year

                    terminal_month = df['Pctym'].values[i]
                    terminal_month = str(terminal_month)
                    terminal_month = [(terminal_month[i:i+n]) for i in range(0, len(terminal_month), n)]
                    split_year = terminal_month[0]
                    split_month = terminal_month[1]
                    #print(split_month)
                    split_month = float(split_month)
                    #print(split_month)
                    split_month = int(split_month)
                    #print(split_month)
                    #print("----------------------")
                    split_month = calendar.month_abbr[split_month]
                    #split_month = calendar.month_abbr[int(split_month)]
                    terminal_month = split_month + ' ' + split_year

                    lots = df['Pqty'].values[i]
                    price = df['Ptpric'].values[i] * temp['Future Price Multiplier'].values[0]

                    buy_sell = df['Pbs'].values[i]

                    if buy_sell == 1:
                        buy_sell = 'BUY'
                        pos = 'Long'
                    else:
                        buy_sell = 'SELL'
                        pos = 'Short'

                    exchange_fee = df['Ptrace'].values[i]
                    df['Pcomm'] = df['Pcomm'].astype(str)
                    df['Pfee2'] = df['Pfee2'].astype(str)
                    df['Pother'] = df['Pother'].astype(str)
                    df['Pfee1'] = df['Pfee1'].astype(str)
                    df['Pfee3'] = df['Pfee3'].astype(str)
                    df['Pcomm'] = df['Pcomm'].str.replace(',', '').astype(float)
                    df['Pfee2'] = df['Pfee2'].str.replace(',', '').astype(float)
                    df['Pother'] = df['Pother'].str.replace(',', '').astype(float)
                    df['Pfee1'] = df['Pfee1'].str.replace(',', '').astype(float)
                    df['Pfee3'] = df['Pfee3'].str.replace(',', '').astype(float)
                    fee_1 = df['Pcomm'].values[i]
                    fee_2 = df['Pfee2'].values[i]
                    fee_3 = df['Pother'].values[i]
                    fee_4 = df['Pfee1'].values[i]
                    fee_5 = df['Pfee3'].values[i]
                    fee = abs(fee_1) + abs(fee_2) + abs(fee_3) + abs(fee_4) + abs(fee_5)
                    fee_currency = df['Pcursy'].values[i]

                    row = [temp['Ctrbook Code'].values[0], ctr_date, pos, temp['Contract Term'].values[0], terminal_month, temp['Profit Center'].values[0], temp['Trader'].values[0], temp['Sub Strategy'].values[0], '(NA)', temp['Broker'].values[0], temp['Broker Reference'].values[0], temp['Commodity'].values[0], temp['Broker'].values[0], temp['Broker Account No.'].values[0], lots, price, '', '', '', exchange_fee, fee, fee_currency]
                    fx.loc[i] = row

            else:
                #print('Futures mapping is empty')
                not_mapped = ['FUTURE or FX', df['Precid'].values[i], df['Pacct'].values[i], df['Ptdate'].values[i], df['Pbs'].values[i], df['Pqty'].values[i], df['Pexch'].values[i], df['Pctym'].values[i], df['Pfc'].values[i], df['GMIShortDesc'].values[i], df['Pstrik'].values[i], df['Psubty'].values[i], df['Ptpric'].values[i], df['Pprtpr'].values[i], df['Pcursy'].values[i], df['Pcomm'].values[i], df['Patcom'].values[i], df['Pother'].values[i], df['Patoth'].values[i], df['Pfee1'].values[i], df['Patfe1'].values[i], df['Pfee2'].values[i], df['Patfe2'].values[i], df['Pfee3'].values[i], df['Patfe3'].values[i], df['Pexbkr'].values[i], df['Pgivio'].values[i], df['Pgivf#'].values[i], df['Pspred'].values[i], df['Ptrace'].values[i], df['Pchit'].values[i], df['Pmultf'].values[i]]
                map.loc[i] = not_mapped


    # init writer

    print("heloo ------------------------------------------------")
    bio = BytesIO()
    writer = pd.ExcelWriter(bio, engine='xlsxwriter')

    # sheets
    opt.to_excel(writer, "Newedge - Options", index=False)
    fut.to_excel(writer, "Newedge - Futures", index=False)
    fx.to_excel(writer, "Newedge - FX", index=False)
    map.to_excel(writer, "Mapping Not Found", index=False)
    backdated_opt.to_excel(writer, "Backdated Options", index=False)
    backdated_fut.to_excel(writer, "Backdated Futures", index=False)
    backdated_fx.to_excel(writer, "Backdated FX", index=False)

    # save the workbook
    writer.save()
    bio.seek(0)

    # get the excel file (answers my question)
    workbook    = bio.read()
    excelFile   = workbook


    storage.child('newedge_output/Newedge Output.xlsx').put(excelFile)
    storage.child('newedge_output/Newedge Output.xlsx').get_url(None)



























































