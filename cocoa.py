import pandas as pd
from pyrebase import pyrebase
from io import BytesIO
import numpy as np

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

storage = firebase.storage()

def cocoa(consolidated, futures, physicals):
    
    df = pd.read_excel(consolidated, skiprows=2)

    new_df = pd.DataFrame(columns = ["Valuation Month", "Physical (Valuation Tag)", "Un Priced (Contract Month)", "E-18 Futures", "FC Stone Futures", "Net Futures (sum of unpriced + futures)", "Net Position"])

    for index in range(0, len(df)):
        
        loc_valuation_month = df.iloc[index]
        terminal_month = loc_valuation_month["Future Terminal Month"].split()[0].upper()
        terminal_year = loc_valuation_month["Future Terminal Month"].split()[1][2] + loc_valuation_month["Future Terminal Month"].split()[1][3]
        
        if(loc_valuation_month["Future Term"].split()[1] == "US"):
            future_term = "USD"
        else:
            future_term = loc_valuation_month["Future Term"].split()[1]
            
        valuation_month = future_term +" - "+  terminal_month + " " + terminal_year
        row = [valuation_month, "nil", "nil", "nil", "nil", "nil", "nil"]
        new_df.loc[index] = row
        
        

    df_futures = pd.read_excel(futures, skiprows=1)

    for index in range(len(df) + 1, len(df_futures)):
        
        valuation_month = df_futures["Tag"].iloc[index]
        row = [valuation_month, "nil", "nil", "nil", "nil", "nil", "nil"]
        new_df.loc[index] = row
        

    df_physicals = pd.read_excel(physicals, skiprows=1)

    for index in range(len(df_futures) + 1, len(df_physicals)):
        
        valuation_month = df_physicals["Tag"].iloc[index]
        row = [valuation_month, "nil", "nil", "nil", "nil", "nil", "nil"]
        new_df.loc[index] = row

    new_df = new_df.drop_duplicates()

    new_df = new_df.reset_index(drop=True)

    df_physicals = df_physicals.fillna(0)

    for index in range(0, len(new_df)):
        physical_valuation = 0
        for i in range(0, len(df_physicals)):
            #print("checkkk")
            #print(new_df['Valuation Month'].iloc[index])
            #print(df_physicals['Tag'].iloc[i])
            #print("print")
            #print(physical_valuation)
            #print(df_physicals['Total Quantity'].iloc[i])
            if(new_df['Valuation Month'].iloc[index] == df_physicals['Tag'].iloc[i]):
                physical_valuation = physical_valuation + df_physicals['Total Quantity'].iloc[i] 
                
                print(physical_valuation)
                print("==============================")       
        #print(physical_valuation)
                new_df.at[index, 'Physical (Valuation Tag)'] = physical_valuation


    df = df.fillna(0)

    for i in range(0, len(new_df)):
        new_valuation_month = new_df["Valuation Month"].iloc[i]
        unpriced = 0
        for index in range(0, len(df)):

            loc_valuation_month = df.iloc[index]
            terminal_month = loc_valuation_month["Future Terminal Month"].split()[0].upper()
            terminal_year = loc_valuation_month["Future Terminal Month"].split()[1][2] + loc_valuation_month["Future Terminal Month"].split()[1][3]

            if(loc_valuation_month["Future Term"].split()[1] == "US"):
                future_term = "USD"
            else:
                future_term = loc_valuation_month["Future Term"].split()[1]

            valuation_month = future_term +" - "+  terminal_month + " " + terminal_year
            if(new_valuation_month == valuation_month):
                unpriced = unpriced + df["Unrealized Quantity(Rpt UOM)(t)"].iloc[index]
        new_df.at[i, 'Un Priced (Contract Month)'] = unpriced*-1
        

    for index in range(0, len(new_df)):
        e18 = 0
        a11626 = 0
        for i in range(0, len(df_futures)):
            if(df_futures["Broker Account"].iloc[i] == "2000ET18"):
                if(new_df['Valuation Month'].iloc[index] == df_futures['Tag'].iloc[i]):
                    if(df_futures['Long Short'].iloc[i] == 'SHORT'):
                        e18 = e18 + df_futures['Trade Quantity(Ctr UOM)(t)'].iloc[i]*-1
                        
                    else:
                        e18 = e18 + df_futures['Trade Quantity(Ctr UOM)(t)'].iloc[i]
                        
            elif(df_futures["Broker Account"].iloc[i] == "11626"):
                if(new_df['Valuation Month'].iloc[index] == df_futures['Tag'].iloc[i]):
                    if(df_futures['Long Short'].iloc[i] == 'SHORT'):
                        a11626 = a11626 + df_futures['Trade Quantity(Ctr UOM)(t)'].iloc[i]*-1
                        
                    else:
                        a11626 = a11626 + df_futures['Trade Quantity(Ctr UOM)(t)'].iloc[i]
                
        new_df.at[index, 'E-18 Futures'] = e18
        new_df.at[index, 'FC Stone Futures'] = a11626
        new_df.at[index, 'Net Futures (sum of unpriced + futures)'] = e18 + a11626 + new_df['Un Priced (Contract Month)'].iloc[index]
        new_df.at[index, 'Net Position'] = new_df['Net Futures (sum of unpriced + futures)'].iloc[index] + new_df['Physical (Valuation Tag)'].iloc[index]

        
    print("------------------------------------------------")
    print(new_df)
    f_new = BytesIO()
    new_df.to_excel(f_new, index=False)
    f_new.seek(0)
    storage.child("cocoa_output/output_cocoa.xlsx").put(f_new)
    



