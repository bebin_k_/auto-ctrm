import pandas as pd
import numpy as np
import calendar
from pyrebase import pyrebase
from io import BytesIO

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

storage = firebase.storage()



def rmb(upload_filename, mapping_filename, file_number, date_1):
    
    new_df = pd.DataFrame(columns = ['Ctrbook Code', 'Profit Center', 'Ctr Date', 'Contract Term', 'Trader', 'Broker', 'Broker Reference', 'Position Type', 'Broker Account No.', 'No. Of Lots', 'Shipment Month', 'Price', 'Sub Strategy', 'Contract type', 'Trade Type', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)']) 
    new_df_1 = pd.DataFrame(columns = ['Ctrbook Code', 'Profit Center', 'Ctr Date', 'Contract Term', 'Trader', 'Broker', 'Broker Reference', 'Position Type', 'Broker Account No.', 'No. Of Lots', 'Shipment Month', 'Price', 'Sub Strategy', 'Contract type', 'Trade Type', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)']) 
    backdated = pd.DataFrame(columns = ['Ctrbook Code', 'Profit Center', 'Ctr Date', 'Contract Term', 'Trader', 'Broker', 'Broker Reference', 'Position Type', 'Broker Account No.', 'No. Of Lots', 'Shipment Month', 'Price', 'Sub Strategy', 'Contract type', 'Trade Type', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)']) 
    backdated_1 = pd.DataFrame(columns = ['Ctrbook Code', 'Profit Center', 'Ctr Date', 'Contract Term', 'Trader', 'Broker', 'Broker Reference', 'Position Type', 'Broker Account No.', 'No. Of Lots', 'Shipment Month', 'Price', 'Sub Strategy', 'Contract type', 'Trade Type', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)']) 
    map = pd.DataFrame(columns =  ['Account', 'Contract'])
    map_1 = pd.DataFrame(columns =  ['Account', 'Contract'])

    df = pd.read_csv(upload_filename)
    #date = df[:0].columns.values[1]
    date_col = df.iloc[:0].columns.values[0].split('\xa0')[6]
    day = date_col.split(' ')[0]
    month = date_col.split(' ')[1]
    month = month.upper()
    year = date_col.split(' ')[2]
    ctr_date = day + '-' + month + '-' + year

    split_shipment = ''
    shipment = ''

    

    #df_extracted = pd.DataFrame()

    map_df = pd.read_excel(mapping_filename, sheet_name='rmb')
    sub_acc = pd.read_excel(mapping_filename, sheet_name = 'sub-accounts')
    sub_acc = sub_acc["Sub-Acc"].to_numpy()


    #broker_account = df.iloc[:0].columns.values[0].split(' ')[0].split('\xa0')[0]
    #date = df.iloc[:0].columns.values[0].split(' ')[0].split('\xa0')[6]
    #month = df.iloc[:0].columns.values[0].split(' ')[1]
    #year = df.iloc[:0].columns.values[0].split(' ')[2]
    #ctr_date = date + '-' + month + '-' + year

    #temp = map_df.loc[map_df['RMB ACC No'] == broker_account]

    df = pd.read_csv(upload_filename, skiprows=2)

    account = df['Account'][0]
    #account = str(account)
    #print(account)
    if pd.isna(account):
        #print("hello")
        account = df['Account'][1]
    
    if account != 'Movement':
        a = account.split('R')
        #print(a)
        a = a[1]
        #print(a[1])
        acc = a.split('4')
        ac = acc[0]
        #print(ac)
        #print("-------")
    
        contract = df['Contract'][0]
        if pd.isna(contract):
            #print("hello")
            contract = df['Contract'][1]
        exp_date = contract.split(' ')[1]
        comm = contract.split(' ')[2]
        b_acc = str(ac) + ' ' + str(comm) + ' ' + str(exp_date) + ' ' + 'RMB'
        #print(b_acc)


        temp = map_df.loc[(map_df['Broker Account No.'] == b_acc) & (map_df['RMB ACC No'] == account)]
        #if temp.empty:
        #    print("Dataframe empty for ", account)
        #    print(b_acc)
        #print(temp)
        
    
        if df['Contract'].isnull()[0] == False:
            split_shipment = str(df['Contract'][0])
            split_shipment = split_shipment.split(' ')[1]
            shipment = split_shipment[0:3] + ' ' + '20' + split_shipment[3: 5]

        else:
            split_shipment = str(df['Contract'][1])
            split_shipment = split_shipment.split(' ')[1]
            shipment = split_shipment[0:3] + ' ' + '20' + split_shipment[3: 5]


        
        #df['ExchangeDRN'].replace('', np.nan, inplace=True)
        #df = df.dropna(subset=['ExchangeDRN'], inplace=True)

        account_array = df.loc[df['ExchangeDRN'].notnull(), :]

        if not account_array.empty:

            #print(account_array)
            print("Data Present")
            print("account",account)

            for i in range(0, len(account_array.values)):

                #print(ctr_date)
                if account in sub_acc:
                    #print("Acc Not to be taken")
                    break

                elif ctr_date != date_1:
                    #print('backdated')
                    buy_sell_value = account_array['No.Dealt'].values[i]
                    price = float(account_array['MtMDealPrice'].values[i]) * temp['Future Price Multiplier'].values[0]
                    #print(price)
                    #print(type(price))
                    if buy_sell_value > 0:
                        buy_sell = 'BUY'
                        lots = buy_sell_value
                    else:
                        buy_sell = 'SELL'
                        lots = -1*buy_sell_value
                    not_mapped = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], buy_sell, temp['Broker Account No.'].values[0], lots, shipment, price, temp['Sub Strategy'].values[0], '', 'FUTURES', '', '', '', '', '']
                    backdated.loc[i] = not_mapped
                    #backdated_1 = backdated_1.append(backdated)
                    
                    break

                elif not temp.empty:
                    buy_sell_value = account_array['No.Dealt'].values[i]
                    price = float(account_array['MtMDealPrice'].values[i]) * temp['Future Price Multiplier'].values[0]
                    #print(price)
                    #print(type(price))
                    if buy_sell_value > 0:
                        buy_sell = 'BUY'
                        lots = buy_sell_value
                    else:
                        buy_sell = 'SELL'
                        lots = -1*buy_sell_value
                    row = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], buy_sell, temp['Broker Account No.'].values[0], lots, shipment, price, temp['Sub Strategy'].values[0], '', 'FUTURES', '', '', '', '', '']
                    new_df.loc[i] = row

                    
                    #new_df_1 = new_df_1.append(new_df)
                    #print(new_df)
                    
                else:
                    #print('Account Not found in mapping sheet')
                    not_mapped = [account, b_acc]
                    map.loc[i] = not_mapped
                    #map_1 = map_1.append(map)
                    
                    #print(map_1)
                    #print("++++++++")
               
    
            #print("kk",file_number)
            #print(new_df)

        else:
            print("Account is Empty")

    return new_df, map, backdated

    #bio = BytesIO()
    #writer = pd.ExcelWriter(bio, engine='xlsxwriter')
    ## sheets
    #new_df_1.to_excel(writer, "RMB", index=False)
    #map_1.to_excel(writer, "Mapping Not found", index=False)
    #backdated_1.to_excel(writer, "Backdated", index=False)
    ## save the workbook
    #writer.save()
    #bio.seek(0)      
    ## get the excel file (answers my question)          
    #workbook    = bio.read()  
    #excelFile   = workbook
    #storage.child('rmb_output/RMB Output.xlsx').put(excelFile)
#
    ##Reading existing values and appending function
    #append_from_data = storage.child('rmb_output/RMB Output.xlsx').get_url(None)
    #append_rmb_data  = pd.DataFrame()
    #append_rmb_mapping  = pd.DataFrame()
    #append_rmb_backdated  = pd.DataFrame()
    #rmb_data = pd.read_excel(append_from_data, sheet_name='RMB')
    #rmb_mapping = pd.read_excel(append_from_data, sheet_name='Mapping Not found')
    #rmb_backdated = pd.read_excel(append_from_data, sheet_name='Backdated')
    #append_rmb_data.append(rmb_data)
    #append_rmb_mapping.append(rmb_mapping)
    #append_rmb_backdated.append(rmb_backdated)
    #bio = BytesIO()
    #writer = pd.ExcelWriter(bio, engine='xlsxwriter')
    ## sheets
    #append_rmb_data.to_excel(writer, "RMB", index=False)
    #append_rmb_mapping.to_excel(writer, "Mapping Not found", index=False)
    #append_rmb_backdated.to_excel(writer, "Backdated", index=False)
    ## save the workbook
    #writer.save()
    #bio.seek(0)      
    ## get the excel file (answers my question)          
    #workbook    = bio.read()  
    #excelFile   = workbook
    #storage.child('rmb_output/RMB Output.xlsx').put(excelFile)

    

