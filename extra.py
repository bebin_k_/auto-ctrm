# import pandas as pd
# import calendar
# from pyrebase import pyrebase
# from io import BytesIO
# import numpy as np

# firebaseConfig = {
#     'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
#     'authDomain': "ctrm-300805.firebaseapp.com",
#     'databaseURL': "ctrm-300805",
#     'storageBucket': "ctrm-300805.appspot.com",
#     'messagingSenderId': "883905439975",
#     'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
# }

# firebase = pyrebase.initialize_app(firebaseConfig)

# storage = firebase.storage()

# def marex_future(upload_filename, mapping_filename):
#     print("hello world futures")

#     #making instrument match the output fiels with broker name as prefix
#     data = [['Société Generale', 'SocGen'], ['INTL FCStone', 'FCStone'],['MAREX SPECTRON','Marex']]
#     b_df = pd.DataFrame(data, columns=['Broker','code'])

#     # code for years
#     yr_df = pd.read_excel(mapping_filename, sheet_name = "year")
#     yr_df['Yr']=yr_df['Yr'].astype('str')
#     yr_df['Year']=yr_df['Year'].astype('str')
#     print(yr_df.dtypes)
#     print(yr_df)    

# #    months=pd.read_csv("D:\Market Prices\MONTHS.csv")
#     months=pd.read_excel(mapping_filename, sheet_name = "months")
#     months.head()
    
#     accounts= pd.read_excel(mapping_filename, sheet_name = "accounts")
#     accounts=pd.merge(accounts,b_df, on ='Broker', how ='inner')   #inner join to match all the broker code names
#     t1 = accounts["code"].isin(['Marex'])
#     accounts = accounts[t1]
#     accounts = pd.DataFrame(accounts)

#     #Instrument coded file
#     ins= pd.read_excel(mapping_filename, sheet_name = "ins_code")

#    # multiplier Dataset
#     Marex_mf = pd.read_excel(mapping_filename, sheet_name = "marex_futures") #directory                           #sheets
#     #Marex_mf= pd.read_excel(xls, 'Marex Futures')
#     Marex_mf.rename(columns = {'CTRM Commodity':'Index'}, inplace = True)

# #    df=pd.read_csv(upload_filename, header=2)
#     df=pd.read_csv(upload_filename)
# #    print("ORIGINAL DATATFRAME",df.shape)
    
#     #print(df)
#     #print(list(df.columns.values))
# #    df = df.dropna(axis=1, how='all')
    
    
#     ##df = pd.read_excel(upload_filename)
#     #filtering based on broker account for MArex, and to get the future prices 
#     x = df["Ledger Code"].isin([8968,8979])
#     x2 = ~df["Option Type"].isin(["C","P"])
#     df2=df[x & x2]
    
#     print("first check")
#     print(df2.shape)
    

#     final = df2[["Instrument Long Name","Delivery/Prompt date","Market Rate"]]
#     final.rename(columns = {'Instrument Long Name':'Index'}, inplace = True)
#     # remove duplicates
#     y=final.drop_duplicates()
#     print(len(y))
#     print(y.shape)
    

#     test = pd.DataFrame()
#     test = y

# #    y['Index'].unique()

#     #inner join to match all the index of FCStone to the corresponding MULTIPLIERS
#     y = pd.merge(y, Marex_mf, on ='Index', how ='inner')

#     print("AFTER MERGE 1 :-", len(y))

#     #Splitting Futures Description Column data into Month - Year - Instrument
#     y[['Year','Number','Date']] = y["Delivery/Prompt date"].str.split('/', n=2 , expand=True)

#     print("check for shape", y.shape)
# #    y['Index'].unique()
#     print(y)

#     print("@@@@@@@@@@@@@@@@@@@@@@@")

#     #add a column with static value 1 
#     y.insert(0, "dd", "1")
#     print(y)

# #    y['Year']=y['Year'].astype('int64')
#     print("check for types", y.dtypes)


#     y = pd.merge(y,yr_df , on ='Year', how ='inner')
#     print(y.shape)
#     print(y)


#     # removing the extra 0 infront of months - for mapping 
#     y["Number"] = y["Number"].apply(lambda x: x.lstrip('0'))
#     y

#     y['Number']=y['Number'].astype('int64')

#     #mapp for months

#     y = pd.merge(y, months , on ='Number', how ='inner')
#     y.head()


#     print("AFTER MERGE 2", len(y))    

#     # concat columns by separator to create DATE column to match Fusion format
#     y['Date']=y['dd'].str.cat(y[['months_sm','Yr']], sep='/')

#     #inner join to match all the index code to the corresponding instrument names
#     joined = pd.merge(y, ins, on ='Index', how ='inner')

#     print("---------------------------------------")
#     print(joined)

#     xyz= "Marex" + " " + joined['Ins_full'] + " " + ">>"
#     joined['Instrument'] = xyz

#     floatArray = np.asfarray(joined['Market Rate'], dtype = float)

#     joined['change_MR']= floatArray

# #    joined['Index'].unique()

# #    print(floatArray)
# #    floatArray = np.asfarray(joined['Market Rate'][0], dtype = float)

#     # Closing price with multiplier
#     joined['CP_with_multiplier']= joined['Future Price Multiplier']*joined['change_MR']



#     #---------------------FcStone--------------------------
#     fcstone_url = storage.child('fusion_fcstone_uploader/Fcstone_future_output.xlsx').get_url(None)

#     op_FCstone= pd.read_excel(fcstone_url)
#     op_FCstone.head()

#     op_copy = op_FCstone.copy()
#     op_ind = op_copy['Months'].values.tolist()
#     op_copy.set_index('Months', inplace=True)

#     joined_records = joined.to_dict('record')
#     for row in joined_records:
#         try:
#             op_copy.at[row['Date'], row['Instrument']]
#             op_copy.at[row['Date'], row['Instrument']] = row['CP_with_multiplier']
#         except KeyError:
#             continue   

#     op_copy.reset_index(inplace=True)
#     # download the file as csv
#     #op_copy.to_csv("C:\\Users\\saumya.joshi\\Downloads\\Market Prices\\csv exports daily\\Output_Marex.csv", index=False)


#     # init writer
#     bio = BytesIO()
#     writer = pd.ExcelWriter(bio, engine='xlsxwriter')
#     # sheets
#     op_copy.to_excel(writer, "Marex - Futures", index=False)
#     test.to_excel(writer, "Marex Unique Values", index=False)

#     # save the workbook
#     writer.save()
#     bio.seek(0)      

#     # get the excel file (answers my question)          
#     workbook    = bio.read()  
#     excelFile   = workbook


#     storage.child('fusion_marex_uploader/Marex_future_output.xlsx').put(excelFile)
#     storage.child('fusion_marex_uploader/Marex_future_output.xlsx').get_url(None)