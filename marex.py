import pandas as pd
import calendar
from pyrebase import pyrebase
from io import BytesIO

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

storage = firebase.storage()

def marex(upload_filename, mapping_filename, date):

    df = pd.read_excel(upload_filename, sheet_name = "TRADE CONFIRMATION",  skiprows=2)
    map_futures = pd.read_excel(mapping_filename, sheet_name = 'marex-futures')
    map_options = pd.read_excel(mapping_filename, sheet_name = 'marex-options')
    sub_acc = pd.read_excel(mapping_filename, sheet_name = 'sub-accounts')
    sub_acc = sub_acc["Sub-Acc"].to_numpy()

    opt = pd.DataFrame(columns = ['Ctrbook Code', 'Option Term', 'Option Exercised', 'Broker', 'Broker Account No.', 'Trader', 'Profit Center', 'Strategy', 'DEFAULT', 'External Reference', 'Trader Date', 'Long/ Short', 'Call/ Put', 'Terminal Month', 'Holiday Calender', 'Strike Price', 'Premium', 'No. Of Lots', 'Broker Commission', 'Commission Currency', 'Commission Amount (Per Lot)'])
    fut = pd.DataFrame(columns = ['Ctrbook Code*', 'Profit Center*', 'Ctr Date*', 'Contract Term*', 'Trader*', 'Broker*', 'Broker Reference', 'Position Type*', 'Broker Account No*', 'No. Of Lots*', 'Shipment Month*', 'Price*', 'Sub Strategy', 'Contract type', 'Trade Type*', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)'])
    fx = pd.DataFrame(columns = ['Contract Book',	'Contract Date',	'Long/Short',	'FX Futures Term',	'Delivery Month',	'Profit Center',	'Trader',	'Strategy',	'Tag',	'Trading Broker',	'Trading Broker Account',	'Commodity',	'Clearing Broker',	'Clearing Broker Account',	'No of Contracts',	'FX Rate',	'Internal Reference',	'External Reference',	'IO Number'])
    map = pd.DataFrame(columns =  ['Latest Run Date',	'Entry Date',	'Trade Date',	'Ledger Code',	'Ledger Name',	'Trade Status',	'Delivery/Prompt Date',	'Market Code',	'Instrument Code',	'Instrument Long Name',	'Currency Code',	'Option Type',	'Trade Type',	'Buy/Sell (Cleared)',	'Transaction Volume (Cleared)',	'Price',	'Strike',	'Trade ID',	'RANsys Serial Number',	'Charge Currency',	'Net Charges (sum)'])
    delete_positions = pd.DataFrame(columns =  ['Latest Run Date',	'Entry Date',	'Trade Date',	'Ledger Code',	'Ledger Name',	'Trade Status',	'Delivery/Prompt Date',	'Market Code',	'Instrument Code',	'Instrument Long Name',	'Currency Code',	'Option Type',	'Trade Type',	'Buy/Sell (Cleared)',	'Transaction Volume (Cleared)',	'Price',	'Strike',	'Trade ID',	'RANsys Serial Number',	'Charge Currency',	'Net Charges (sum)'])
    #backdated= pd.DataFrame(columns =  ['Latest Run Date',	'Entry Date',	'Trade Date',	'Ledger Code',	'Ledger Name',	'Trade Status',	'Delivery/Prompt Date',	'Market Code',	'Instrument Code',	'Instrument Long Name',	'Currency Code',	'Option Type',	'Trade Type',	'Buy/Sell (Cleared)',	'Transaction Volume (Cleared)',	'Price',	'Strike',	'Trade ID',	'RANsys Serial Number',	'Charge Currency',	'Net Charges (sum)'])
    backdated_opt = pd.DataFrame(columns = ['Ctrbook Code', 'Option Term', 'Option Exercised', 'Broker', 'Broker Account No.', 'Trader', 'Profit Center', 'Strategy', 'DEFAULT', 'External Reference', 'Trader Date', 'Long/ Short', 'Call/ Put', 'Terminal Month', 'Holiday Calender', 'Strike Price', 'Premium', 'No. Of Lots', 'Broker Commission', 'Commission Currency', 'Commission Amount (Per Lot)'])
    backdated_fut = pd.DataFrame(columns = ['Ctrbook Code*', 'Profit Center*', 'Ctr Date*', 'Contract Term*', 'Trader*', 'Broker*', 'Broker Reference', 'Position Type*', 'Broker Account No*', 'No. Of Lots*', 'Shipment Month*', 'Price*', 'Sub Strategy', 'Contract type', 'Trade Type*', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)'])
    backdated_fx = pd.DataFrame(columns = ['Contract Book',	'Contract Date',	'Long/Short',	'FX Futures Term',	'Delivery Month',	'Profit Center',	'Trader',	'Strategy',	'Tag',	'Trading Broker',	'Trading Broker Account',	'Commodity',	'Clearing Broker',	'Clearing Broker Account',	'No of Contracts',	'FX Rate',	'Internal Reference',	'External Reference',	'IO Number'])
    
    pos = ''
    opt_type_in = ''
    n = 10
    lot = 0
    df['Transaction Volume (Cleared)'] = abs(df['Transaction Volume (Cleared)'])
    for i in range(0, len(df)): 
        ctr_date = str(df['Trade Date'].values[i])
        split_date = [(ctr_date[i:i+n]) for i in range(0, len(ctr_date), n)]
        split_date = split_date[0]
        year = split_date.split('-')[0]
        month = split_date.split('-')[1]
        month = calendar.month_abbr[int(month)]
        month = month.upper()
        dt = split_date.split('-')[2]
        ctr_date = dt + '-' + month + '-' + str(year)

        if df['Ledger Code'].values[i] in sub_acc:
            #print("Acc Not to be taken")
            continue

        elif df['Trade Type'].values[i] != 'FUTURE':
            if df['Trade Status'].values[i]  == 'INS':
                if ctr_date == date:
                    #print('options')
                    temp = map_options.loc[(map_options['Marex File Commodity Name'] == df['Instrument Long Name'][i]) & (map_options['Marex ACC No'] == df['Ledger Code'][i])]
                    #print(temp)
                    if not temp.empty:
                        opt_type = df['Option Type'].values[i]
                        if opt_type == 'C':
                            opt_type_in = 'CALL'
                        else:
                            opt_type_in = 'PUT'


                    
                        buy_sell = df['Buy/Sell (Cleared)'].values[i]

                        if buy_sell == 'Buy':
                            pos = 'Long'
                        else:
                            pos = 'Short'

                        premium = df['Price'].values[i] * temp['Premium Multiplier'].values[0]
                        strike = df['Strike'].values[i] * temp['Strike Multiplier'].values[0]
                        lots = df['Transaction Volume (Cleared)'].values[i]

                        terminal_month = str(df['Delivery/Prompt Date'].values[i])
                        terminal_month = terminal_month.split(' ')
                        terminal_month = terminal_month[0]
                        year = terminal_month.split('-')[0]
                        month = terminal_month.split('-')[1]
                        month = calendar.month_abbr[int(month)]
                        terminal_month = month + ' ' + str(year)

                        row = [temp['Ctrbook Code'].values[0], temp['Contract Term'].values[0], 'Option Exercised', 'MAREX', temp['Broker Account No.'].values[0], temp['Trader'].values[0], temp['Profit Center'].values[0], temp['Sub Strategy'].values[0], 'DEFAULT', ' ', ctr_date, pos, opt_type_in, terminal_month, temp['Holiday Calender'].values[0], strike, premium, lots, 'Brokerage Commission', 'USD', '0.01']
                        opt.loc[i] = row

                    else:
                        #print("Option not mapped")
                        not_mapped = [df['Latest Run Date'].values[i], df['Entry Date'].values[i], df['Trade Date'].values[i], df['Ledger Code'].values[i], df['Ledger Name'].values[i], df['Trade Status'].values[i], df['Delivery/Prompt Date'].values[i], df['Market Code'].values[i], df['Instrument Code'].values[i], df['Instrument Long Name'].values[i], df['Currency Code'].values[i], df['Option Type'].values[i], df['Trade Type'].values[i], df['Buy/Sell (Cleared)'].values[i], df['Transaction Volume (Cleared)'].values[i], df['Price'].values[i], df['Strike'].values[i], df['Trade ID'].values[i], df['RANsys Serial Number'].values[i], df['Charge Currency'].values[i], df['Net Charges (sum)'].values[i]]
                        map.loc[i] = not_mapped

                else:
                    #print('backdated')
                    #print('options')
                    temp = map_options.loc[(map_options['Marex File Commodity Name'] == df['Instrument Long Name'][i]) & (map_options['Marex ACC No'] == df['Ledger Code'][i])]
                    #print(temp)
                    if not temp.empty:

                        opt_type = df['Option Type'].values[i]

                        if opt_type == 'C':
                            opt_type_in = 'CALL'
                        else:
                            opt_type_in = 'PUT'

                        buy_sell = df['Buy/Sell (Cleared)'].values[i]

                        if buy_sell == 'Buy':
                            pos = 'Long'
                        else:
                            pos = 'Short'

                        premium = df['Price'].values[i] * temp['Premium Multiplier'].values[0]
                        strike = df['Strike'].values[i] * temp['Strike Multiplier'].values[0]
                        lots = df['Transaction Volume (Cleared)'].values[i]

                        terminal_month = str(df['Delivery/Prompt Date'].values[i])
                        terminal_month = terminal_month.split(' ')
                        terminal_month = terminal_month[0]
                        year = terminal_month.split('-')[0]
                        month = terminal_month.split('-')[1]
                        month = calendar.month_abbr[int(month)]
                        terminal_month = month + ' ' + str(year)

                        row = [temp['Ctrbook Code'].values[0], temp['OptionTerm'].values[0], 'Option Exercised', 'MAREX', temp['Broker Account No.'].values[0], temp['Trader'].values[0], temp['Profit Center'].values[0], temp['Sub Strategy'].values[0], 'DEFAULT', ' ', ctr_date, pos, opt_type_in, terminal_month, temp['Holiday Calender'].values[0], strike, premium, lots, 'Brokerage Commission', 'USD', '0.01']
                        backdated_opt.loc[i] = row

                    else:
                        #print("Option not mapped")
                        not_mapped = [df['Latest Run Date'].values[i], df['Entry Date'].values[i], df['Trade Date'].values[i], df['Ledger Code'].values[i], df['Ledger Name'].values[i], df['Trade Status'].values[i], df['Delivery/Prompt Date'].values[i], df['Market Code'].values[i], df['Instrument Code'].values[i], df['Instrument Long Name'].values[i], df['Currency Code'].values[i], df['Option Type'].values[i], df['Trade Type'].values[i], df['Buy/Sell (Cleared)'].values[i], df['Transaction Volume (Cleared)'].values[i], df['Price'].values[i], df['Strike'].values[i], df['Trade ID'].values[i], df['RANsys Serial Number'].values[i], df['Charge Currency'].values[i], df['Net Charges (sum)'].values[i]]
                        map.loc[i] = not_mapped
                  

            else:
                #print("delete positions")
                not_mapped = [df['Latest Run Date'].values[i], df['Entry Date'].values[i], df['Trade Date'].values[i], df['Ledger Code'].values[i], df['Ledger Name'].values[i], df['Trade Status'].values[i], df['Delivery/Prompt Date'].values[i], df['Market Code'].values[i], df['Instrument Code'].values[i], df['Instrument Long Name'].values[i], df['Currency Code'].values[i], df['Option Type'].values[i], df['Trade Type'].values[i], df['Buy/Sell (Cleared)'].values[i], df['Transaction Volume (Cleared)'].values[i], df['Price'].values[i], df['Strike'].values[i], df['Trade ID'].values[i], df['RANsys Serial Number'].values[i], df['Charge Currency'].values[i], df['Net Charges (sum)'].values[i]]
                delete_positions.loc[i] = not_mapped
    
        elif df['Trade Type'].values[i] == 'FUTURE':
            if df['Trade Status'].values[i] == 'INS':
                if ctr_date == date:
                    #print('futures')
                    temp = map_futures.loc[(map_futures['Marex File Commodity Name'] == df['Instrument Long Name'][i]) & (map_futures['Marex ACC No'] == df['Ledger Code'][i])]
                    #print(temp)

                    if not temp.empty:
                        if temp['Futures or FX'].values[0] == 'Futures':
                            ctr_date = str(df['Trade Date'].values[i])
                            split_date = [(ctr_date[i:i+n]) for i in range(0, len(ctr_date), n)]
                            split_date = split_date[0]
                            year = split_date.split('-')[0]
                            month = split_date.split('-')[1]
                            month = calendar.month_abbr[int(month)]
                            dt = split_date.split('-')[2]
                            ctr_date = dt + '-' + month + '-' + str(year)

                            terminal_month = str(df['Delivery/Prompt Date'].values[i])
                            terminal_month = terminal_month.split(' ')
                            terminal_month = terminal_month[0]
                            year = terminal_month.split('-')[0]
                            month = terminal_month.split('-')[1]
                            month = calendar.month_abbr[int(month)]
                            terminal_month = month + ' ' + str(year)

                            price = df['Price'].values[i] * temp['Future Price Multiplier'].values[0]
                            buy_sell = df['Buy/Sell (Cleared)'].values[i]
                            buy_sell = buy_sell.upper()

                            lots = df['Transaction Volume (Cleared)'].values[i]

                            row = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], buy_sell, temp['Broker Account No.'].values[0], lots, terminal_month, price, temp['Sub Strategy'].values[0], '', 'FUTURES', '', '', '', '', '']
                            fut.loc[i] = row
                        
                        else:
                            ctr_date = str(df['Trade Date'].values[i])
                            split_date = [(ctr_date[i:i+n]) for i in range(0, len(ctr_date), n)]
                            split_date = split_date[0]
                            year = split_date.split('-')[0]
                            month = split_date.split('-')[1]
                            month = calendar.month_abbr[int(month)]
                            dt = split_date.split('-')[2]
                            ctr_date = dt + '-' + month + '-' + str(year)

                            terminal_month = str(df['Delivery/Prompt Date'].values[i])
                            terminal_month = terminal_month.split(' ')
                            terminal_month = terminal_month[0]
                            year = terminal_month.split('-')[0]
                            month = terminal_month.split('-')[1]
                            month = calendar.month_abbr[int(month)]
                            terminal_month = month + ' ' + str(year)

                            price = df['Price'].values[i] * temp['Future Price Multiplier'].values[0]
                            buy_sell = df['Buy/Sell (Cleared)'].values[i]
                            buy_sell = buy_sell.upper()
                            

                            if buy_sell == 'BUY':
                                pos = 'Long'
                            else:
                                pos = 'Short'

                            lots = df['Transaction Volume (Cleared)'].values[i]

                            row = [temp['Ctrbook Code'].values[0],ctr_date, pos, temp['Contract Term'].values[0], terminal_month, temp['Profit Center'].values[0], temp['Trader'].values[0], temp['Sub Strategy'].values[0], '(NA)', temp['Broker'].values[0], temp['Broker Reference'].values[0], temp['Commodity'].values[0], temp['Broker'].values[0], temp['Broker Account No.'].values[0], lots, price, '', '', '']
                            fx.loc[i] = row


                    else:
                        #print("Future and FX not mapped")
                        not_mapped = [df['Latest Run Date'].values[i], df['Entry Date'].values[i], df['Trade Date'].values[i], df['Ledger Code'].values[i], df['Ledger Name'].values[i], df['Trade Status'].values[i], df['Delivery/Prompt Date'].values[i], df['Market Code'].values[i], df['Instrument Code'].values[i], df['Instrument Long Name'].values[i], df['Currency Code'].values[i], df['Option Type'].values[i], df['Trade Type'].values[i], df['Buy/Sell (Cleared)'].values[i], df['Transaction Volume (Cleared)'].values[i], df['Price'].values[i], df['Strike'].values[i], df['Trade ID'].values[i], df['RANsys Serial Number'].values[i], df['Charge Currency'].values[i], df['Net Charges (sum)'].values[i]]
                        map.loc[i] = not_mapped

                else:
                    #print('backdated')
                    #print('futures')
                    temp = map_futures.loc[(map_futures['Marex File Commodity Name'] == df['Instrument Long Name'][i]) & (map_futures['Marex ACC No'] == df['Ledger Code'][i])]
                    #print(temp)

                    if not temp.empty:
                        if temp['Futures or FX'].values[0] == 'Futures':
                            ctr_date = str(df['Trade Date'].values[i])
                            split_date = [(ctr_date[i:i+n]) for i in range(0, len(ctr_date), n)]
                            split_date = split_date[0]
                            year = split_date.split('-')[0]
                            month = split_date.split('-')[1]
                            month = calendar.month_abbr[int(month)]
                            dt = split_date.split('-')[2]
                            ctr_date = dt + '-' + month + '-' + str(year)

                            terminal_month = str(df['Delivery/Prompt Date'].values[i])
                            terminal_month = terminal_month.split(' ')
                            terminal_month = terminal_month[0]
                            year = terminal_month.split('-')[0]
                            month = terminal_month.split('-')[1]
                            month = calendar.month_abbr[int(month)]
                            terminal_month = month + ' ' + str(year)

                            price = df['Price'].values[i] * temp['Future Price Multiplier'].values[0]
                            buy_sell = df['Buy/Sell (Cleared)'].values[i]
                            buy_sell = buy_sell.upper()

                            lots = df['Transaction Volume (Cleared)'].values[i]

                            not_mapped = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], buy_sell, temp['Broker Account No.'].values[0], lots, terminal_month, price, temp['Sub Strategy'].values[0], '', 'FUTURES', '', '', '', '', '']
                            backdated_fut.loc[i] = not_mapped
                        
                        else:
                            ctr_date = str(df['Trade Date'].values[i])
                            split_date = [(ctr_date[i:i+n]) for i in range(0, len(ctr_date), n)]
                            split_date = split_date[0]
                            year = split_date.split('-')[0]
                            month = split_date.split('-')[1]
                            month = calendar.month_abbr[int(month)]
                            dt = split_date.split('-')[2]
                            ctr_date = dt + '-' + month + '-' + str(year)

                            terminal_month = str(df['Delivery/Prompt Date'].values[i])
                            terminal_month = terminal_month.split(' ')
                            terminal_month = terminal_month[0]
                            year = terminal_month.split('-')[0]
                            month = terminal_month.split('-')[1]
                            month = calendar.month_abbr[int(month)]
                            terminal_month = month + ' ' + str(year)

                            price = df['Price'].values[i] * temp['Future Price Multiplier'].values[0]
                            buy_sell = df['Buy/Sell (Cleared)'].values[i]
                            buy_sell = buy_sell.upper()
                            

                            if buy_sell == 'BUY':
                                pos = 'Long'
                            else:
                                pos = 'Short'

                            lots = df['Transaction Volume (Cleared)'].values[i]

                            not_mapped = [temp['Ctrbook Code'].values[0],ctr_date, pos, temp['Contract Term'].values[0], terminal_month, temp['Profit Center'].values[0], temp['Trader'].values[0], temp['Sub Strategy'].values[0], '(NA)', temp['Broker'].values[0], temp['Broker Reference'].values[0], temp['Commodity'].values[0], temp['Broker'].values[0], temp['Broker Account No.'].values[0], lots, price, '', '', '']
                            backdated_fx.loc[i] = not_mapped

                    else:
                        #print("Future and FX not mapped")
                        not_mapped = [df['Latest Run Date'].values[i], df['Entry Date'].values[i], df['Trade Date'].values[i], df['Ledger Code'].values[i], df['Ledger Name'].values[i], df['Trade Status'].values[i], df['Delivery/Prompt Date'].values[i], df['Market Code'].values[i], df['Instrument Code'].values[i], df['Instrument Long Name'].values[i], df['Currency Code'].values[i], df['Option Type'].values[i], df['Trade Type'].values[i], df['Buy/Sell (Cleared)'].values[i], df['Transaction Volume (Cleared)'].values[i], df['Price'].values[i], df['Strike'].values[i], df['Trade ID'].values[i], df['RANsys Serial Number'].values[i], df['Charge Currency'].values[i], df['Net Charges (sum)'].values[i]]
                        map.loc[i] = not_mapped

            else:
                #print("delete positions")
                not_mapped = [df['Latest Run Date'].values[i], df['Entry Date'].values[i], df['Trade Date'].values[i], df['Ledger Code'].values[i], df['Ledger Name'].values[i], df['Trade Status'].values[i], df['Delivery/Prompt Date'].values[i], df['Market Code'].values[i], df['Instrument Code'].values[i], df['Instrument Long Name'].values[i], df['Currency Code'].values[i], df['Option Type'].values[i], df['Trade Type'].values[i], df['Buy/Sell (Cleared)'].values[i], df['Transaction Volume (Cleared)'].values[i], df['Price'].values[i], df['Strike'].values[i], df['Trade ID'].values[i], df['RANsys Serial Number'].values[i], df['Charge Currency'].values[i], df['Net Charges (sum)'].values[i]]
                delete_positions.loc[i] = not_mapped

        # init writer
        bio = BytesIO()
        writer = pd.ExcelWriter(bio, engine='xlsxwriter')

        # sheets
        opt.to_excel(writer, "Marex - Options", index=False)
        fut.to_excel(writer, "Marex - Futures", index=False)
        fx.to_excel(writer, "Marex - FX", index=False)
        delete_positions.to_excel(writer, "Delete POS", index=False)
        map.to_excel(writer, "Mapping Not Found", index=False)
        backdated_opt.to_excel(writer, "Backdated Options", index=False)
        backdated_fut.to_excel(writer, "Backdated Futures", index=False)
        backdated_fx.to_excel(writer, "Backdated FX", index=False)

    # save the workbook
    writer.save()
    bio.seek(0)      

    # get the excel file (answers my question)          
    workbook    = bio.read()  
    excelFile   = workbook


    storage.child('marex_output/Marex Output.xlsx').put(excelFile)
    storage.child('marex_output/Marex Output.xlsx').get_url(None)