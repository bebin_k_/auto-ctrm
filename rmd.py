from numpy.core.numeric import NaN
import pandas as pd
from pyrebase import pyrebase
from io import BytesIO
import re
import calendar
import datetime
#from datetime import datetime, timedelta

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

storage = firebase.storage()

def rmd(upload_filename, mapping_filename, date_1):
  df = pd.read_csv(upload_filename, header=None, encoding='latin-1')
  print("hello")
  map_futures = pd.read_excel(mapping_filename, sheet_name = 'rmd-futures')
  print("hello world")
  map_options = pd.read_excel(mapping_filename, sheet_name = 'rmd-options')

  df2 = pd.DataFrame(columns = ['Ctrbook Code', 'Option Term', 'Option Exercised', 'Broker', 'Broker Account No.', 'Trader', 'Profit Center', 'Strategy', 'DEFAULT', 'External Reference', 'Trader Date', 'Long/ Short', 'Call/ Put', 'Terminal Month', 'Holiday Calender', 'Strike Price', 'Premium', 'No. Of Lots', 'Broker Commission', 'Commission Currency', 'Commission Amount (Per Lot)'])
  new_df = pd.DataFrame(columns = ['Ctrbook Code*', 'Profit Center*', 'Ctr Date*', 'Contract Term*', 'Trader*', 'Broker*', 'Broker Reference', 'Position Type*', 'Broker Account No*', 'No. Of Lots*', 'Shipment Month*', 'Price*', 'Sub Strategy', 'Contract type', 'Trade Type*', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)'])
  map = pd.DataFrame(columns =  ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','46','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63','64','65','66','67','68','69','70','71','72','73','74'])
  #backdated = pd.DataFrame(columns =  ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','46','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63','64','65','66','67','68','69','70','71','72','73','74'])
  backdated_opt = pd.DataFrame(columns = ['Ctrbook Code', 'Option Term', 'Option Exercised', 'Broker', 'Broker Account No.', 'Trader', 'Profit Center', 'Strategy', 'DEFAULT', 'External Reference', 'Trader Date', 'Long/ Short', 'Call/ Put', 'Terminal Month', 'Holiday Calender', 'Strike Price', 'Premium', 'No. Of Lots', 'Broker Commission', 'Commission Currency', 'Commission Amount (Per Lot)'])
  backdated_fut = pd.DataFrame(columns = ['Ctrbook Code*', 'Profit Center*', 'Ctr Date*', 'Contract Term*', 'Trader*', 'Broker*', 'Broker Reference', 'Position Type*', 'Broker Account No*', 'No. Of Lots*', 'Shipment Month*', 'Price*', 'Sub Strategy', 'Contract type', 'Trade Type*', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)'])
 
  sub_acc = pd.read_excel(mapping_filename, sheet_name = 'sub-accounts')
  sub_acc = sub_acc["Sub-Acc"].to_numpy()

  #ctr_date = df[39][0]

  df = df[~df[40].isnull()]
  df = df.reset_index(drop=True)

  cleaned_lots = ''
  cat = ''
  pos = ''
  for i in range(0, len(df)):
    date = df[39].values[i]
    if date[2] == '/':
      year = date.split('/')[2]
      dt = date.split('/')[0]
      month = date.split('/')[1]
      month_naming = calendar.month_name[int(month)]
      #print(month_naming)
      month_naming = month_naming.upper()[0:3]
      ctr_date = dt+'-'+month_naming+'-'+year
      #print(ctr_date)
    else:
      year = date.split('-')[2]
      dt = date.split('-')[0]
      month = date.split('-')[1]
      month_naming = calendar.month_name[int(month)]
      #print(month_naming)
      month_naming = month_naming.upper()[0:3]
      ctr_date = dt+'-'+month_naming+'-'+year
      #print(ctr_date)

    d = df[19][i]
    id = df[17].values[i]
    id = id.split(' ')[3]

    if id in sub_acc:
      #print("Acc Not to be taken")
      continue

    elif ctr_date != date_1:
      #print('backdated')
      if df[21].values[i] != 0.0:

        temp = map_options.loc[(map_options['RMD File Commodity Name'] == df[20][i]) & (map_options['RMD ACC No'] == df[17][i])]
        if not temp.empty:
          #print("Opop")
          strike = df[21].values[i] * temp['Strike Multiplier'].values[0]
          premium = df[46].values[i] * temp['Premium Multiplier'].values[0]
          if df[22].values[i] == 'P':
            cat = 'PUT'
          if df[22].values[i] == 'C':
            cat = 'CALL'
          #print(cat)

          dt = df[19][i]
          #month = df[19][i].split('-')[1]
          #year = int(df[19][i].split('-')[2]) + 2000
          #terminal_month = month + ' ' + str(year)

          today = datetime.datetime.strptime(dt, '%d-%b-%y')
          next_month = (today.replace(day=28) + datetime.timedelta(days=10))
          next_month = next_month.replace(day=min(today.day, calendar.monthrange(next_month.year, next_month.month)[1]))
          t_date = str(next_month)
          month = t_date.split('-')[1]
          month = calendar.month_abbr[int(month)]
          year = t_date.split('-')[0]
          terminal_month = month + ' ' + str(year)


          lots = df[43].values[i]
          #lots = df.iloc[i, 43]
          #print(lots)
          #for j in range(0, len(lots)):

            #print(type(lots))
            #print(type(cleaned_lots))
          cleaned_lots = ''
          cleaned_lots = str(cleaned_lots) + str(lots)

          #if lots != '(' and lots != ')':
          #  print(lots)
          #  if int(lots) > 0:
          #    pos = 'LONG'
          #  pos = 'LONG'

          if cleaned_lots[0] != '(':
            if int(lots) > 0:
              pos = 'LONG'

          if cleaned_lots[0] == '(':
            pos = 'SHORT'
            cleaned_lots = re.split(r'( | )', cleaned_lots)
            cleaned_lots = cleaned_lots[0]
            lots = cleaned_lots.split('(')
            lots = lots[1]
            lots = lots.split(')')
            #print(lots)
            lots = int(lots[0])
            #print(lots)
            #lots = int(lots)
          elif float(cleaned_lots) < 0:
            pos = 'SHORT'
          lots = int(lots)
          #print(type(lots))  
          #print(lots)
          not_mapped = [temp['Ctrbook Code'].values[0], temp['OptionTerm'].values[0], 'Option Exercised', 'RMD', temp['Broker Account No.'].values[0], temp['Trader'].values[0], temp['Profit Center'].values[0], temp['Sub Strategy'].values[0], 'DEFAULT', ' ', ctr_date, pos, cat, terminal_month, temp['Holiday Calender'].values[0], strike, premium, lots, 'Brokerage Commission', 'USD', '0.01']
          backdated_opt.loc[i] = not_mapped

      else:
        temp = map_futures.loc[(map_futures['RMD File Commodity Name'] == df[20][i]) & (map_futures['RMD ACC No'] == df[17][i])]
        #print(temp)
        if not temp.empty:
          lots = df[43].values[i]
          #print(lots)
          #print(type(lots))
          if lots == 0 or lots == "0":
            continue
            #print("zero detected")

          price = df[46].values[i] * temp['Future Price Multiplier'].values[0]
          dt = df[19][i].split('-')[0]
          month = df[19][i].split('-')[1]
          year = int(df[19][i].split('-')[2]) + 2000
          shipment_date = month + ' ' + str(year)

          #lots = df.iloc[i, 43]
          #print(lots)
          #for j in range(0, len(lots)):
          if lots != '(' and lots != ')':
            #print(type(lots))
            #print(type(cleaned_lots))
            cleaned_lots = ''
            cleaned_lots = str(cleaned_lots) + str(lots)
          if cleaned_lots[0] == '(':
            cleaned_lots = re.split(r'( | )', cleaned_lots)
            cleaned_lots = cleaned_lots[0]
            lots = cleaned_lots.split('(')
            lots = lots[1]
            #print(lots)
            lots = lots.split(')')
            #print(lots)
            lots = int(lots[0])
            #print(lots)
            #lots = int(lots)

          lots = int(lots)
          #print(type(lots))
          #print(lots)
          #print(df[43].values[i])
          #if df.iloc[i, 43][0] == '(' or lots < 0:
          #print(df[43].values[i])
          #print("------")
          #print(lots)
          #l = df[43].values[i]

          if type(df[43].values[i]) == str:
            #print("---")

            if df[43].values[i][0] == '(':
            #if l[0] == '(':
              pos_type = 'SELL'
            else:
              pos_type = "BUY"

          elif type(lots) == int:
            if lots < 0:
              pos_type = 'SELL'
            else:
              pos_type = "BUY"

          #print("hello")
          not_mapped = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], pos_type, temp['Broker Account No.'].values[0], abs(lots), shipment_date, price, temp['Sub Strategy'].values[0], '', 'FUTURES', '', '', '', '', '']
          backdated_fut.loc[i] = not_mapped


    elif df[21].values[i] != 0.0:
      #print("Options")
      #print(map_options['RMD File Commodity Name'])
      #print("-----------------------")
      #print(df[20][i])
      #print("-----------------------")
      #print(map_options['RMD ACC No'])
      #print("-----------------------")
      #print(df[17][i])
      #temp = map_options.loc[(map_options['RMD File Commodity Name'] == df[20][i])]
      temp = map_options.loc[(map_options['RMD File Commodity Name'] == df[20][i]) & (map_options['RMD ACC No'] == df[17][i])]
      if not temp.empty:
        #print("Opop")
        strike = df[21].values[i] * temp['Strike Multiplier'].values[0]
        premium = df[46].values[i] * temp['Premium Multiplier'].values[0]
        if df[22].values[i] == 'P':
          cat = 'PUT'
        if df[22].values[i] == 'C':
          cat = 'CALL'
        #print(cat)
        
        dt = df[19][i]
        #month = df[19][i].split('-')[1]
        #year = int(df[19][i].split('-')[2]) + 2000
        #terminal_month = month + ' ' + str(year)

        today = datetime.datetime.strptime(dt, '%d-%b-%y')
        next_month = (today.replace(day=28) + datetime.timedelta(days=10))
        next_month = next_month.replace(day=min(today.day, calendar.monthrange(next_month.year, next_month.month)[1]))
        t_date = str(next_month)
        month = t_date.split('-')[1]
        month = calendar.month_abbr[int(month)]
        year = t_date.split('-')[0]
        terminal_month = month + ' ' + str(year)


        lots = df[43].values[i]
        #lots = df.iloc[i, 43]
        #print(lots)
        #for j in range(0, len(lots)):
        
          #print(type(lots))
          #print(type(cleaned_lots))
        cleaned_lots = ''
        cleaned_lots = str(cleaned_lots) + str(lots)

        #if lots != '(' and lots != ')':
        #  print(lots)
        #  if int(lots) > 0:
        #    pos = 'LONG'
        #  pos = 'LONG'

        if cleaned_lots[0] != '(':
          if int(lots) > 0:
            pos = 'LONG'

        if cleaned_lots[0] == '(':
          pos = 'SHORT'
          cleaned_lots = re.split(r'( | )', cleaned_lots)
          cleaned_lots = cleaned_lots[0]
          lots = cleaned_lots.split('(')
          lots = lots[1]
          lots = lots.split(')')
          #print(lots)
          lots = int(lots[0])
          #print(lots)
          #lots = int(lots)
        elif float(cleaned_lots) < 0:
          pos = 'SHORT'
        lots = int(lots)
        #print(type(lots))  
        #print(lots)
        row = [temp['Ctrbook Code'].values[0], temp['OptionTerm'].values[0], 'Option Exercised', 'RMD', temp['Broker Account No.'].values[0], temp['Trader'].values[0], temp['Profit Center'].values[0], temp['Sub Strategy'].values[0], 'DEFAULT', ' ', ctr_date, pos, cat, terminal_month, temp['Holiday Calender'].values[0], strike, premium, lots, 'Brokerage Commission', 'USD', '0.01']
        df2.loc[i] = row
      else: 
        #print('Options mapping not found')
        not_mapped = [df[17].values[i], df[18].values[i], df[19].values[i], df[20].values[i], df[21].values[i], df[22].values[i], df[23].values[i], df[24].values[i], df[25].values[i], df[26].values[i], df[27].values[i], df[28].values[i], df[29].values[i], df[30].values[i], df[31].values[i], df[32].values[i], df[33].values[i], df[34].values[i], df[35].values[i], df[36].values[i], df[37].values[i], df[38].values[i], df[39].values[i], df[40].values[i], df[41].values[i], df[42].values[i], df[43].values[i], df[44].values[i], df[45].values[i], df[46].values[i], df[47].values[i], df[48].values[i], df[49].values[i], df[50].values[i], df[51].values[i], df[52].values[i], df[53].values[i], df[54].values[i], df[55].values[i], df[56].values[i], df[57].values[i], df[58].values[i], df[59].values[i], df[60].values[i], df[61].values[i], df[62].values[i], df[63].values[i], df[64].values[i], df[65].values[i], df[66].values[i], df[67].values[i], df[68].values[i], df[69].values[i], df[70].values[i], df[71].values[i], df[72].values[i], df[73].values[i], df[74].values[i], df[75].values[i], df[76].values[i], df[77].values[i], df[78].values[i], df[79].values[i], df[80].values[i], df[81].values[i], df[82].values[i], df[83].values[i], df[84].values[i], df[85].values[i], df[86].values[i], df[87].values[i], df[88].values[i], df[89].values[i], df[90].values[i]]
        map.loc[i] = not_mapped
        

    else:
      temp = map_futures.loc[(map_futures['RMD File Commodity Name'] == df[20][i]) & (map_futures['RMD ACC No'] == df[17][i])]
      #print(temp)
      if not temp.empty:
        lots = df[43].values[i]
        #print("***************************")
        #print(lots)
        #print(type(lots))
        if lots == 0 or lots == "0":
          continue
          #print("zero detected")
        
        price = df[46].values[i] * temp['Future Price Multiplier'].values[0]
        dt = df[19][i].split('-')[0]
        month = df[19][i].split('-')[1]
        year = int(df[19][i].split('-')[2]) + 2000
        shipment_date = month + ' ' + str(year)
        
        #lots = df.iloc[i, 43]
        #print(lots)
        #for j in range(0, len(lots)):
        if lots != '(' and lots != ')':
          #print(type(lots))
          #print(type(cleaned_lots))
          cleaned_lots = ''
          cleaned_lots = str(cleaned_lots) + str(lots)
        if cleaned_lots[0] == '(':
          cleaned_lots = re.split(r'( | )', cleaned_lots)
          cleaned_lots = cleaned_lots[0]
          lots = cleaned_lots.split('(')
          lots = lots[1]
          #print(lots)
          lots = lots.split(')')
          #print(lots)
          lots = int(lots[0])
          #print(lots)
          #lots = int(lots)
        
        #lots = int(lots)
        #print(type(lots))
        #print("@@@@@@@@@@@@@@@@@@")
        if type(lots) == str:
          lots = int(lots.replace(',', ''))
        else:
          lots = int(lots)
        #print(type(lots))
        #print(lots)
        #print(df[43].values[i])
        #if df.iloc[i, 43][0] == '(' or lots < 0:
        #print(df[43].values[i])
        #print("------")
        #print(lots)
        #l = df[43].values[i]
       
        if type(df[43].values[i]) == str:
          #print("---")

          if df[43].values[i][0] == '(':
          #if l[0] == '(':
            pos_type = 'SELL'
          else:
            pos_type = "BUY"

        elif type(lots) == int:
          if lots < 0:
            pos_type = 'SELL'
          else:
            pos_type = "BUY"

        #print("hello")
        row = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], pos_type, temp['Broker Account No.'].values[0], abs(lots), shipment_date, price, temp['Sub Strategy'].values[0], '', 'FUTURES', '', '', '', '', '']
        new_df.loc[i] = row
      else:
        #print("Futures Mapping is Empty")
        not_mapped = [df[17].values[i], df[18].values[i], df[19].values[i], df[20].values[i], df[21].values[i], df[22].values[i], df[23].values[i], df[24].values[i], df[25].values[i], df[26].values[i], df[27].values[i], df[28].values[i], df[29].values[i], df[30].values[i], df[31].values[i], df[32].values[i], df[33].values[i], df[34].values[i], df[35].values[i], df[36].values[i], df[37].values[i], df[38].values[i], df[39].values[i], df[40].values[i], df[41].values[i], df[42].values[i], df[43].values[i], df[44].values[i], df[45].values[i], df[46].values[i], df[47].values[i], df[48].values[i], df[49].values[i], df[50].values[i], df[51].values[i], df[52].values[i], df[53].values[i], df[54].values[i], df[55].values[i], df[56].values[i], df[57].values[i], df[58].values[i], df[59].values[i], df[60].values[i], df[61].values[i], df[62].values[i], df[63].values[i], df[64].values[i], df[65].values[i], df[66].values[i], df[67].values[i], df[68].values[i], df[69].values[i], df[70].values[i], df[71].values[i], df[72].values[i], df[73].values[i], df[74].values[i], df[75].values[i], df[76].values[i], df[77].values[i], df[78].values[i], df[79].values[i], df[80].values[i], df[81].values[i], df[82].values[i], df[83].values[i], df[84].values[i], df[85].values[i], df[86].values[i], df[87].values[i], df[88].values[i], df[89].values[i], df[90].values[i]]
        map.loc[i] = not_mapped

  # init writer
  bio = BytesIO()
  writer = pd.ExcelWriter(bio, engine='xlsxwriter')

  # sheets
  df2.to_excel(writer, "RMD-Options", index=False)
  new_df.to_excel(writer, "RMD-Futures", index=False)
  map.to_excel(writer, "Mapping Not Found", index=False)
  backdated_opt.to_excel(writer, "Backdated-options", index=False)
  backdated_fut.to_excel(writer, "Backdated-futures", index=False)
  # save the workbook
  writer.save()
  bio.seek(0)      

  # get the excel file (answers my question)          
  workbook    = bio.read()  
  excelFile   = workbook


  storage.child('rmd_output/RMD Output.xlsx').put(excelFile)
  storage.child('rmd_output/RMD Output.xlsx').get_url(None)

def rmd_fx(upload_filename, mapping_filename, date_1):
  df = pd.read_csv(upload_filename, header=None)
  map_fx = pd.read_excel(mapping_filename, sheet_name = 'rmd-futures')
  new_df = pd.DataFrame(columns = ['Contract Book',	'Contract Date',	'Long/Short',	'FX Futures Term',	'Delivery Month',	'Profit Center',	'Trader',	'Strategy',	'Tag',	'Trading Broker',	'Trading Broker Account',	'Commodity',	'Clearing Broker',	'Clearing Broker Account',	'No of Contracts',	'FX Rate',	'Internal Reference',	'External Reference',	'IO Number'])
  map = pd.DataFrame(columns =  ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'])
  #backdated = pd.DataFrame(columns =  ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'])
  backdated = pd.DataFrame(columns = ['Contract Book',	'Contract Date',	'Long/Short',	'FX Futures Term',	'Delivery Month',	'Profit Center',	'Trader',	'Strategy',	'Tag',	'Trading Broker',	'Trading Broker Account',	'Commodity',	'Clearing Broker',	'Clearing Broker Account',	'No of Contracts',	'FX Rate',	'Internal Reference',	'External Reference',	'IO Number'])

  sub_acc = pd.read_excel(mapping_filename, sheet_name = 'sub-accounts')
  sub_acc = sub_acc["Sub-Acc"].to_numpy()
  
  #print(ctr_date)

  pos = ''
  for i in range(0, len(df)):
    if pd.isna(df[39].values[i]) == False:
      #print("==========================================================")
      date = df[39].values[i]
      dt = date.split('/')[0]
      year = date.split('/')[2]
      month = date.split('/')[1]
      month_naming = calendar.month_name[int(month)]
      month_naming = month_naming.upper()[0:3]
      ctr_date = dt+'-'+month_naming+'-'+year
      temp = map_fx.loc[(map_fx['RMD File Commodity Name'] == df[20][i]) & (map_fx['RMD ACC No'] == df[17][i])]
      #print(temp)
      #print(ctr_date)

      id = df[17].values[i]
      id = id.split(' ')[3]

      if id in sub_acc:
        #print("Acc Not to be taken")
        break
      
      elif ctr_date != date_1:
        #print('backdated')
        lots = df[44].values[i]
        #print(lots)
        if lots > 0:
          pos = 'LONG'
        else:
          pos = 'SHORT'

        lots = abs(lots)
        #print(lots)

        dt = df[19][i].split('-')[0]
        month = df[19][i].split('-')[1]
        year = int(df[19][i].split('-')[2]) + 2000
        shipment_date = month + '-' + str(year)
        #print(shipment_date)

        price = df[46].values[i] * temp['Future Price Multiplier'].values[0]

        not_mapped = [temp['Ctrbook Code'].values[0], ctr_date, pos, temp['Contract Term'].values[0], shipment_date, temp['Profit Center'].values[0], temp['Trader'].values[0], temp['Sub Strategy'].values[0], '(NA)', temp['Broker'].values[0], temp['Broker Reference'].values[0], temp['Commodity'].values[0], temp['Broker'].values[0], temp['Broker Account No.'].values[0], lots, price, '', '', '']
        backdated.loc[i] = not_mapped

      elif not temp.empty:
        lots = df[44].values[i]
        #print(lots)
        if lots > 0:
          pos = 'LONG'
        else:
          pos = 'SHORT'

        lots = abs(lots)
        #print(lots)

        dt = df[19][i].split('-')[0]
        month = df[19][i].split('-')[1]
        year = int(df[19][i].split('-')[2]) + 2000
        shipment_date = month + '-' + str(year)
        #print(shipment_date)

        price = df[46].values[i] * temp['Future Price Multiplier'].values[0]

        row = [temp['Ctrbook Code'].values[0], ctr_date, pos, temp['Contract Term'].values[0], shipment_date, temp['Profit Center'].values[0], temp['Trader'].values[0], temp['Sub Strategy'].values[0], '(NA)', temp['Broker'].values[0], temp['Broker Reference'].values[0], temp['Commodity'].values[0], temp['Broker'].values[0], temp['Broker Account No.'].values[0], lots, price, '', '', '']
        new_df.loc[i] = row

      else:
        #print('Mapping Not Found')
        not_mapped = [df[1].values[i], df[2].values[i], df[3].values[i], df[4].values[i], df[5].values[i], df[6].values[i], df[7].values[i], df[8].values[i],  df[9].values[i], df[10].values[i], df[11].values[i], df[12].values[i], df[13].values[i], df[14].values[i], df[15].values[i], df[16].values[i], df[17].values[i], df[18].values[i], df[19].values[i], df[20].values[i], df[21].values[i], df[22].values[i], df[23].values[i], df[24].values[i]]
        map.loc[i] = not_mapped
    else:
      break

  # init writer
  bio = BytesIO()
  writer = pd.ExcelWriter(bio, engine='xlsxwriter')

  # sheets
  new_df.to_excel(writer, "RMD - FX", index=False)
  map.to_excel(writer, "Mapping Not Found", index=False)
  backdated.to_excel(writer, "Backdated", index=False)
  # save the workbook
  writer.save()
  bio.seek(0)      

  # get the excel file (answers my question)          
  workbook    = bio.read()  
  excelFile   = workbook


  storage.child('rmd_output/RMD Output.xlsx').put(excelFile)
  storage.child('rmd_output/RMD Output.xlsx').get_url(None)




