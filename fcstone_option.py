import pandas as pd
import calendar
from pyrebase import pyrebase
from io import BytesIO
import numpy as np

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

storage = firebase.storage()

def fcstone_option(upload_filename, mapping_filename):
    print("hello world futures")
    #making instrument match the output fiels with broker name as prefix

    data = [['Société Generale', 'SocGen'], ['INTL FCStone', 'FCStone'],['MAREX SPECTRON','ms']]
    b_df = pd.DataFrame(data, columns=['Broker','code'])

    accounts= pd.read_excel(mapping_filename, sheet_name = "accounts")
    accounts=pd.merge(accounts,b_df, on ='Broker', how ='inner')   #inner join to match all the broker code names
    t1 = accounts["code"].isin(['FCStone'])
    accounts = accounts[t1]

    #Instrument coded file
    ins= pd.read_excel(mapping_filename, sheet_name = "ins_code") #directory
    #ins = pd.read_excel(file1, 'Ins_code')
    #ins_op= pd.read_excel(file1, 'for_options')

    # multiplier Dataset
    FC_mo = pd.read_excel(mapping_filename, sheet_name = "fcstone_options") #directory
    #FC_mo = pd.read_excel(xls, 'FC Stone Options')
    FC_mo.rename(columns = {'Commodity':'Index'}, inplace = True)

    #Tickers
    months=pd.read_excel(mapping_filename, sheet_name = "months")
    
    df=pd.read_csv(upload_filename)

    #filtering based on broker account in PL grains-SocGen, and to get the future prices 
    x = df["Account Id"].isin(accounts['Broker Ac'])
    x2 = df["Trade Type"].isin(["Call","Put"])
    df2=df[x & x2]

    final = df2[["Instrument Name","Strike Price","Trade Type","Delivery Period","Market Price","Composite Delta"]]
    final.rename(columns = {'Instrument Name':'Index'}, inplace = True)
    # remove duplicates
    y=final.drop_duplicates()

    test = pd.DataFrame()
    test = y

    y['Index'].unique()

    #splitting the column
    #y[['Month','Yr']] = y["Delivery Period"].str.split('-', expand=True)
    y[['months_sm','Yr']] = y["Delivery Period"].str.split('-', expand=True)

    y=pd.merge(y, months, on ='months_sm', how ='inner')

    #inner join to match all the index code to the corresponding instrument names
    y = pd.merge(y, ins, on ='Index', how ='inner')

    #inner join to match all commodity to the corresponding multiplier
    y = pd.merge(y,FC_mo , on ='Index', how ='inner')


    y["Final Closing Price"]=y["Market Price"]*y["Premium Multiplier"]


    # Ticker Fendhal mapping for Exchange column
    FC_ticker = pd.read_excel(mapping_filename, sheet_name = "fcstone_ticker") #directory
    #FC_ticker = pd.read_excel(xls_ticker, 'FCStone')
    FC_ticker.rename(columns = {'Fusion Price Quote':'Index'}, inplace = True)

    y = pd.merge(y, FC_ticker, on ='Index', how ='inner')

    xyz= "FCStone" + " " + y['Ins_full']
    y['Instrument'] = xyz

    gel= y['Fusion Ticker'] + "-" + y['Code'] + y['Yr'] + " " + "(" + y['Full'] + ")"
    y['Exchange Contract'] = gel

    y['Strike Price']= y['Strike Price']*y['Strike Multiplier']

    y['Trade Type'] = y['Trade Type'].str.upper()

    # Remove two extra columns
    y.drop(['Index','Delivery Period','months_sm','Number','Month', 'Yr','Code','Full','Ins_full','Fusion Ticker'], axis=1,inplace=True)
    



    #Adding extra empty columns

    y['Order'] = ''
    y['Quote Date'] = ''
    y['Price'] = ''
    y['Start Date'] = ''
    y['End Date'] = ''
    y['Expiry Date'] = ''
    y['Volatility'] = ''
    y['Interest Rate'] = ''

    y['Rho'] = ''
    y['Impl Volatility'] = ''
    y['Correlation'] = ''
    y['Instrument Type Code'] = ''

    y['Gamma'] = ''
    y['Vega'] = ''
    y['Theta'] = ''


    # reaaranging columns

    dft=y[['Instrument','Order','Exchange Contract','Quote Date','Strike Price','Trade Type','Price','Start Date','End Date','Expiry Date','Volatility','Interest Rate','Final Closing Price','Rho','Impl Volatility','Correlation','Instrument Type Code','Composite Delta','Gamma','Vega','Theta']]
#    dft=y[['Instrument','Exchange Contract','Strike Price','Trade Type','Final Closing Price', 'Composite Delta']]
    

    dft['Instrument'].unique()

    dft['Strike Price']=dft['Strike Price'].round(1)

    test2 = pd.DataFrame()
    test2 = dft


    #print(dft.head())

    # download the file as csv
    #dft.to_csv("C:\\Users\\saumya.joshi\\Downloads\\Market Prices\\csv exports daily\\Option_FCStone_table.csv", index=False)

    #op2= pd.read_csv("C:\\Users\\saumya.joshi\\Downloads\\Market Prices\\csv exports daily\\Output_Socgen_option.csv")
    op2 = storage.child('fusion_socgen_uploader/Socgen_option_output.xlsx').get_url(None)
    op2= pd.read_excel(op2)
    #print("============")
    #print(op2.head())


    for i in range(len(dft)):
        for j in range(len(op2)):
            x1=dft.iloc[i,0]
            x2=dft.iloc[i,2]
            x3=dft.iloc[i,4]
            x4=dft.iloc[i,5]
            y1=op2.iloc[j,0]
            y2=op2.iloc[j,2]
            y3=op2.iloc[j,4]
            y4=op2.iloc[j,5]

            #print("read file",x1,x2,x3,x4)
            #print("write file",y1,y2,y3,y4)
            if x1==y1 and x2==y2 and int(x3)==int(y3) and x4==y4 :
                op2.iloc[j,12]=dft.iloc[i,12]
                op2.iloc[j,17]=dft.iloc[i,17]
                #print("match")
    


    # download the file as csv
    #op2.to_csv("C:\\Users\\saumya.joshi\\Downloads\\Market Prices\\csv exports daily\\Output_FCStone_option.csv", index=False)

    #print("============")
    #print(op2.head())
    # init writer
    bio = BytesIO()
    writer = pd.ExcelWriter(bio, engine='xlsxwriter')
    # sheets
    op2.to_excel(writer, "FCStone - Options", index=False)
    test.to_excel(writer, "FcStone Unique Values", index=False)
    test2.to_excel(writer, "FcStone Mapped Table", index=False)

    # save the workbook
    writer.save()
    bio.seek(0)      

    # get the excel file (answers my question)          
    workbook    = bio.read()  
    excelFile   = workbook


    storage.child('fusion_fcstone_uploader/Fcstone_option_output.xlsx').put(excelFile)
    storage.child('fusion_fcstone_uploader/Fcstone_option_output.xlsx').get_url(None)






