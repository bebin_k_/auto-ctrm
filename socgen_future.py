import pandas as pd
import calendar
from pyrebase import pyrebase
from io import BytesIO
import numpy as np

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

storage = firebase.storage()

def socgen_future(upload_filename, mapping_filename):
    print("hello world futures")
    data = [['Société Generale', 'SocGen'], ['INTL FCStone', 'FCStone'],['MAREX SPECTRON','MAREX']]
    b_df = pd.DataFrame(data, columns=['Broker','code'])
    
    SG_mf = pd.read_excel(mapping_filename, sheet_name = "socgen_futures") #multiplier sheet
    #SG_mf = pd.read_excel(xls,'Soc Gen Futures')
    SG_mf.rename(columns = {'Commodity':'Index'}, inplace = True)
    
    accounts= pd.read_excel(mapping_filename, sheet_name = "accounts") #combined accounts
    accounts=pd.merge(accounts,b_df, on ='Broker', how ='inner')   #inner join to match all the broker code names
    t1 = accounts["code"].isin(['SocGen'])      #filter
    accounts = accounts[t1]

    ins= pd.read_excel(mapping_filename, sheet_name = "ins_code")

    df=pd.read_csv(upload_filename) #input file

    x = df["Account Number"].isin(accounts['Broker Ac'])
    x2 = ~df["Put/Call"].isin(["C","P"])
    df2=df[x & x2]

    final = df2[["Futures Description","Closing Price"]]
    # remove duplicates
    y=final.drop_duplicates()

    test = pd.DataFrame()
    test = y

    #Splitting Futures Description Column data into Month - Year - Instrument
    y[['Futures Description','Yr','Index']] = y["Futures Description"].str.split(' ', n=2 , expand=True)

    #inner join to match all the index of Socgen to the corresponding MULTIPLIERS
    y = pd.merge(y, SG_mf, on ='Index', how ='inner')

    #add a column with static value 1 
    y.insert(0, "dd", "1")

    # concat columns by separator to create DATE column to match Fusion format
    y['Date']=y['dd'].str.cat(y[['Futures Description','Yr']], sep='-')


    #inner join to match all the index code to the corresponding instrument names
    joined = pd.merge(y, ins, on ='Index', how ='inner')

    xyz= "SocGen" + " " + joined['Ins_full'] + " " + ">>"
    joined['Instrument'] = xyz

    # for using in lambda function
    joined.rename(columns = {'Closing Price':'Closing_Price' },inplace=True)

    joined['space']=joined['Closing_Price'].apply(lambda Closing_Price: Closing_Price.lstrip())

    #Splitting 
    joined[['no','fraction']] = joined["space"].str.split(' ', expand=True)

    # Defining all the conditions inside a function
    def condition(x):
        if x=="1/2":
            return 5
        elif x=="1/4":
            return 25
        elif x=="3/4":
            return 75
        elif x=="1/8":
            return 125
        elif x=="3/8":
            return 375
        elif x=="7/8":
            return 875
        elif x=="5/8":
            return 625
        else:
            return 0

    # Applying the conditions
    joined['f2'] = joined['fraction'].apply(condition)

    joined['f2']=joined['f2'].apply(str)

    # concat columns to create a complete Closing Price 
    joined['joined price']= joined['no']+joined['f2']

    # for making the closing price multipliable with Strike price
    floatArray = np.asfarray(joined['joined price'], dtype = float)

    joined['change_CP']= floatArray

    # Closing price with Strike prices
    joined['CP_with_multiplier']= joined['Multiplier']*joined['change_CP']

    #Changing the date format to match the FUSION date format
    #print(joined['Date'])
    joined['Date'] = joined['Date'].replace({'-':'/'},regex=True)
    #print(joined['Date'])
    #joined['Date'] = joined['Date'].apply,(lambda x: x.replace('-', '/'))
    joined['Date'] = joined['Date'].apply(lambda x: '/'.join([x.split('/')[0], x.split('/')[1][0] + x.split('/')[1][1:].lower(), x.split('/')[2]]))

    # Reading the empty Fusion Input Template
    op= pd.read_excel(mapping_filename, sheet_name = "future_output")

    #change data to records to match later
    op_copy = op.copy()
    op_ind = op_copy['Months'].values.tolist()
    op_copy.set_index('Months', inplace=True)

    #Matching the months and instruments and placing the prices
    joined_records = joined.to_dict('record')
    for row in joined_records:
        try:
            op_copy.at[row['Date'], row['Instrument']]
            op_copy.at[row['Date'], row['Instrument']] = row['CP_with_multiplier']
        except KeyError:
            continue

    op_copy.reset_index(inplace=True)


    # init writer
    bio = BytesIO()
    writer = pd.ExcelWriter(bio, engine='xlsxwriter')
    # sheets
    op_copy.to_excel(writer, "SocGen - Futures", index=False)
    test.to_excel(writer, "ScoGen Unique Values", index=False)

    # save the workbook
    writer.save()
    bio.seek(0)      

    # get the excel file (answers my question)          
    workbook    = bio.read()  
    excelFile   = workbook


    storage.child('fusion_socgen_uploader/Socgen_future_output.xlsx').put(excelFile)
    storage.child('fusion_socgen_uploader/Socgen_future_output.xlsx').get_url(None)