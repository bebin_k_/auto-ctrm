import pandas as pd
import calendar
from pyrebase import pyrebase
from io import BytesIO
import warnings

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

storage = firebase.storage()

def recon_macquaire(recon_macquaire_url, recon_ctrm_url):
    broker_file = pd.read_csv(recon_macquaire_url)


    with warnings.catch_warnings(record=True):
        warnings.simplefilter("always")
        ctrm_file = pd.read_excel(recon_ctrm_url, engine="openpyxl")

        #Broker Filter
        filter = ctrm_file.loc[ctrm_file['Broker'] == 'MACQUARIE']
        #print(filter.shape)
        #print(filter.columns)

        #Futures Filter
        filter_fut = filter.loc[filter['Instrument Type'] == 'FUTURES']
        #print(filter_fut.shape)

        #Options Filter
        filter_opt = filter.loc[filter['Instrument Type'] == 'OPTIONS']
        #print(filter_opt.shape)

        #account filter for Futures
        account_filter_fut = filter_fut['Broker Account'].unique()
        #print(account_filter)
        #account filter for Options
        account_filter_opt = filter_opt['Broker Account'].unique()
        #print(account_filter)
        
        #Long Short for Fut and Opt
        filter_fut_long = filter_fut.loc[filter_fut['Long Short'] == 'LONG']
        filter_fut_short = filter_fut.loc[filter_fut['Long Short'] == 'SHORT']
        filter_opt_long = filter_opt.loc[filter_opt['Long Short'] == 'LONG']
        filter_opt_short = filter_opt.loc[filter_opt['Long Short'] == 'SHORT']

        #Net Lots acc to Long and Short
        lots_ctrm_fut_long = filter_fut_long['No Of Lots'].sum()
        lots_ctrm_fut_short = filter_fut_short['No Of Lots'].sum()
        lots_ctrm_opt_long = filter_opt_long['No Of Lots'].sum()
        lots_ctrm_opt_short = filter_opt_short['No Of Lots'].sum()






        #Broker File
        broker_file['PUT_CALL'] = broker_file['PUT_CALL'].astype(str)

        #Futures Filter
        filter_op_fut = broker_file.loc[broker_file['PUT_CALL'] == 'nan']
        #print(filter_op_fut.shape)

        #Futures account filter
        acc_filter_op_fut = filter_op_fut.loc[filter_op_fut['CLIENT_CODE'].isin(account_filter_fut)]
        #print(acc_filter_op_fut.shape)

        #Options Filter
        filter_op_opt = broker_file[broker_file['PUT_CALL'].isin(['P', 'C'])]
        #print(filter_op_opt.shape)

        #Options account Filter
        acc_filter_op_opt = filter_op_opt.loc[filter_op_opt['CLIENT_CODE'].isin(account_filter_opt)]
        #print(acc_filter_op_opt.shape)

        #Futures Net Lot with long and short
        long_sum_fut = acc_filter_op_fut['BOUGHT_LOTS'].sum()
        short_sum_fut = acc_filter_op_fut['SOLD_LOTS'].sum()
        net_lots_fut = long_sum_fut + short_sum_fut 

        #Options Net lots with long and short
        long_sum_opt = acc_filter_op_opt['BOUGHT_LOTS'].sum()
        short_sum_opt = acc_filter_op_opt['SOLD_LOTS'].sum()
        net_lots_opt = long_sum_opt + short_sum_opt

        print(lots_ctrm_fut_long , ':', long_sum_fut)
        print(lots_ctrm_fut_short , ':', short_sum_fut)
        print(lots_ctrm_opt_short , ':', short_sum_opt)
        print(lots_ctrm_opt_long , ':', long_sum_opt)



    
