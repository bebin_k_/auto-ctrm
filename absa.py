import pandas as pd
import calendar
from pyrebase import pyrebase
from io import BytesIO
import datetime

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

storage = firebase.storage()

def absa(upload_filename, mapping_filename, date_1):

    df = pd.read_csv(upload_filename, error_bad_lines=False)
    dt = df[:0].columns.values[1]

    new_df = pd.DataFrame(columns = ['Ctrbook Code', 'Profit Center', 'Ctr Date', 'Contract Term', 'Trader', 'Broker', 'Broker Reference', 'Position Type', 'Broker Account No.', 'No. Of Lots', 'Shipment Month', 'Price', 'Sub Strategy', 'Contract type', 'Trade Type', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)']) 
    map = pd.DataFrame(columns =  ['Safex_Ref', 	'Safex_Reg_Code', 	'Member_Reg_Code', 	'Contract', 	'Contdate', 	'Member_Ref', 	'Time_Dealt', 	'No_of_Contracts', 	'Physical_Position', 	'Price/Premium', 	'Booking_Fee', 	'Management_Fee', 	'Mark-To-Market', 	'Funding_Interest', 	'Cumulative_Mtm'])
    #backdated = pd.DataFrame(columns =  ['Safex_Ref', 	'Safex_Reg_Code', 	'Member_Reg_Code', 	'Contract', 	'Contdate', 	'Member_Ref', 	'Time_Dealt', 	'No_of_Contracts', 	'Physical_Position', 	'Price/Premium', 	'Booking_Fee', 	'Management_Fee', 	'Mark-To-Market', 	'Funding_Interest', 	'Cumulative_Mtm'])
    backdated = pd.DataFrame(columns = ['Ctrbook Code', 'Profit Center', 'Ctr Date', 'Contract Term', 'Trader', 'Broker', 'Broker Reference', 'Position Type', 'Broker Account No.', 'No. Of Lots', 'Shipment Month', 'Price', 'Sub Strategy', 'Contract type', 'Trade Type', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)']) 

    df_extracted = pd.DataFrame()

    map_df = pd.read_excel(mapping_filename, sheet_name='absa')
    sub_acc = pd.read_excel(mapping_filename, sheet_name = 'sub-accounts')
    sub_acc = sub_acc["Sub-Acc"].to_numpy()

    dt = datetime.datetime.strptime(dt, '%d/%m/%Y')
    date = str(dt)
    date = date.split(' ')
    date = date[0]

    dt = date.split('-')[2]

    month = date.split('-')[1]

    year = date.split('-')[0]
    month_naming = calendar.month_name[int(month)]
    #print(month_naming)
    month_naming = month_naming.upper()[0:3]
    ctr_date = dt+'-'+month_naming+'-'+year
    #shipment_month = month_naming+' '+year
    #print(ctr_date)

    df = pd.read_csv(upload_filename, skiprows=1, error_bad_lines=False)

    #broker_account = ['NSC132', 'PKR012', 'NRO942', 'ENS592', 'HBY342', 'FHC672', 'WJH282', 'LTD292']
    broker_account = map_df['Broker Account No.'].unique()
    brought = 'BROUGHT FORWARD POSITION'
    carried = 'CARRIED FORWARD POSITION'

    for x in broker_account:
        exists = x in df.Safex_Reg_Code.to_list()
        if exists == True:
            temp = df.loc[df['Safex_Reg_Code'] == x,:]
            df_extracted = df_extracted.append(temp)
            
    df_extracted = df_extracted.loc[df['Safex_Ref'] != carried]
    df_extracted = df_extracted.loc[df['Safex_Ref'] != brought]
    df_extracted = df_extracted.loc[df['No_of_Contracts'] != 0]

    map_df['ABSA File Commodity Name'] = map_df['ABSA File Commodity Name'].str.strip()

    map_df['ABSA ACC No'] = map_df['ABSA ACC No'].str.strip()

    buy_sell = ''
    lots = 0
    price = 0
    safex_reg_code = ''
    contract = ''
    temp = ''
    shipment_month = ''

    for i in range(0, len(df_extracted.values)):
        safex_reg_code = df_extracted['Safex_Reg_Code'].values[i]
        afex_reg_code = safex_reg_code.strip()
        contract = df_extracted['Contract'].values[i]
        contract = contract.split(' ')[2]
        contract = contract.strip()
        #print(contract)
        temp = map_df.loc[(map_df['ABSA File Commodity Name'] == contract) & (map_df['ABSA ACC No'] == safex_reg_code)]
        #print(temp)

        if safex_reg_code in sub_acc:
            #print("Acc Not to be taken")
            continue
        
        elif ctr_date != date_1:
            #print("Backdated")
            #print("Position found")   
            buy_sell_value = df_extracted['No_of_Contracts'].values[i]
            price = df_extracted['Price/Premium'].values[i]

            shipment_month = df_extracted['Contract'].values[i]
            #print(shipment_month)
            dt = shipment_month.split(' ')[1]
            index_month = 3
            month = dt[:index_month]
            index_year = 3
            year = dt[index_year:]
            year = "20" + year
            shipment = month + ' ' + year
            

            if buy_sell_value > 0:
                buy_sell = 'BUY'
                lots = buy_sell_value
            else:
                buy_sell = 'SELL'
                lots = -1*buy_sell_value
            not_mapped  = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], buy_sell, temp['Broker Account No.'].values[0], lots, shipment, price, temp['Sub Strategy'].values[0], '', 'FUTURES', '', '', '', '', '']
            backdated.loc[i] = not_mapped
            

        elif not temp.empty:
            #print("Position found")   
            buy_sell_value = df_extracted['No_of_Contracts'].values[i]
            price = df_extracted['Price/Premium'].values[i]

            #shipment_month = df_extracted['Contdate'].values[i].split(' ')[0]
            #print(shipment_month)
            #shipment_year = '20' + df_extracted['Contdate'].values[i].split('-')[0]
            ##print(shipment_year)
            #month = df_extracted['Contdate'].values[i].split('-')[1]
            #print(month)
            #shipment_year = '20' + df_extracted['Contdate'].values[i].split(' ')[1]
            #shipment = month + ' ' + shipment_year
            #print(month, shipment_year)

            shipment_month = df_extracted['Contract'].values[i]
            #print(shipment_month)
            dt = shipment_month.split(' ')[1]
            index_month = 3
            month = dt[:index_month]
            index_year = 3
            year = dt[index_year:]
            year = "20" + year
            shipment = month + ' ' + year
            

            if buy_sell_value > 0:
                buy_sell = 'BUY'
                lots = buy_sell_value
            else:
                buy_sell = 'SELL'
                lots = -1*buy_sell_value
            row = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], buy_sell, temp['Broker Account No.'].values[0], lots, shipment, price, temp['Sub Strategy'].values[0], '', 'FUTURES', '', '', '', '', '']
            new_df.loc[i] = row

        else:
            #print("Mapping Not Found")
            not_mapped = [df_extracted['Safex_Ref'].values[i], df_extracted['Safex_Reg_Code'].values[i], df_extracted['Member_Reg_Code'].values[i], df_extracted['Contract'].values[i], df_extracted['Contdate'].values[i], df_extracted['Member_Ref'].values[i], df_extracted['Time_Dealt'].values[i], df_extracted['No_of_Contracts'].values[i],  df_extracted['Physical_Position'].values[i], df_extracted['Price/Premium'].values[i], df_extracted['Booking_Fee'].values[i], df_extracted['Management_Fee'].values[i], df_extracted['Mark-To-Market'].values[i], df_extracted['Funding_Interest'].values[i], df_extracted['Cumulative_Mtm'].values[i]]
            map.loc[i] = not_mapped

    # init writer
    bio = BytesIO()
    writer = pd.ExcelWriter(bio, engine='xlsxwriter')
    # sheets
    new_df.to_excel(writer, "Absa",index=False)
    map.to_excel(writer, "Mapping Not found", index=False)
    backdated.to_excel(writer, "Backdated", index=False)

    # save the workbook
    writer.save()
    bio.seek(0)      

    # get the excel file (answers my question)          
    workbook    = bio.read()  
    excelFile   = workbook


    storage.child('absa_output/ABSA Output.xlsx').put(excelFile)
    storage.child('absa_output/ABSA Output.xlsx').get_url(None)
    
