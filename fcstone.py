import pandas as pd
import calendar
from pyrebase import pyrebase
from io import BytesIO

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

storage = firebase.storage()

def fcstone(upload_filename, mapping_filename, date):
    df = pd.read_csv(upload_filename)
    map_futures = pd.read_excel(mapping_filename, sheet_name = 'fc-stone-futures')
    map_options = pd.read_excel(mapping_filename, sheet_name = 'fc-stone-options')
    sub_acc = pd.read_excel(mapping_filename, sheet_name = 'sub-accounts')
    sub_acc = sub_acc["Sub-Acc"].to_numpy()

    opt = pd.DataFrame(columns = ['Ctrbook Code', 'Option Term', 'Option Exercised', 'Broker', 'Broker Account No.', 'Trader', 'Profit Center', 'Strategy', 'DEFAULT', 'External Reference', 'Trader Date', 'Long/ Short', 'Call/ Put', 'Terminal Month', 'Holiday Calender', 'Strike Price', 'Premium', 'No. Of Lots', 'Broker Commission', 'Commission Currency', 'Commission Amount (Per Lot)','Exchange ID', 'Fee', 'Fee currency'])
    fut = pd.DataFrame(columns = ['Ctrbook Code*', 'Profit Center*', 'Ctr Date*', 'Contract Term*', 'Trader*', 'Broker*', 'Broker Reference', 'Position Type*', 'Broker Account No*', 'No. Of Lots*', 'Shipment Month*', 'Price*', 'Sub Strategy', 'Contract type', 'Trade Type*', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)','Exchange ID', 'Fee', 'Fee currency'])
    fx = pd.DataFrame(columns = ['Contract Book',	'Contract Date',	'Long/Short',	'FX Futures Term',	'Delivery Month',	'Profit Center',	'Trader',	'Strategy',	'Tag',	'Trading Broker',	'Trading Broker Account',	'Commodity',	'Clearing Broker',	'Clearing Broker Account',	'No of Contracts',	'FX Rate',	'Internal Reference',	'External Reference',	'IO Number','Exchange ID', 'Fee', 'Fee currency'])
    delete_positions = pd.DataFrame(columns =  ['Transaction ID',	'Business Date',	'Trade Date',	'Action',	'Account Id',	'Family Group',	'Account Name',	'Exchange',	'Currency',	'Exchange Code',	'Instrument Code',	'Instrument Name',	'Trade Type',	'Option Style',	'Buy/Sell',	'Abs Quantity',	'Net Quantity',	'Prompt',	'Trade Price',	'Strike Price',	'Market Price',	'Premium',	'MtM',	'Trade Value',	'Total Charge',	'Commission',	'Exchange Fee',	'Clearing Fee',	'NFA Fee', 'Average Price Premium/Discount',	'Delivery Type',	'Days To Maturity',	'Maturity Date',	'OTC Indicator',	'OTC Type',	'Average Price Start Date',	'Average Price End Date'])
    map = pd.DataFrame(columns =  ['Transaction ID',	'Business Date',	'Trade Date',	'Action',	'Account Id',	'Family Group',	'Account Name',	'Exchange',	'Currency',	'Exchange Code',	'Instrument Code',	'Instrument Name',	'Trade Type',	'Option Style',	'Buy/Sell',	'Abs Quantity',	'Net Quantity',	'Prompt',	'Trade Price',	'Strike Price',	'Market Price',	'Premium',	'MtM',	'Trade Value',	'Total Charge',	'Commission',	'Exchange Fee',	'Clearing Fee',	'NFA Fee', 'Average Price Premium/Discount',	'Delivery Type',	'Days To Maturity',	'Maturity Date',	'OTC Indicator',	'OTC Type',	'Average Price Start Date',	'Average Price End Date'])
    #backdated = pd.DataFrame(columns =  ['Transaction ID',	'Business Date',	'Trade Date',	'Action',	'Account Id',	'Family Group',	'Account Name',	'Exchange',	'Currency',	'Exchange Code',	'Instrument Code',	'Instrument Name',	'Trade Type',	'Option Style',	'Buy/Sell',	'Abs Quantity',	'Net Quantity',	'Prompt',	'Trade Price',	'Strike Price',	'Market Price',	'Premium',	'MtM',	'Trade Value',	'Total Charge',	'Commission',	'Exchange Fee',	'Clearing Fee',	'NFA Fee', 'Average Price Premium/Discount',	'Delivery Type',	'Days To Maturity',	'Maturity Date',	'OTC Indicator',	'OTC Type',	'Average Price Start Date',	'Average Price End Date'])
    backdated_opt = pd.DataFrame(columns = ['Ctrbook Code', 'Option Term', 'Option Exercised', 'Broker', 'Broker Account No.', 'Trader', 'Profit Center', 'Strategy', 'DEFAULT', 'External Reference', 'Trader Date', 'Long/ Short', 'Call/ Put', 'Terminal Month', 'Holiday Calender', 'Strike Price', 'Premium', 'No. Of Lots', 'Broker Commission', 'Commission Currency', 'Commission Amount (Per Lot)','Exchange ID', 'Fee', 'Fee currency'])
    backdated_fut = pd.DataFrame(columns = ['Ctrbook Code*', 'Profit Center*', 'Ctr Date*', 'Contract Term*', 'Trader*', 'Broker*', 'Broker Reference', 'Position Type*', 'Broker Account No*', 'No. Of Lots*', 'Shipment Month*', 'Price*', 'Sub Strategy', 'Contract type', 'Trade Type*', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)','Exchange ID', 'Fee', 'Fee currency'])
    backdated_fx = pd.DataFrame(columns = ['Contract Book',	'Contract Date',	'Long/Short',	'FX Futures Term',	'Delivery Month',	'Profit Center',	'Trader',	'Strategy',	'Tag',	'Trading Broker',	'Trading Broker Account',	'Commodity',	'Clearing Broker',	'Clearing Broker Account',	'No of Contracts',	'FX Rate',	'Internal Reference',	'External Reference',	'IO Number','Exchange ID', 'Fee', 'Fee currency'])

    
    if type(df['Trade Price'][0]) == str:
        df['Trade Price'] = df['Trade Price'].str.replace(",(\d{3})", "\\1")
        df['Trade Price'] = pd.to_numeric(df['Trade Price'])
        df['Trade Price'] = df['Trade Price'].astype(float)
    #year = df['Business Date'][0].split('-')[0]
    #month = df['Business Date'][0].split('-')[1]
    #dt = df['Business Date'][0].split('-')[2]
    #ctr_date_opt = dt + '/' + month + '/' + str(year)

    #year = df['Business Date'][0].split('-')[0]
    #month = df['Business Date'][0].split('-')[1]
    #dt = df['Business Date'][0].split('-')[2]
    #ctr_date_fut = dt + '-' + month + '-' + str(year)

    #year = df['Trade Date'][0].split('-')[0]
    #month = df['Business Date'][0].split('-')[1]
    #month = calendar.month_abbr[int(month)]
    #dt = df['Business Date'][0].split('-')[2]
    #ctr_date_fx = dt + '-' + month + '-' + str(year)

    pos = ''
    for i in range(0, len(df)):
        ctr_date = df['Trade Date'].values[i]

        dt_1 = ctr_date.split('-')[0]
        a = len(dt_1)
        if a == 4:
            year = ctr_date.split('-')[0] 
            month = ctr_date.split('-')[1]
            month = calendar.month_abbr[int(month)]
            month = month.upper() 
            day = ctr_date.split('-')[2] 
            ctr_date = day + '-' + month + '-' + str(year)
            #print(ctr_date)
        else:
            year = ctr_date.split('-')[2]
            month = ctr_date.split('-')[1]
            month = calendar.month_abbr[int(month)]
            month = month.upper()
            dt = ctr_date.split('-')[0]
            ctr_date = dt + '-' + month + '-' + "20" + str(year)
        #print("sys",date)
        #print("df date",ctr_date)
        #print("----")

        if df['Account Id'].values[i] in sub_acc:
            #print("Acc Not to be taken")
            continue

        elif df['Trade Type'].values[i] != 'Future':
            if df['Action'].values[i] == 'Insert':
                if ctr_date == date:
                    #print("Options")
                    #print(map_options['FC Stone File Commodity Name'])
                    #print("-----------------------")
                    #print(df['Instrument Name'][i])
                    #print("-----------------------")
                    #print(map_options['FC Stone ACC No'])
                    #print("-----------------------")
                    #print(df['Account Id'][i])
                    temp = map_options.loc[(map_options['FC Stone File Commodity Name'] == df['Instrument Name'][i]) & (map_options['FC Stone ACC No'] == df['Account Id'][i])]
                    #print(temp)
                    if not temp.empty:
                        cat = df['Trade Type'].values[i]

                        buy_sell = df['Buy/Sell'].values[i]

                        if buy_sell == 'Buy':
                            pos = 'Long'
                        else:
                            pos = 'Short'

                        lots = df['Abs Quantity'].values[i]

                        terminal_month = df['Prompt'].values[i]
                        month = terminal_month.split('-')[0]
                        year = int(terminal_month.split('-')[1]) + 2000
                        terminal_month = month + ' ' + str(year)

                        #premium = (df['Trade Price'].values[i] * temp['Premium Multiplier'].values[0])

                        data_price = float(df['Trade Price'].values[i])
                        temp_price = temp['Premium Multiplier'].values[0]
                        premium = data_price * temp_price
                        #print(premium)
                        #print('=================')
                        #print(type(df['Trade Price'].values[i]))
                        #print('===================')
                        #print(type(temp['Premium Multiplier'].values[0]))
                        exchange_fee = df['Transaction ID'].values[i]
                        exchange_fee = exchange_fee.split('-')[1]
                        fee = df['Total Charge'].values[i]
                        fee_currency = df['Currency'].values[i]

                        strike = df['Strike Price'].values[i] * temp['Strike Multiplier'].values[0]

                        row = [temp['Ctrbook Code'].values[0], temp['OptionTerm'].values[0], 'Option Exercised', 'FC STONE', temp['Broker Account No.'].values[0], temp['Trader'].values[0], temp['Profit Center'].values[0], temp['Sub Strategy'].values[0], 'DEFAULT', ' ', ctr_date, pos, cat, terminal_month, temp['Holiday Calender'].values[0], strike, premium, lots, 'Brokerage Commission', 'USD', '0.01', exchange_fee, fee, fee_currency]
                        opt.loc[i] = row

                    else:
                        #print("Options")
                        not_mapped = [df['Transaction ID'].values[i], df['Business Date'].values[i], df['Trade Date'].values[i], df['Action'].values[i], df['Account Id'].values[i], df['Family Group'].values[i], df['Account Name'].values[i], df['Exchange'].values[i], df['Currency'].values[i], df['Exchange Code'].values[i], df['Instrument Code'].values[i], df['Instrument Name'].values[i], df['Trade Type'].values[i], df['Option Style'].values[i], df['Buy/Sell'].values[i], df['Abs Quantity'].values[i], df['Net Quantity'].values[i], df['Prompt'].values[i], df['Trade Price'].values[i], df['Strike Price'].values[i], df['Market Price'].values[i], df['Premium'].values[i], df['MtM'].values[i], df['Trade Value'].values[i], df['Total Charge'].values[i], df['Commission'].values[i], df['Exchange Fee'].values[i], df['Clearing Fee'].values[i], df['NFA Fee'].values[i], df['Average Price Premium/Discount'].values[i], df['Delivery Type'].values[i], df['Days To Maturity'].values[i], df['Maturity Date'].values[i], df['OTC Indicator'].values[i], df['OTC Type'].values[i], df['Average Price Start Date'].values[i], df['Average Price End Date'].values[i]]
                        map.loc[i] = not_mapped

                else:
                    #print('backdated')
                    temp = map_options.loc[(map_options['FC Stone File Commodity Name'] == df['Instrument Name'][i]) & (map_options['FC Stone ACC No'] == df['Account Id'][i])]
                    #print(temp)
                    if not temp.empty:
                        cat = df['Trade Type'].values[i]

                        buy_sell = df['Buy/Sell'].values[i]

                        if buy_sell == 'Buy':
                            pos = 'Long'
                        else:
                            pos = 'Short'

                        lots = df['Abs Quantity'].values[i]

                        terminal_month = df['Prompt'].values[i]
                        month = terminal_month.split('-')[0]
                        year = int(terminal_month.split('-')[1]) + 2000
                        terminal_month = month + ' ' + str(year)

                        #premium = (df['Trade Price'].values[i] * temp['Premium Multiplier'].values[0])

                        data_price = float(df['Trade Price'].values[i])
                        temp_price = temp['Premium Multiplier'].values[0]
                        premium = data_price * temp_price
                        #print(premium)
                        #print('=================')
                        #print(type(df['Trade Price'].values[i]))
                        #print('===================')
                        #print(type(temp['Premium Multiplier'].values[0]))
                        exchange_fee = df['Transaction ID'].values[i]
                        exchange_fee = exchange_fee.split('-')[1]
                        fee = df['Total Charge'].values[i]
                        fee_currency = df['Currency'].values[i]

                        strike = df['Strike Price'].values[i] * temp['Strike Multiplier'].values[0]

                        not_mapped = [temp['Ctrbook Code'].values[0], temp['OptionTerm'].values[0], 'Option Exercised', 'FC STONE', temp['Broker Account No.'].values[0], temp['Trader'].values[0], temp['Profit Center'].values[0], temp['Sub Strategy'].values[0], 'DEFAULT', ' ', ctr_date, pos, cat, terminal_month, temp['Holiday Calender'].values[0], strike, premium, lots, 'Brokerage Commission', 'USD', '0.01', exchange_fee, fee, fee_currency]
                        backdated_opt.loc[i] = not_mapped

                    else:
                        #print("Options")
                        not_mapped = [df['Transaction ID'].values[i], df['Business Date'].values[i], df['Trade Date'].values[i], df['Action'].values[i], df['Account Id'].values[i], df['Family Group'].values[i], df['Account Name'].values[i], df['Exchange'].values[i], df['Currency'].values[i], df['Exchange Code'].values[i], df['Instrument Code'].values[i], df['Instrument Name'].values[i], df['Trade Type'].values[i], df['Option Style'].values[i], df['Buy/Sell'].values[i], df['Abs Quantity'].values[i], df['Net Quantity'].values[i], df['Prompt'].values[i], df['Trade Price'].values[i], df['Strike Price'].values[i], df['Market Price'].values[i], df['Premium'].values[i], df['MtM'].values[i], df['Trade Value'].values[i], df['Total Charge'].values[i], df['Commission'].values[i], df['Exchange Fee'].values[i], df['Clearing Fee'].values[i], df['NFA Fee'].values[i], df['Average Price Premium/Discount'].values[i], df['Delivery Type'].values[i], df['Days To Maturity'].values[i], df['Maturity Date'].values[i], df['OTC Indicator'].values[i], df['OTC Type'].values[i], df['Average Price Start Date'].values[i], df['Average Price End Date'].values[i]]
                        map.loc[i] = not_mapped
  
            else:
                #print("delete position")
                not_mapped = [df['Transaction ID'].values[i], df['Business Date'].values[i], df['Trade Date'].values[i], df['Action'].values[i], df['Account Id'].values[i], df['Family Group'].values[i], df['Account Name'].values[i], df['Exchange'].values[i], df['Currency'].values[i], df['Exchange Code'].values[i], df['Instrument Code'].values[i], df['Instrument Name'].values[i], df['Trade Type'].values[i], df['Option Style'].values[i], df['Buy/Sell'].values[i], df['Abs Quantity'].values[i], df['Net Quantity'].values[i], df['Prompt'].values[i], df['Trade Price'].values[i], df['Strike Price'].values[i], df['Market Price'].values[i], df['Premium'].values[i], df['MtM'].values[i], df['Trade Value'].values[i], df['Total Charge'].values[i], df['Commission'].values[i], df['Exchange Fee'].values[i], df['Clearing Fee'].values[i], df['NFA Fee'].values[i], df['Average Price Premium/Discount'].values[i], df['Delivery Type'].values[i], df['Days To Maturity'].values[i], df['Maturity Date'].values[i], df['OTC Indicator'].values[i], df['OTC Type'].values[i], df['Average Price Start Date'].values[i], df['Average Price End Date'].values[i]]
                delete_positions.loc[i] = not_mapped

        elif df['Trade Type'].values[i] == 'Future':
            if df['Action'].values[i] == 'Insert':
                if ctr_date == date:
                    comm = df['Exchange'] [i] + " " + df['Instrument Name'][i]
                    temp = map_futures.loc[(map_futures['FC Stone File Commodity Name'] == comm) & (map_futures['FC Stone ACC No'] == df['Account Id'][i])]
                    #print(temp)

                    if not temp.empty:
                        if temp['Futures or FX'].values[0] == 'Futures':
                            buy_sell = df['Buy/Sell'].values[i]
                            lots = df['Abs Quantity'].values[i]

                            terminal_month = df['Prompt'].values[i]
                            month = terminal_month.split('-')[0]
                            year = int(terminal_month.split('-')[1]) + 2000
                            terminal_month = month + ' ' + str(year)

                            exchange_fee = df['Transaction ID'].values[i]
                            exchange_fee = exchange_fee.split('-')[1]
                            fee = df['Total Charge'].values[i]
                            fee_currency = df['Currency'].values[i]


                            price = df['Trade Price'].values[i] * temp['Future Price Multiplier'].values[0]

                            row = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], buy_sell, temp['Broker Account No.'].values[0], lots, terminal_month, price, temp['Sub Strategy'].values[0], '', 'FUTURES', '', '', '', '', '', exchange_fee, fee, fee_currency]
                            fut.loc[i] = row

                        else:
                            #print('FX')

                            terminal_month = df['Prompt'].values[i]
                            month = terminal_month.split('-')[0]
                            year = int(terminal_month.split('-')[1]) + 2000
                            month = month.upper()
                            terminal_month = month + ' ' + str(year)
                            buy_sell = df['Buy/Sell'].values[i]
                            if buy_sell == 'Buy':
                                pos = 'LONG'
                            else:
                                pos = 'SHORT'

                            lots = df['Abs Quantity'].values[i]
                            price = df['Trade Price'].values[i] * temp['Future Price Multiplier'].values[0]

                            exchange_fee = df['Transaction ID'].values[i]
                            exchange_fee = exchange_fee.split('-')[1]
                            fee = df['Total Charge'].values[i]
                            fee_currency = df['Currency'].values[i]

                            row = [temp['Ctrbook Code'].values[0], ctr_date, pos, temp['Contract Term'].values[0], terminal_month, temp['Profit Center'].values[0], temp['Trader'].values[0], temp['Sub Strategy'].values[0], '(NA)', temp['Broker'].values[0], temp['Broker Reference'].values[0], temp['Commodity'].values[0], temp['Broker'].values[0], temp['Broker Account No.'].values[0], lots, price, '', '', '', exchange_fee, fee, fee_currency]
                            fx.loc[i] = row

                    else:
                        #print("Futures and FX Mapping is not found")
                        not_mapped = [df['Transaction ID'].values[i], df['Business Date'].values[i], df['Trade Date'].values[i], df['Action'].values[i], df['Account Id'].values[i], df['Family Group'].values[i], df['Account Name'].values[i], df['Exchange'].values[i], df['Currency'].values[i], df['Exchange Code'].values[i], df['Instrument Code'].values[i], df['Instrument Name'].values[i], df['Trade Type'].values[i], df['Option Style'].values[i], df['Buy/Sell'].values[i], df['Abs Quantity'].values[i], df['Net Quantity'].values[i], df['Prompt'].values[i], df['Trade Price'].values[i], df['Strike Price'].values[i], df['Market Price'].values[i], df['Premium'].values[i], df['MtM'].values[i], df['Trade Value'].values[i], df['Total Charge'].values[i], df['Commission'].values[i], df['Exchange Fee'].values[i], df['Clearing Fee'].values[i], df['NFA Fee'].values[i], df['Average Price Premium/Discount'].values[i], df['Delivery Type'].values[i], df['Days To Maturity'].values[i], df['Maturity Date'].values[i], df['OTC Indicator'].values[i], df['OTC Type'].values[i], df['Average Price Start Date'].values[i], df['Average Price End Date'].values[i]]
                        map.loc[i] = not_mapped

                else:
                    #print('backdated')
                    comm = df['Exchange'] [i] + " " + df['Instrument Name'][i]
                    temp = map_futures.loc[(map_futures['FC Stone File Commodity Name'] == comm) & (map_futures['FC Stone ACC No'] == df['Account Id'][i])]
                    #print(temp)

                    if not temp.empty:
                        if temp['Futures or FX'].values[0] == 'Futures':
                                buy_sell = df['Buy/Sell'].values[i]
                                lots = df['Abs Quantity'].values[i]
    
                                terminal_month = df['Prompt'].values[i]
                                month = terminal_month.split('-')[0]
                                year = int(terminal_month.split('-')[1]) + 2000
                                terminal_month = month + ' ' + str(year)
    
    
                                price = df['Trade Price'].values[i] * temp['Future Price Multiplier'].values[0]

                                exchange_fee = df['Transaction ID'].values[i]
                                exchange_fee = exchange_fee.split('-')[1]
                                fee = df['Total Charge'].values[i]
                                fee_currency = df['Currency'].values[i]
    
                                not_mapped = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], buy_sell, temp['Broker Account No.'].values[0], lots, terminal_month, price, temp['Sub Strategy'].values[0], '', 'FUTURES', '', '', '', '', '', exchange_fee, fee, fee_currency]
                                backdated_fut.loc[i] = not_mapped
    
                        else:
                            #print('FX')
                            terminal_month = df['Prompt'].values[i]
                            month = terminal_month.split('-')[0]
                            year = int(terminal_month.split('-')[1]) + 2000
                            month = month.upper()
                            terminal_month = month + ' ' + str(year)
                            buy_sell = df['Buy/Sell'].values[i]
                            if buy_sell == 'Buy':
                                pos = 'LONG'
                            else:
                                pos = 'SHORT'
                            lots = df['Abs Quantity'].values[i]
                            price = df['Trade Price'].values[i] * temp['Future Price Multiplier'].values[0]

                            exchange_fee = df['Transaction ID'].values[i]
                            exchange_fee = exchange_fee.split('-')[1]
                            fee = df['Total Charge'].values[i]
                            fee_currency = df['Currency'].values[i]

                            not_mapped = [temp['Ctrbook Code'].values[0], ctr_date, pos, temp['Contract Term'].values[0], terminal_month, temp['Profit Center'].values[0], temp['Trader'].values[0], temp['Sub Strategy'].values[0], '(NA)', temp['Broker'].values[0], temp['Broker Reference'].values[0], temp['Commodity'].values[0], temp['Broker'].values[0], temp['Broker Account No.'].values[0], lots, price, '', '', '', exchange_fee, fee, fee_currency]
                            backdated_fx.loc[i] = not_mapped

                    else:
                        #print("Futures and FX Mapping is not found")
                        not_mapped = [df['Transaction ID'].values[i], df['Business Date'].values[i], df['Trade Date'].values[i], df['Action'].values[i], df['Account Id'].values[i], df['Family Group'].values[i], df['Account Name'].values[i], df['Exchange'].values[i], df['Currency'].values[i], df['Exchange Code'].values[i], df['Instrument Code'].values[i], df['Instrument Name'].values[i], df['Trade Type'].values[i], df['Option Style'].values[i], df['Buy/Sell'].values[i], df['Abs Quantity'].values[i], df['Net Quantity'].values[i], df['Prompt'].values[i], df['Trade Price'].values[i], df['Strike Price'].values[i], df['Market Price'].values[i], df['Premium'].values[i], df['MtM'].values[i], df['Trade Value'].values[i], df['Total Charge'].values[i], df['Commission'].values[i], df['Exchange Fee'].values[i], df['Clearing Fee'].values[i], df['NFA Fee'].values[i], df['Average Price Premium/Discount'].values[i], df['Delivery Type'].values[i], df['Days To Maturity'].values[i], df['Maturity Date'].values[i], df['OTC Indicator'].values[i], df['OTC Type'].values[i], df['Average Price Start Date'].values[i], df['Average Price End Date'].values[i]]
                        map.loc[i] = not_mapped

            else:
                #print("delete position")
                not_mapped = [df['Transaction ID'].values[i], df['Business Date'].values[i], df['Trade Date'].values[i], df['Action'].values[i], df['Account Id'].values[i], df['Family Group'].values[i], df['Account Name'].values[i], df['Exchange'].values[i], df['Currency'].values[i], df['Exchange Code'].values[i], df['Instrument Code'].values[i], df['Instrument Name'].values[i], df['Trade Type'].values[i], df['Option Style'].values[i], df['Buy/Sell'].values[i], df['Abs Quantity'].values[i], df['Net Quantity'].values[i], df['Prompt'].values[i], df['Trade Price'].values[i], df['Strike Price'].values[i], df['Market Price'].values[i], df['Premium'].values[i], df['MtM'].values[i], df['Trade Value'].values[i], df['Total Charge'].values[i], df['Commission'].values[i], df['Exchange Fee'].values[i], df['Clearing Fee'].values[i], df['NFA Fee'].values[i], df['Average Price Premium/Discount'].values[i], df['Delivery Type'].values[i], df['Days To Maturity'].values[i], df['Maturity Date'].values[i], df['OTC Indicator'].values[i], df['OTC Type'].values[i], df['Average Price Start Date'].values[i], df['Average Price End Date'].values[i]]
                delete_positions.loc[i] = not_mapped



    # init writer
    bio = BytesIO()
    writer = pd.ExcelWriter(bio, engine='xlsxwriter')
    # sheets
    opt.to_excel(writer, "FC Stone - Options", index=False)
    fut.to_excel(writer, "FC Stone -Futures", index=False)
    fx.to_excel(writer, "FC Stone -FX", index=False)
    delete_positions.to_excel(writer, "Delete Positions", index=False) 
    map.to_excel(writer, "Mapping Not found", index=False)
    backdated_opt.to_excel(writer, "Backdated Options", index=False)
    backdated_fut.to_excel(writer, "Backdated Futures", index=False)
    backdated_fx.to_excel(writer, "Backdated FX", index=False)

    # save the workbook
    writer.save()
    bio.seek(0)      

    # get the excel file (answers my question)          
    workbook    = bio.read()  
    excelFile   = workbook


    storage.child('fcstone_output/FCSTONE Output.xlsx').put(excelFile)
    storage.child('fcstone_output/FCSTONE Output.xlsx').get_url(None)
































































































































