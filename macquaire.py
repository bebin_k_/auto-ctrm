import pandas as pd
import calendar
from pyrebase import pyrebase
from io import BytesIO

firebaseConfig = {
    'apiKey': "AIzaSyBc4HS2jHC8vm5TFUc4GeWSvMMhGamwOSs",
    'authDomain': "ctrm-300805.firebaseapp.com",
    'databaseURL': "ctrm-300805",
    'storageBucket': "ctrm-300805.appspot.com",
    'messagingSenderId': "883905439975",
    'appId': "1:883905439975:web:49d7ce571bf3e276d1da42",
}

firebase = pyrebase.initialize_app(firebaseConfig)

storage = firebase.storage()

def macquaire(upload_filename, mapping_filename, date):
    df = pd.read_csv(upload_filename)
    map_futures = pd.read_excel(mapping_filename, sheet_name = 'macquaire-futures')
    map_options = pd.read_excel(mapping_filename, sheet_name = 'macquaire-options')
    sub_acc = pd.read_excel(mapping_filename, sheet_name = 'sub-accounts')
    sub_acc = sub_acc["Sub-Acc"].to_numpy()
    #print(sub_acc)
    #print(type(sub_acc))

    opt = pd.DataFrame(columns = ['Ctrbook Code', 'Option Term', 'Option Exercised', 'Broker', 'Broker Account No.', 'Trader', 'Profit Center', 'Strategy', 'DEFAULT', 'External Reference', 'Trader Date', 'Long/ Short', 'Call/ Put', 'Terminal Month', 'Holiday Calender', 'Strike Price', 'Premium', 'No. Of Lots', 'Broker Commission', 'Commission Currency', 'Commission Amount (Per Lot)'])
    fut = pd.DataFrame(columns = ['Ctrbook Code*', 'Profit Center*', 'Ctr Date*', 'Contract Term*', 'Trader*', 'Broker*', 'Broker Reference', 'Position Type*', 'Broker Account No*', 'No. Of Lots*', 'Shipment Month*', 'Price*', 'Sub Strategy', 'Contract type', 'Trade Type*', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)'])
    fx = pd.DataFrame(columns = ['Contract Book',	'Contract Date',	'Long/Short',	'FX Futures Term',	'Delivery Month',	'Profit Center',	'Trader',	'Strategy',	'Tag',	'Trading Broker',	'Trading Broker Account',	'Commodity',	'Clearing Broker',	'Clearing Broker Account',	'No of Contracts',	'FX Rate',	'Internal Reference',	'External Reference',	'IO Number'])
    map = pd.DataFrame(columns= ['Fut or Opt', 'BUSINESS_DATE',	'CLIENT_CODE',	'DESCRIPTION',	'GROUP_CODE',	'CURRENCY',	'CATEGORY',	'COMMODITY_CODE',	'DELIVERY_MONTH',	'FUTURES_OR_OPTION',	'PUT_CALL',	'TRADE_DATE',	'BOUGHT_SOLD',	'QUANTITY',	'BOUGHT_PRICE',	'BOUGHT_PRICE_NUM',	'SOLD_PRICE',	'SOLD_PRICE_NUM',	'CURRENT_PRICE',	'MARGIN_VARIANCE',	'EXECUTING_BROKER_CODE',	'EXERCISE_PRICE',	'EXERCISE_PRICE_NUM',	'COMMISSION_AMOUNT',	'COMM_ONLY_AMOUNT',	'FEES_AMOUNT',	'INSTRUMENT_TYPE',	'LINE_NUMBER',	'COMMISSION_GST_AMOUNT',	'FEES_GST_AMOUNT',	'COMMISSION_EX_GST',	'FEES_EX_GST',	'SETTLEMENT_AMOUNT',	'BOUGHT_CLIENT_DEAL_ID',	'SOLD_CLIENT_DEAL_ID',	'STRATEGY_CODE',	'DATE_FROM',	'DATE_TO',	'DELIVERY_DATE',	'TICK_VALUE',	'TICK_SIZE'])
    #backdated = pd.DataFrame(columns= ['BUSINESS_DATE',	'CLIENT_CODE',	'DESCRIPTION',	'GROUP_CODE',	'CURRENCY',	'CATEGORY',	'COMMODITY_CODE',	'DELIVERY_MONTH',	'FUTURES_OR_OPTION',	'PUT_CALL',	'TRADE_DATE',	'BOUGHT_SOLD',	'QUANTITY',	'BOUGHT_PRICE',	'BOUGHT_PRICE_NUM',	'SOLD_PRICE',	'SOLD_PRICE_NUM',	'CURRENT_PRICE',	'MARGIN_VARIANCE',	'EXECUTING_BROKER_CODE',	'EXERCISE_PRICE',	'EXERCISE_PRICE_NUM',	'COMMISSION_AMOUNT',	'COMM_ONLY_AMOUNT',	'FEES_AMOUNT',	'INSTRUMENT_TYPE',	'LINE_NUMBER',	'COMMISSION_GST_AMOUNT',	'FEES_GST_AMOUNT',	'COMMISSION_EX_GST',	'FEES_EX_GST',	'SETTLEMENT_AMOUNT',	'BOUGHT_CLIENT_DEAL_ID',	'SOLD_CLIENT_DEAL_ID',	'STRATEGY_CODE',	'DATE_FROM',	'DATE_TO',	'DELIVERY_DATE',	'TICK_VALUE',	'TICK_SIZE'])
    backdated_opt = pd.DataFrame(columns = ['Ctrbook Code', 'Option Term', 'Option Exercised', 'Broker', 'Broker Account No.', 'Trader', 'Profit Center', 'Strategy', 'DEFAULT', 'External Reference', 'Trader Date', 'Long/ Short', 'Call/ Put', 'Terminal Month', 'Holiday Calender', 'Strike Price', 'Premium', 'No. Of Lots', 'Broker Commission', 'Commission Currency', 'Commission Amount (Per Lot)'])
    backdated_fut = pd.DataFrame(columns = ['Ctrbook Code*', 'Profit Center*', 'Ctr Date*', 'Contract Term*', 'Trader*', 'Broker*', 'Broker Reference', 'Position Type*', 'Broker Account No*', 'No. Of Lots*', 'Shipment Month*', 'Price*', 'Sub Strategy', 'Contract type', 'Trade Type*', 'Tags', 'IO Number', 'Commission Type', 'Commission Currency', 'Commission Amount (Per Lot)'])
    backdated_fx = pd.DataFrame(columns = ['Contract Book',	'Contract Date',	'Long/Short',	'FX Futures Term',	'Delivery Month',	'Profit Center',	'Trader',	'Strategy',	'Tag',	'Trading Broker',	'Trading Broker Account',	'Commodity',	'Clearing Broker',	'Clearing Broker Account',	'No of Contracts',	'FX Rate',	'Internal Reference',	'External Reference',	'IO Number'])
    #21
    #20
    #19
    pos = ''
    n = 2
    for i in range(0, len(df)):

        ctr_date = df['TRADE_DATE'].values[i]
        year = ctr_date.split('-')[0]
        month = ctr_date.split('-')[1]
        month = calendar.month_abbr[int(month)]
        month = month.upper()
        dt = ctr_date.split('-')[2]
        ctr_date = dt + '-' + month + '-' + str(year)

        if df['CLIENT_CODE'].values[i] in sub_acc:
            #print("Acc Not to be taken")
            continue

        elif ctr_date != date:
            #print('backdated')
            if df['FUTURES_OR_OPTION'].values[i] == 'O':
                #print('option')
                temp = map_options.loc[(map_options['Macquaire File Commodity Name'] == df['COMMODITY_CODE'][i]) & (map_options['Macquaire ACC No'] == df['CLIENT_CODE'][i])]
                #print(temp)
                if not temp.empty:
                    ctr_date = df['TRADE_DATE'].values[i]
                    year = ctr_date.split('-')[0]
                    month = ctr_date.split('-')[1]
                    month = calendar.month_abbr[int(month)]
                    dt = ctr_date.split('-')[2]
                    ctr_date = dt + '-' + month + '-' + str(year)

                    pos = df['PUT_CALL'].values[i]
                    if pos == 'C':
                        pos = 'Call'
                    else:
                        pos = 'Put'

                    buy_sell = df['BOUGHT_SOLD'].values[i]
                    if buy_sell == 'B': 
                        buy_sell = 'Long'
                        premium = df['BOUGHT_PRICE'].values[i] * temp['Premium Multiplier'].values[0]
                    else:
                        buy_sell = 'Short'
                        premium = df['SOLD_PRICE'].values[i] * temp['Premium Multiplier'].values[0]

                    terminal_month = df['DELIVERY_MONTH'].values[i]
                    terminal_month = str(terminal_month)
                    split_month = [(terminal_month[i:i+n]) for i in range(0, len(terminal_month), n)]
                    year = int(split_month[0]) + 2000
                    month = split_month[1]
                    month = calendar.month_abbr[int(month)]
                    terminal_month = month + ' ' + str(year)

                    strike = df['EXERCISE_PRICE'].values[i] * temp['Strike Multiplier'].values[0]
                    lots = df['QUANTITY'].values[i]

                    not_mapped = [temp['Ctrbook Code'].values[0], temp['OptionTerm'].values[0], 'Option Exercised', 'MACQUARIE', temp['Broker Account No.'].values[0], temp['Trader'].values[0], temp['Profit Center'].values[0], temp['Sub Strategy'].values[0], 'DEFAULT', ' ', ctr_date, buy_sell, pos, terminal_month, temp['Holiday Calender'].values[0], strike, premium, lots, 'Brokerage Commission', 'USD', '0.01']
                    backdated_opt.loc[i] = not_mapped

                else:
                    #print('Options mapping not found')
                    not_mapped = ['OPTIONS', df['BUSINESS_DATE'].values[i], df['CLIENT_CODE'].values[i], df['DESCRIPTION'].values[i], df['GROUP_CODE'].values[i], df['CURRENCY'].values[i], df['CATEGORY'].values[i], df['COMMODITY_CODE'].values[i], df['DELIVERY_MONTH'].values[i], df['FUTURES_OR_OPTION'].values[i], df['PUT_CALL'].values[i], df['TRADE_DATE'].values[i], df['BOUGHT_SOLD'].values[i], df['QUANTITY'].values[i], df['BOUGHT_PRICE'].values[i], df['BOUGHT_PRICE_NUM'].values[i], df['SOLD_PRICE'].values[i], df['SOLD_PRICE_NUM'].values[i], df['CURRENT_PRICE'].values[i], df['MARGIN_VARIANCE'].values[i], df['EXECUTING_BROKER_CODE'].values[i], df['EXERCISE_PRICE'].values[i], df['EXERCISE_PRICE_NUM'].values[i], df['COMMISSION_AMOUNT'].values[i], df['COMM_ONLY_AMOUNT'].values[i], df['FEES_AMOUNT'].values[i], df['INSTRUMENT_TYPE'].values[i], df['LINE_NUMBER'].values[i], df['COMMISSION_GST_AMOUNT'].values[i], df['FEES_GST_AMOUNT'].values[i], df['COMMISSION_EX_GST'].values[i], df['FEES_EX_GST'].values[i], df['SETTLEMENT_AMOUNT'].values[i], df['BOUGHT_CLIENT_DEAL_ID'].values[i], df['SOLD_CLIENT_DEAL_ID'].values[i], df['STRATEGY_CODE'].values[i], df['DATE_FROM'].values[i], df['DATE_TO'].values[i], df['DELIVERY_DATE'].values[i], df['TICK_VALUE'].values[i], df['TICK_SIZE'].values[i]]
                    map.loc[i] = not_mapped 

            elif df['FUTURES_OR_OPTION'].values[i] == 'F':
                #print('futures')
                temp = map_futures.loc[(map_futures['Macquaire File Commodity Name'] == df['COMMODITY_CODE'][i]) & (map_futures['Macquaire ACC No'] == df['CLIENT_CODE'][i])]
                #print(temp)
                if not temp.empty:
                    if temp['Futures or FX'].values[0] == 'Futures':

                        ctr_date = df['TRADE_DATE'].values[i]
                        year = ctr_date.split('-')[0]
                        month = ctr_date.split('-')[1]
                        month = calendar.month_abbr[int(month)]
                        dt = ctr_date.split('-')[2]
                        ctr_date = dt + '-' + month + '-' + str(year)

                        buy_sell = df['BOUGHT_SOLD'].values[i]
                        if buy_sell == 'B':
                            buy_sell = 'BUY'
                            price = df['BOUGHT_PRICE'].values[i] * temp['Future Price Multiplier'].values[0]
                        else:
                            buy_sell = 'SELL'
                            price = df['SOLD_PRICE'].values[i] * temp['Future Price Multiplier'].values[0]

                        lots = df['QUANTITY'].values[i]

                        terminal_month = df['DELIVERY_MONTH'].values[i]
                        terminal_month = str(terminal_month)
                        split_month = [(terminal_month[i:i+n]) for i in range(0, len(terminal_month), n)]
                        year = int(split_month[0]) + 2000
                        month = split_month[1]
                        month = calendar.month_abbr[int(month)]
                        terminal_month = month + ' ' + str(year)

                        not_mapped = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], buy_sell, temp['Broker Account No.'].values[0], lots, terminal_month, price, temp['Sub Strategy'].values[0], '', 'FUTURES', '', '', '', '', '']
                        backdated_fut.loc[i] = not_mapped


                    else:
                        #print('FX')
                        ctr_date = df['TRADE_DATE'].values[i]
                        year = ctr_date.split('-')[0]
                        month = ctr_date.split('-')[1]
                        month = calendar.month_abbr[int(month)]
                        dt = ctr_date.split('-')[2]
                        ctr_date = dt + '-' + month + '-' + str(year)

                        terminal_month = df['DELIVERY_MONTH'].values[i]
                        terminal_month = str(terminal_month)
                        split_month = [(terminal_month[i:i+n]) for i in range(0, len(terminal_month), n)]
                        year = int(split_month[0]) + 2000
                        month = split_month[1]
                        month = calendar.month_abbr[int(month)]
                        terminal_month = month + ' ' + str(year)

                        lots = df['QUANTITY'].values[i]

                        buy_sell = df['BOUGHT_SOLD'].values[i]
                        if buy_sell == 'B':
                            buy_sell = 'Long'
                            price = df['BOUGHT_PRICE'].values[i] * temp['Future Price Multiplier'].values[0]
                        else:
                            buy_sell = 'Short'
                            price = df['SOLD_PRICE'].values[i] * temp['Future Price Multiplier'].values[0]

                        not_mapped = [temp['Ctrbook Code'].values[0], ctr_date, buy_sell, temp['Contract Term'].values[0], terminal_month, temp['Profit Center'].values[0], temp['Trader'].values[0], temp['Sub Strategy'].values[0], '(NA)', temp['Broker'].values[0], temp['Broker Reference'].values[0], temp['Commodity'].values[0], temp['Broker'].values[0], temp['Broker Account No.'].values[0], lots, price, '', '', '']
                        backdated_fx.loc[i] = not_mapped

                else:
                    #print('Future and FX mapping not found')
                    not_mapped = ['FUTURE OR FX', df['BUSINESS_DATE'].values[i], df['CLIENT_CODE'].values[i], df['DESCRIPTION'].values[i], df['GROUP_CODE'].values[i], df['CURRENCY'].values[i], df['CATEGORY'].values[i], df['COMMODITY_CODE'].values[i], df['DELIVERY_MONTH'].values[i], df['FUTURES_OR_OPTION'].values[i], df['PUT_CALL'].values[i], df['TRADE_DATE'].values[i], df['BOUGHT_SOLD'].values[i], df['QUANTITY'].values[i], df['BOUGHT_PRICE'].values[i], df['BOUGHT_PRICE_NUM'].values[i], df['SOLD_PRICE'].values[i], df['SOLD_PRICE_NUM'].values[i], df['CURRENT_PRICE'].values[i], df['MARGIN_VARIANCE'].values[i], df['EXECUTING_BROKER_CODE'].values[i], df['EXERCISE_PRICE'].values[i], df['EXERCISE_PRICE_NUM'].values[i], df['COMMISSION_AMOUNT'].values[i], df['COMM_ONLY_AMOUNT'].values[i], df['FEES_AMOUNT'].values[i], df['INSTRUMENT_TYPE'].values[i], df['LINE_NUMBER'].values[i], df['COMMISSION_GST_AMOUNT'].values[i], df['FEES_GST_AMOUNT'].values[i], df['COMMISSION_EX_GST'].values[i], df['FEES_EX_GST'].values[i], df['SETTLEMENT_AMOUNT'].values[i], df['BOUGHT_CLIENT_DEAL_ID'].values[i], df['SOLD_CLIENT_DEAL_ID'].values[i], df['STRATEGY_CODE'].values[i], df['DATE_FROM'].values[i], df['DATE_TO'].values[i], df['DELIVERY_DATE'].values[i], df['TICK_VALUE'].values[i], df['TICK_SIZE'].values[i]]
                    map.loc[i] = not_mapped



        elif df['FUTURES_OR_OPTION'].values[i] == 'O':
            #print('option')
            temp = map_options.loc[(map_options['Macquaire File Commodity Name'] == df['COMMODITY_CODE'][i]) & (map_options['Macquaire ACC No'] == df['CLIENT_CODE'][i])]
            #print(temp)
            if not temp.empty:
                ctr_date = df['TRADE_DATE'].values[i]
                year = ctr_date.split('-')[0]
                month = ctr_date.split('-')[1]
                month = calendar.month_abbr[int(month)]
                dt = ctr_date.split('-')[2]
                ctr_date = dt + '-' + month + '-' + str(year)

                pos = df['PUT_CALL'].values[i]
                if pos == 'C':
                    pos = 'Call'
                else:
                    pos = 'Put'

                buy_sell = df['BOUGHT_SOLD'].values[i]
                if buy_sell == 'B': 
                    buy_sell = 'Long'
                    premium = df['BOUGHT_PRICE'].values[i] * temp['Premium Multiplier'].values[0]
                else:
                    buy_sell = 'Short'
                    premium = df['SOLD_PRICE'].values[i] * temp['Premium Multiplier'].values[0]

                terminal_month = df['DELIVERY_MONTH'].values[i]
                terminal_month = str(terminal_month)
                split_month = [(terminal_month[i:i+n]) for i in range(0, len(terminal_month), n)]
                year = int(split_month[0]) + 2000
                month = split_month[1]
                month = calendar.month_abbr[int(month)]
                terminal_month = month + ' ' + str(year)

                strike = df['EXERCISE_PRICE'].values[i] * temp['Strike Multiplier'].values[0]
                lots = df['QUANTITY'].values[i]

                row = [temp['Ctrbook Code'].values[0], temp['OptionTerm'].values[0], 'Option Exercised', 'MACQUARIE', temp['Broker Account No.'].values[0], temp['Trader'].values[0], temp['Profit Center'].values[0], temp['Sub Strategy'].values[0], 'DEFAULT', ' ', ctr_date, buy_sell, pos, terminal_month, temp['Holiday Calender'].values[0], strike, premium, lots, 'Brokerage Commission', 'USD', '0.01']
                opt.loc[i] = row

            else:
                #print('Options mapping not found')
                not_mapped = ['OPTIONS', df['BUSINESS_DATE'].values[i], df['CLIENT_CODE'].values[i], df['DESCRIPTION'].values[i], df['GROUP_CODE'].values[i], df['CURRENCY'].values[i], df['CATEGORY'].values[i], df['COMMODITY_CODE'].values[i], df['DELIVERY_MONTH'].values[i], df['FUTURES_OR_OPTION'].values[i], df['PUT_CALL'].values[i], df['TRADE_DATE'].values[i], df['BOUGHT_SOLD'].values[i], df['QUANTITY'].values[i], df['BOUGHT_PRICE'].values[i], df['BOUGHT_PRICE_NUM'].values[i], df['SOLD_PRICE'].values[i], df['SOLD_PRICE_NUM'].values[i], df['CURRENT_PRICE'].values[i], df['MARGIN_VARIANCE'].values[i], df['EXECUTING_BROKER_CODE'].values[i], df['EXERCISE_PRICE'].values[i], df['EXERCISE_PRICE_NUM'].values[i], df['COMMISSION_AMOUNT'].values[i], df['COMM_ONLY_AMOUNT'].values[i], df['FEES_AMOUNT'].values[i], df['INSTRUMENT_TYPE'].values[i], df['LINE_NUMBER'].values[i], df['COMMISSION_GST_AMOUNT'].values[i], df['FEES_GST_AMOUNT'].values[i], df['COMMISSION_EX_GST'].values[i], df['FEES_EX_GST'].values[i], df['SETTLEMENT_AMOUNT'].values[i], df['BOUGHT_CLIENT_DEAL_ID'].values[i], df['SOLD_CLIENT_DEAL_ID'].values[i], df['STRATEGY_CODE'].values[i], df['DATE_FROM'].values[i], df['DATE_TO'].values[i], df['DELIVERY_DATE'].values[i], df['TICK_VALUE'].values[i], df['TICK_SIZE'].values[i]]
                map.loc[i] = not_mapped 

        elif df['FUTURES_OR_OPTION'].values[i] == 'F':
            #print('futures')
            temp = map_futures.loc[(map_futures['Macquaire File Commodity Name'] == df['COMMODITY_CODE'][i]) & (map_futures['Macquaire ACC No'] == df['CLIENT_CODE'][i])]
            #print(temp)
            if not temp.empty:
                if temp['Futures or FX'].values[0] == 'Futures':

                    ctr_date = df['TRADE_DATE'].values[i]
                    year = ctr_date.split('-')[0]
                    month = ctr_date.split('-')[1]
                    month = calendar.month_abbr[int(month)]
                    dt = ctr_date.split('-')[2]
                    ctr_date = dt + '-' + month + '-' + str(year)

                    buy_sell = df['BOUGHT_SOLD'].values[i]
                    if buy_sell == 'B':
                        buy_sell = 'BUY'
                        price = df['BOUGHT_PRICE'].values[i] * temp['Future Price Multiplier'].values[0]
                    else:
                        buy_sell = 'SELL'
                        price = df['SOLD_PRICE'].values[i] * temp['Future Price Multiplier'].values[0]

                    lots = df['QUANTITY'].values[i]

                    terminal_month = df['DELIVERY_MONTH'].values[i]
                    terminal_month = str(terminal_month)
                    split_month = [(terminal_month[i:i+n]) for i in range(0, len(terminal_month), n)]
                    year = int(split_month[0]) + 2000
                    month = split_month[1]
                    month = calendar.month_abbr[int(month)]
                    terminal_month = month + ' ' + str(year)

                    row = [temp['Ctrbook Code'].values[0], temp['Profit Center'].values[0], ctr_date, temp['Contract Term'].values[0], temp['Trader'].values[0], temp['Broker'].values[0], temp['Broker Reference'].values[0], buy_sell, temp['Broker Account No.'].values[0], lots, terminal_month, price, temp['Sub Strategy'].values[0], '', 'FUTURES', '', '', '', '', '']
                    fut.loc[i] = row

                
                else:
                    #print('FX')
                    ctr_date = df['TRADE_DATE'].values[i]
                    year = ctr_date.split('-')[0]
                    month = ctr_date.split('-')[1]
                    month = calendar.month_abbr[int(month)]
                    dt = ctr_date.split('-')[2]
                    ctr_date = dt + '-' + month + '-' + str(year)

                    terminal_month = df['DELIVERY_MONTH'].values[i]
                    terminal_month = str(terminal_month)
                    split_month = [(terminal_month[i:i+n]) for i in range(0, len(terminal_month), n)]
                    year = int(split_month[0]) + 2000
                    month = split_month[1]
                    month = calendar.month_abbr[int(month)]
                    terminal_month = month + ' ' + str(year)

                    lots = df['QUANTITY'].values[i]

                    buy_sell = df['BOUGHT_SOLD'].values[i]
                    if buy_sell == 'B':
                        buy_sell = 'Long'
                        price = df['BOUGHT_PRICE'].values[i] * temp['Future Price Multiplier'].values[0]
                    else:
                        buy_sell = 'Short'
                        price = df['SOLD_PRICE'].values[i] * temp['Future Price Multiplier'].values[0]

                    row = [temp['Ctrbook Code'].values[0], ctr_date, buy_sell, temp['Contract Term'].values[0], terminal_month, temp['Profit Center'].values[0], temp['Trader'].values[0], temp['Sub Strategy'].values[0], '(NA)', temp['Broker'].values[0], temp['Broker Reference'].values[0], temp['Commodity'].values[0], temp['Broker'].values[0], temp['Broker Account No.'].values[0], lots, price, '', '', '']
                    fx.loc[i] = row

            else:
                #print('Future and FX mapping not found')
                not_mapped = ['FUTURE OR FX', df['BUSINESS_DATE'].values[i], df['CLIENT_CODE'].values[i], df['DESCRIPTION'].values[i], df['GROUP_CODE'].values[i], df['CURRENCY'].values[i], df['CATEGORY'].values[i], df['COMMODITY_CODE'].values[i], df['DELIVERY_MONTH'].values[i], df['FUTURES_OR_OPTION'].values[i], df['PUT_CALL'].values[i], df['TRADE_DATE'].values[i], df['BOUGHT_SOLD'].values[i], df['QUANTITY'].values[i], df['BOUGHT_PRICE'].values[i], df['BOUGHT_PRICE_NUM'].values[i], df['SOLD_PRICE'].values[i], df['SOLD_PRICE_NUM'].values[i], df['CURRENT_PRICE'].values[i], df['MARGIN_VARIANCE'].values[i], df['EXECUTING_BROKER_CODE'].values[i], df['EXERCISE_PRICE'].values[i], df['EXERCISE_PRICE_NUM'].values[i], df['COMMISSION_AMOUNT'].values[i], df['COMM_ONLY_AMOUNT'].values[i], df['FEES_AMOUNT'].values[i], df['INSTRUMENT_TYPE'].values[i], df['LINE_NUMBER'].values[i], df['COMMISSION_GST_AMOUNT'].values[i], df['FEES_GST_AMOUNT'].values[i], df['COMMISSION_EX_GST'].values[i], df['FEES_EX_GST'].values[i], df['SETTLEMENT_AMOUNT'].values[i], df['BOUGHT_CLIENT_DEAL_ID'].values[i], df['SOLD_CLIENT_DEAL_ID'].values[i], df['STRATEGY_CODE'].values[i], df['DATE_FROM'].values[i], df['DATE_TO'].values[i], df['DELIVERY_DATE'].values[i], df['TICK_VALUE'].values[i], df['TICK_SIZE'].values[i]]
                map.loc[i] = not_mapped

    # init writer
    bio = BytesIO()
    writer = pd.ExcelWriter(bio, engine='xlsxwriter')

    # sheets
    opt.to_excel(writer, "Macquaire - Options", index=False)
    fut.to_excel(writer, "Macquaire - Futures", index=False)
    fx.to_excel(writer, "Macquaire - FX", index=False)
    map.to_excel(writer, "Mapping Not Found", index=False)
    backdated_opt.to_excel(writer, "Backdated Options", index=False)
    backdated_fut.to_excel(writer, "Backdated Futures", index=False)
    backdated_fx.to_excel(writer, "Backdated FX", index=False)
    # save the workbook
    writer.save()
    bio.seek(0)      

    # get the excel file (answers my question)          
    workbook    = bio.read()  
    excelFile   = workbook


    storage.child('macquaire_output/Macquaire Output.xlsx').put(excelFile)
    storage.child('macquaire_output/Macquaire Output.xlsx').get_url(None)